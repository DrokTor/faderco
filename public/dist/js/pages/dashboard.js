$(function () {

  "use strict";

  //Make the dashboard widgets sortable Using jquery UI
  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs",
    forcePlaceholderSize: true,
    zIndex: 999999
  });
  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

  //jvectormap data
  var visitorsData = {
    "US": 398, //USA
    "SA": 400, //Saudi Arabia
    "CA": 1000, //Canada
    "DE": 500, //Germany
    "FR": 760, //France
    "CN": 300, //China
    "AU": 700, //Australia
    "BR": 600, //Brazil
    "IN": 800, //India
    "GB": 320, //Great Britain
    "RU": 3000 //Russia
  };
  //World map by jvectormap
  $('#world-map').vectorMap({
    map: 'dz_fr',
    backgroundColor: "transparent",
    regionStyle: {
      initial: {
        fill: '#e4e4e4',
        "fill-opacity": 1,
        stroke: 'none',
        "stroke-width": 0,
        "stroke-opacity": 1
      }
    },
    series: {
      regions: [{
        values: visitorsData,
        scale: ["#92c1dc", "#ebf4f9"],
        normalizeFunction: 'polynomial'
      }]
    },
    onRegionLabelShow: function (e, el, code) {
      if (typeof visitorsData[code] != "undefined")
        el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
    }
  });

  //SLIMSCROLL FOR CHAT WIDGET
  $('#chat-box').slimScroll({
    height: '400px'
  });

  /* Morris.js Charts */
  var charts = new Morris.Area({
    element: 'pdvs-chart',
    resize: true,
    xkey: 'period',
    ykeys: ['pdvs', 'validated', 'rpms', 'rvcs', 'rps', 'rpcs'],
    labels: ['Pdvs Ajoutés', 'Pdvs Validés', 'Rpms Ajoutés', 'Rvcs Ajoutés', 'Rps Ajoutés', 'Rpcs Ajoutés'],
    xLabels : 'day',
    lineColors: ['#a0d0e0', '#3c8dbc', '#00a65a', '#dd4b39', '#f39c12', '#ff851b'],
    hideHover: 'auto',
    parseTime : false
  });

  $.ajax({
    type : 'GET',
    dataType : 'json',
    url : '/ajax'
  })
  .done(function (data) {
    charts.setData(data['charts'])  ;
  })
  .fail(function () {
    bootbox.dialog({
      closeButton: false,
      message: "Une erreur s'est produite, veuillez reessayer",
      title: "Faderco Information",
      buttons: {
        success: {
          label: "Fermer!",
          className: "btn-danger",
          callback: function() {
            location.href='/';
          }
        }
      }
    });
  });

  //Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
    donut.redraw();
    line.redraw();
  });
});
