<?php
/**
 * Created by PhpStorm.
 * User: Messi89
 * Date: 16-04-2016
 * Time: 17:40
 */

namespace App\Overgen\Facades;

use \Illuminate\Support\Facades\Facade;

class FadercoResponse extends Facade
{
    protected static function getFacadeAccessor() { return 'App\Overgen\Faderco\FadercoResponse'; }
}