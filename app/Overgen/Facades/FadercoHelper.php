<?php
/**
 * Created by PhpStorm.
 * User: Messi89
 * Date: 16-04-2016
 * Time: 20:13
 */

namespace App\Overgen\Facades;

use Illuminate\Support\Facades\Facade;

class FadercoHelper extends Facade
{
    protected static function getFacadeAccessor() { return 'App\Overgen\Faderco\FadercoHelper'; }
}