<?php
/**
 * Created by PhpStorm.
 * User: Messi89
 * Date: 16-04-2016
 * Time: 15:41
 */

namespace App\Overgen\Faderco;


class FadercoResponse
{
    //API SUCCESS
    public static $SUCCCESS = array(array('id' => '-200')); //message de succès générique pour les requetes sur l'api

    //API REQUEST ERROR
    public static $REQ_TYPE_ERR = array(array('id' => '-201')); //erreur dans le type de la requete http

    //API Authentification
    public static $AUTH_ERR = array(array('id' => '-400')); //echec d'authentification (username/password)
    public static $AUTH_TOKEN_ERR = array(array('id' => '-402')); //echec de generation de l'acces token (champ manquant ou invalide)
    public static $AUTH_TOKEN_INVALID = array(array('id' => '-403')); //token invalide

    //API SGBD
    public static $SGBD_ERR = array(array('id' => '-500')); //erreur avec la bdd
    public static $SGBD_NO_MODIF = array(array('id' => '-501')); //aucune modif n'a était effectué
    public static $SGBD_EMPTY_ANSWER = array(array('id' => '-502')); // la requete n'a retourné aucun résultat
    public static $SGBD_EMPTY_ANSWER_PAGGED = array(array('id' => '-502')); // la requete n'a retourné aucun résultat
    public static $SGBD_ROW_NOT_FOUND = array(array('id' => '-503')); // enregistrement non trouvé
    public static $SGBD_VER_ERR = array(array('id' => '-504'));


    //API Validation Form
    public static $INPUT_VALID_ERR = array(array('id' => '-600')); // des champs necessaires sont manquant/invalide

    //API Directory/File error
    public static $DIR_CREAT_ERR = array(array('id' => '-300')); //erreur dans la création du repertoire
    public static $FILE_CREAT_ERR = array(array('id' => '-301')); //erreur dans la création du fichier
    public static $FILE_UPLOAD_ERR = array(array('id' => '-302')); //erreur dans l'upload du fichier (fichier endomagé)


    /**
     * @return array
     */
    public static function getSUCCCESS()
    {
        return self::$SUCCCESS;
    }

    /**
     * @return array
     */
    public static function getREQTYPEERR()
    {
        return self::$REQ_TYPE_ERR;
    }

    /**
     * @return array
     */
    public static function getAUTHERR()
    {
        return self::$AUTH_ERR;
    }

    /**
     * @return array
     */
    public static function getAUTHTOKENERR()
    {
        return self::$AUTH_TOKEN_ERR;
    }

    /**
     * @return array
     */
    public static function getAUTHTOKENINVALID()
    {
        return self::$AUTH_TOKEN_INVALID;
    }

    /**
     * @return array
     */
    public static function getSGBDERR()
    {
        return self::$SGBD_ERR;
    }

    /**
     * @return array
     */
    public static function getSGBDNOMODIF()
    {
        return self::$SGBD_NO_MODIF;
    }

    /**
     * @return array
     */
    public static function getSGBDEMPTYANSWER()
    {
        return self::$SGBD_EMPTY_ANSWER;
    }

    /**
     * @return array
     */
    public static function getSGBDEMPTYANSWERPAGGED()
    {
        return self::$SGBD_EMPTY_ANSWER_PAGGED;
    }

    /**
     * @return array
     */
    public static function getINPUTVALIDERR()
    {
        return self::$INPUT_VALID_ERR;
    }
    /**
     * @return array
     */
    public static function getSGBDROWNOTFOUND()
    {
        return self::$SGBD_ROW_NOT_FOUND;
    }

    /**
     * @return array
     */
    public static function getDIRCREATERR()
    {
        return self::$DIR_CREAT_ERR;
    }

    /**
     * @return array
     */
    public static function getFILECREATERR()
    {
        return self::$FILE_CREAT_ERR;
    }

    /**
     * @return array
     */
    public static function getFILEUPLOADERR()
    {
        return self::$FILE_UPLOAD_ERR;
    }

    /**
     * @return array
     */
    public static function getSGBDVERERR()
    {
        return self::$SGBD_VER_ERR;
    }
}
