<?php
/**
 * Created by PhpStorm.
 * User: Messi89
 * Date: 16-04-2016
 * Time: 20:12
 */

namespace App\Overgen\Faderco;

use App\Models\Activity;
use App\Models\Versioning;
use Authorizer;
use Exception;
use App\Models\User;
use App\Models\Oauth_Access_Token;
use App\Models\Oauth;
use App\Models\Oauth_Session;
use League\OAuth2\Server\Exception\UnsupportedGrantTypeException;
use League\OAuth2\Server\Exception\InvalidClientException;
use League\OAuth2\Server\Exception\InvalidRequestException;
use League\OAuth2\Server\Exception\AccessDeniedException;
use League\OAuth2\Server\Exception\OAuthException;
use Log;

class FadercoHelper
{
    public static function getClientSecret($username) {
        try {
            return Oauth::where('id', '=', base64_encode($username))->first()->secret;
        }
        catch (Exception $ex) {
            return false;
        }
    }

    public static function getToken($username, $grant_type, $client_id, $client_secret) {
        try {
            if ($client_secret == Oauth::where('id', '=', base64_encode($username))->first()->secret)
                return Authorizer::issueAccessToken($grant_type, $client_id, $client_secret);

            return false;
        }
        catch(InvalidClientException $e) {
            Log::error($e->getMessage());
            return false;
        }
        catch(InvalidRequestException $e) {
            Log::error($e->getMessage());
            return false;
        }
        catch(UnsupportedGrantTypeException $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public static function checkToken($token) {
        try
        {
            return (Authorizer::validateAccessToken(true, $token));
        }
        catch(AccessDeniedException $e) {
            return false;
        }
        catch(OAuthException $ex) {
            return false;
        }
    }

    public static function getUserID($token){

        try {
            $session_id = Oauth_Access_Token::where('id', '=', $token)->first()->session_id;

            $username = base64_decode(Oauth_Session::where('id', '=', $session_id)->first()->client_id);

            $user_id = User::where('username', '=', $username)->first()->id;

            return $user_id;
        }
        catch (Exception $e) {
            return -1;
        }
    }
    
    public static function createDirectory($path) {

        try {
            umask(0);

            if(!\File::isDirectory(public_path($path)))
                \File::makeDirectory(public_path($path), 0777);

            return true;
        }
        catch (Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    public static function deleteDirectory($path) {
        try {
            umask(0);

            if(\File::isDirectory(public_path($path)))
                \File::deleteDirectory(public_path($path));

            return true;
        }
        catch (Exception $e) {
            Log::info($e->getMessage());
            return false;
        }
    }

    public static function moveFile($file, $filename, $path) {
        try {
            $destinationPath = public_path($path);

            $file->move($destinationPath, $filename);

            return true;
        }
        catch (Exception $e) {
            Log::info($e->getMessage());
            return false;
        }

    }

    public static function setActivity($adresse_ip, $localisation_lat, $localisation_long, $message, $type_activity_id, $user_id ) {
        try {
            $data = [
                'adresse_ip' => $adresse_ip,
                'localisation_lat' => $localisation_lat,
                'localisation_long' => $localisation_long,
                'message' => $message,
                'type_activity_id' => $type_activity_id,
                'user_id' => $user_id
            ];

            Activity::create($data);

            return true;
        }
        catch(Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public static function setVersioning() {
        try {

            $version = Versioning::first();
            $version->version += 1;
            $version->save();

            return true;
        }
        catch(Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    public static function getVersion() {
        try {
            return Versioning::first()->version;
        }
        catch(Exception $e) {
            Log::error($e->getMessage());
            return -1;
        }
    }

    public function truncateMessage($string, $limit, $break=".", $pad="...")
    {
        // return with no change if string is shorter than $limit
        if(strlen($string) <= $limit) return $string;

        // is $break present between $limit and the end of the string?
        if(false !== ($breakpoint = strpos($string, $break, $limit))) {
            if($breakpoint < strlen($string) - 1) {
                $string = substr($string, 0, $breakpoint) . $pad;
            }
        }

        return $string;
    }
}