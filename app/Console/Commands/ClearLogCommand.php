<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearLogCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faderco:clear-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Application Logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $command = 'truncate -s 0 '.storage_path('logs/laravel.log');
        shell_exec($command);
    }
}
