<?php

namespace App\Console\Commands;

use App\Models\Oauth_Access_Token;
use App\Models\Oauth_Session;
use Illuminate\Console\Command;
use Log;

class ClearExpiredTokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faderco:clear-expired-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete expired Tokens';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $expired_sessions = Oauth_Access_Token::where('expire_time', '<=', time())->get();

        if($expired_sessions) {
            foreach ($expired_sessions as $expired_session) {
                if(Oauth_Session::where('id', '=', $expired_session->session_id)->delete()) {
                    Log::info('Expired Tokens with session id : '.$expired_session->session_id.' deleted');
                }
            }
        }
    }
}
