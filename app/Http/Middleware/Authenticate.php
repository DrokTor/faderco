<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return  redirect()->guest('connexion');
            }
        }

        $user = Auth::user();
        //dd($role);
        if ($user == null || $user->role->designation != $role) {

            Auth::logout();
            return  redirect('connexion');
        }

        return $next($request);
    }
}
