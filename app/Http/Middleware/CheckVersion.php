<?php

namespace App\Http\Middleware;

use App\Overgen\Facades\FadercoHelper;
use Closure;
use FadercoResponse;
use Response;

class CheckVersion
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $version = FadercoHelper::getVersion();

        if($request->version == null) return $next($request);

        if($request->version != $version ) return Response::json(FadercoResponse::getSGBDVERERR());

        return $next($request);
    }
}
