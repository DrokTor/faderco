<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();

//Route::get('/home', 'Web\HomeController@index');

Route::get('version', function(){ return 'ok'; })->middleware('version');

//////////////////////////////////////Authentification/Sessions////////////////////////////////////////////
// Route::get('connexion', 'Web\HomeController@index');
// Route::post('connexion', 'Web\HomeController@index');
Route::get('connexion', 'Web\AuthController@create');
Route::post('connexion', 'Web\AuthController@store');
Route::get('deconnexion', 'Web\AuthController@destroy');

Route::resource('marques', 'Web\MarquesController');
Route::resource('/', 'Web\HomeController', ['only' => ['index']]);
    Route::resource('users', 'Web\UsersController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
    Route::resource('messages', 'Web\MessagesController', ['only' => ['index', 'create', 'store', 'show', 'destroy']]);
    Route::get('pdvs/pending', 'Web\PdvsController@pendingIndex');
    Route::get('pdvs/advanced', 'Web\PdvsController@advancedSearch');
    Route::resource('pdvs', 'Web\PdvsController', ['only' => ['index', 'create', 'show', 'edit', 'update', 'destroy']]);

//////////////////////////////////////Web////////////////////////////////////////////
Route::group(['middleware' => 'auth:administrateur'], function() {
    Route::resource('/', 'Web\HomeController', ['only' => ['index']]);
    Route::resource('users', 'Web\UsersController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
    Route::resource('messages', 'Web\MessagesController', ['only' => ['index', 'create', 'store', 'show', 'destroy']]);
    Route::get('pdvs/pending', 'Web\PdvsController@pendingIndex');
    Route::get('pdvs/advanced', 'Web\PdvsController@advancedSearch');
    Route::resource('pdvs', 'Web\PdvsController', ['only' => ['index', 'create', 'show', 'edit', 'update', 'destroy']]);
    Route::resource('rpms', 'Web\RpmsController', ['only' => ['index', 'show', 'destroy']]);
    Route::get('rpms/{id}/pdf', 'Web\RpmsController@pdfGenerator');
    Route::resource('rvcs', 'Web\RvcsController', ['only' => ['index', 'show', 'destroy']]);
    Route::get('rvcs/{id}/pdf', 'Web\RvcsController@pdfGenerator');
    Route::resource('rps', 'Web\RpsController', ['only' => ['index', 'show', 'destroy']]);
    Route::get('rps/{id}/pdf', 'Web\RpsController@pdfGenerator');
    Route::resource('rpcs', 'Web\RpcsController', ['only' => ['index', 'show', 'destroy']]);
    Route::get('rpcs/{id}/pdf', 'Web\RpcsController@pdfGenerator');
    Route::resource('marques', 'Web\MarquesController');
    Route::get('pdvs/pdvs/plvs', 'Web\PdvsController@servePlvs');
    Route::get('marques/marques/all', 'Web\MarquesController@serveMarques'); // ajax pour récupérer un dropbox de marques
    Route::resource('familles', 'Web\FamillesController');
    Route::resource('produits', 'Web\ProduitsController');
    Route::get('produits/familles/{id}', 'Web\ProduitsController@serveFamillies'); // ajax pour récupérer un dropbox de familles d'une marque
    Route::resource('cannaux', 'Web\CannauxAchatsController');
    Route::resource('typespdvs', 'Web\TypesPdvsController');
    Route::resource('zones', 'Web\ZonesChalandisesController');
    Route::resource('surfaces', 'Web\SurfacesPdvsController');
    Route::resource('plvs', 'Web\PlvsStoresController');
    Route::resource('freins', 'Web\FreinsController');
    Route::resource('ruptures', 'Web\RupturesController');
    Route::resource('regions', 'Web\RegionsController');
    Route::resource('wilayas', 'Web\WilayasController');
    Route::get('wilayas/region/{id}', 'Web\WilayasController@serveWilayas'); // ajax pour récupérer un dropbox de wilayas d'une région
    Route::resource('communes', 'Web\CommunesController');
    Route::get('communes/wilaya/{id}', 'Web\CommunesController@serveCommunes'); // ajax pour récupérer un dropbox de communes d'une wilayas
    Route::resource('mechanisms', 'Web\MechanismsController');
    Route::resource('actiontypes', 'Web\ActionTypesController');
});

Route::group(['prefix' => 'ajax', 'middleware' => 'auth:administrateur'], function() {
    Route::resource('/', 'Ajax\HomeController', ['only' => ['index']]);
    Route::resource('/regions', 'Ajax\RegionsController', ['only' => ['show']]);
    Route::resource('/wilayas', 'Ajax\WilayasController', ['only' => ['show']]);
});


//////////////////////////////////////API////////////////////////////////////////////
Route::resource('api/sessions', 'Api\SessionsController', ['only' => ['store'] ]);
Route::get('api/checktoken/{token}', 'Api\SessionsController@checkToken');
Route::group(['prefix' => 'api', 'middleware' => ['oauth', 'version']], function () {
    Route::resource('home', 'Api\HomeController', ['only' => ['index', 'store', 'update', 'destroy'] ]);
    Route::resource('users', 'Api\UsersController', ['only' => ['update']]);
    Route::resource('messages', 'Api\MessagesController', ['only' => ['index', 'store', 'show'] ]);
    Route::resource('marques', 'Api\MarquesController', ['only' => ['index', 'show','store', 'update', 'destroy']]);
    Route::resource('familles', 'Api\FamillesController', ['only' => ['index', 'show','store', 'update', 'destroy']]);
    Route::resource('produits', 'Api\ProduitsController', ['only' => ['index', 'show','store', 'update', 'destroy']]);
    Route::resource('typepdvs', 'Api\PdvTypesController', ['only' => ['index', 'show','store', 'update', 'destroy']]);
    Route::resource('surfacepdvs', 'Api\PdvSurfacesController', ['only' => ['index', 'show','store', 'update', 'destroy']]);
    Route::resource('zonechalandises', 'Api\ZonesChalandisesController', ['only' => ['index', 'show','store', 'update', 'destroy']]);
    Route::resource('plvs', 'Api\PlvsController', ['only' => ['index', 'show','store', 'update', 'destroy']]);
    Route::resource('canalachats', 'Api\CanalAchatsController', ['only' => ['index', 'show','store', 'update', 'destroy']]);
    Route::resource('pdvs', 'Api\PdvsController', ['only' => ['index', 'show','store', 'update']]);
    Route::resource('activities', 'Api\ActivitiesController', ['only' => ['store']]);
    Route::resource('activitiestypes', 'Api\ActivitiesTypesController', ['only' => ['index', 'show']]);
    Route::resource('regions', 'Api\RegionsController', ['only' => ['index', 'show']]);
    Route::resource('wilayas', 'Api\WilayasController', ['only' => ['index', 'show']]);
    Route::resource('communes', 'Api\CommunesController', ['only' => ['index', 'show']]);
    Route::resource('rupturereasons', 'Api\RuptureReasonsController', ['only' => ['index']]);
    Route::resource('executionfreins', 'Api\ExecutionFreinsController', ['only' => ['index']]);
    Route::resource('rpms', 'Api\RpmsController', ['only' => ['store']]);
    Route::resource('rps', 'Api\RpsController', ['only' => ['store']]);
    Route::resource('rpcs', 'Api\RpcsController', ['only' => ['store']]);
    Route::resource('rvcs', 'Api\RvcsController', ['only' => ['store']]);
    Route::resource('actiontypes', 'Api\ActionTypesController', ['only' => ['index', 'show']]);
});

//////////////////////////////////////Debug////////////////////////////////////////////
Route::get('debug', function () {

    //$regions = \App\Models\Region::all(['nom']);
    //$wilayas = \App\Models\Region::where('nom', '=', 'Centre')->first()->wilayas()->get(['nom']);
    //$communes = \App\Models\Wilaya::where('nom', '=', 'Alger')->first()->communes()->get(['nom']);

    //$cotex = ['Essuietout', 'Papier Hygiènique', 'Serviette de table','Mouchoir Box', 'Mouchoir Etuit'];

    $now = date('Y-m-d H:i:s');
    /*$famille_id = DB::table('familles')->insertGetId([
        ['nom' => 'Essuietout', 'created_at' => $now, 'updated_at' => $now ]
    ]);*/

    //$famille_id = DB::table('roles')->insertGetId(['designation' => 'Testeur', 'created_at' => $now, 'updated_at' => $now ]);

    /////pivot test
    $marque = \App\Models\Marque::where('nom','=', 'Awane')->first();
    $familles = $marque->familles()->get(['nom']);

    //$famille = \App\Models\Famille::where('nom', '=', 'Confort Eco')->first();
    //$marques = $famille->marques()->get(['nom']);

    //dd($marque->pivot->famill);


    //return Response::json($marque->pivot->id);

    //dd($famille->marques()->where('nom', '=', 'Awane')->first()->pivot->id);
    //return $marque->familles()->where('nom', '=', 'Confort Eco')->first()->pivot->id;

    //role
    /*$admin = \App\Models\User::where('role_id', '=', \App\Models\Role::where('designation', '=', 'Administrateur')->first()->id)->first();
    $merchandisers = \App\Models\User::where('role_id', '=', \App\Models\Role::where('designation', '=', 'Merchandiser')->first()->id)->get();

    $session_id = \App\Models\Oauth_Access_Token::where('id', '=', 'hfpeJNX45q0YKHRIvmhTjRoI6tN6dNNTuen2Obir')
                    ->first()->session_id;

    $session_id = \App\Models\Oauth_Access_Token::find(1)->oauth_session()->first();*/

    //$response = HttpClient::get('https://api.apixu.com/v1/current.json?key=2d6c2275af8a4685903223602160405&q=36.7382543,3.142549499999973');

    //return Response::json($response->json());

   /* $x = \Cornford\Googlmapper\Facades\MapperFacade::map(36.7382, 3.1425);
    return $x->render();*/

    //$pdf = \Barryvdh\DomPDF  PDF::
    /*$pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();*/

    $x= '[{"2":{"quantite":"5","date":"2016-06-23T00:00:00"}},{"2":{"quantite":"5","date":"2016-06-23T00:00:00"},"5":{"quantite":"5","date":"2016-06-23T00:00:00"}},{"2":{"quantite":"5","date":"2016-06-23T00:00:00"},"5":{"quantite":"5","date":"2016-06-23T00:00:00"},"7":{"quantite":"55","date":"2016-06-23T00:00:00"}}]';
    $x = json_decode($x, true);

    dd($x);

    $s = '[{"famille":{"id":9,"nom":"Pack Eco"},"lineaire_developpe":"5"}, {"famille":{"id":10,"nom":"Pack Eco"},"lineaire_developpe":"10"}]';
    $dataLinDev = json_decode($s, true);

    $linDev = \App\Models\LinDev::where('pdv_id', '=',579)->get(array('lineaire_developpe','famille_id', 'pdv_id'))->toArray();

    foreach($dataLinDev as $key=>$row) {
        //insertion id pdv dans dataLinDev
        $dataLinDev[$key]['pdv_id']= 579;

        //famille_id
        $dataLinDev[$key]['famille_id'] = $dataLinDev[$key]["famille"]["id"];
        unset( $dataLinDev[$key]["famille"]);

        $diff = array_diff($linDev[$key], $dataLinDev[$key]);
        Log::info(json_encode($diff));
    }

    //dd($dataLinDev);

    //$dataLinDev = collect($dataLinDev);

    //dd(array_diff($linDev, $dataLinDev));;
    dd($dataLinDev);

});

Route::post('debug', function (Illuminate\Http\Request $request) {

    try {
        //photos envoyées
        if ($request->hasFile('photos')) $photos = $request['photos'];
        else $photos = null;

        foreach ($photos as $photo) {
           if(!$photo->isValid()) {
               throw new \App\Exceptions\UploadPhotoException("Incorrect file uploaded.");
           }
        }

        return "-200";
    }
    catch(\App\Exceptions\UploadPhotoException $e) {
        Log::info($e->getMessage());
        return "-900";
    }
});
