<?php

namespace App\Http\Controllers\Web;

use App\Models\Rvc;
use App\Models\RvcPhoto;
use Exception;
use FadercoHelper;
use Flash;
use HTML;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Mapper;
use Redirect;
use Response;
use PDF;

class RvcsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $data = $request->all();

            if ($request->ajax()) {

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('rvcs.id', 'pdvs.nom', 'familles.nom', 'users.nom',
                    'action_types.nom', 'mechanisms.nom', 'rvcs.created_at');

                $getColumns = array('rvcs.id', 'pdvs.nom as pdv', 'familles.nom as famille', 'users.nom as utilisateur',
                    'action_types.nom as action_type', 'mechanisms.nom as mechanism', 'rvcs.created_at as created_at');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];

                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = Rvc::count();

                        $rvcs = Rvc::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('familles', 'familles.id', '=', 'famille_id')
                            ->join('action_type_mechanism', 'action_type_mechanism.id', '=', 'action_type_mechanism_id')
                            ->join('action_types', 'action_types.id', '=', 'action_type_mechanism.action_type_id')
                            ->join('mechanisms', 'mechanisms.id', '=', 'action_type_mechanism.mechanism_id')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    } else { //recherche

                        $sTotal = Rvc::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('familles', 'familles.id', '=', 'famille_id')
                            ->join('action_type_mechanism', 'action_type_mechanism.id', '=', 'action_type_mechanism_id')
                            ->join('action_types', 'action_types.id', '=', 'action_type_mechanism.action_type_id')
                            ->join('mechanisms', 'mechanisms.id', '=', 'action_type_mechanism.mechanism_id')
                            ->where('action_types.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('mechanisms.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('pdvs.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('familles.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('users.nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $rvcs = Rvc::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('familles', 'familles.id', '=', 'famille_id')
                            ->join('action_type_mechanism', 'action_type_mechanism.id', '=', 'action_type_mechanism_id')
                            ->join('action_types', 'action_types.id', '=', 'action_type_mechanism.action_type_id')
                            ->join('mechanisms', 'mechanisms.id', '=', 'action_type_mechanism.mechanism_id')
                            ->where('action_types.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('mechanisms.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('pdvs.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('familles.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('users.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }

                    if (count($rvcs) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($rvcs as $key => $row) {
                        $rvcs[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $rvcs[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $rvcs));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('rvcs.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $rvc = Rvc::join('users', 'users.id', '=', 'user_id')
                ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                ->join('familles', 'familles.id', '=', 'famille_id')
                ->join('action_type_mechanism', 'action_type_mechanism.id', '=', 'action_type_mechanism_id')
                ->join('action_types', 'action_types.id', '=', 'action_type_mechanism.action_type_id')
                ->join('mechanisms', 'mechanisms.id', '=', 'action_type_mechanism.mechanism_id')
                ->where('rvcs.id', '=', $id)
                ->firstOrFail(array(
                    'rvcs.id',
                    'rvcs.reference',
                    'rvcs.autre',
                    'rvcs.periode',
                    'rvcs.observation',
                    'pdvs.id as pdv_id', 'pdvs.nom as pdv_nom', 'pdvs.localisation_lat as pdv_localisation_lat', 'pdvs.localisation_long as pdv_localisation_long',
                    'familles.id as famille_id', 'familles.nom as famille_nom',
                    'users.id as user_id', 'users.nom as user_nom', 'users.username as user_username',
                    'users.telephone as user_telephone', 'users.avatar as user_avatar',
                    'action_types.nom as action_type_nom',
                    'mechanisms.nom as mechanism_nom',
                    'rvcs.created_at as created_at'));


            //map render
            $content = '<h4 class="box-title"><a href="/pdvs/' . $rvc->pdv_id . '" target="_blank">' . HTML::entities($rvc->pdv_nom) . '</a></h4><p style="color:#333;">Crée par : ' . HTML::entities($rvc->user_nom) . '</p>';
            Mapper::map($rvc->pdv_localisation_lat, $rvc->pdv_localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($rvc->pdv_nom), 'content' => $content, 'animation' => 'DROP']]);
            $map = Mapper::render();

            //dd($map);
            //photos
            $photos = RvcPhoto::where('rvc_id', '=', $rvc->id)->get(array('url'));

            $photos = $photos->each(function ($item, $key) {
                if (strpos($item['url'], ".jpg") === false && strpos($item['url'], ".png") === false) {
                    $item['thumbs'] = $item['url'] . "_320x240";
                } elseif (strpos($item['url'], ".jpg") !== false) {
                    $url = str_replace(".jpg", "", $item['url']);
                    $item['thumbs'] = $url . "_320x240.jpg";
                } else {
                    $url = str_replace(".png", "", $item['url']);
                    $item['thumbs'] = $url . "_320x240.png";
                }

            });

            if (count($photos) == 0) return view('rvcs.show', compact('rvc', 'map'));


            return view('rvcs.show', compact('rvc', 'map', 'photos'));
        } catch (ModelNotFoundException $e) {
            Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/rvcs');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $rvc = Rvc::findOrfail($id);

                $path = 'images/rvcs/' . $id;

                //delete photos folder
                if (FadercoHelper::deleteDirectory($path)) {
                    $rvc->delete();

                    return Response::json('rapport merchandising supprimé', 200);
                }


                return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
            } catch (Exception $e) {
                Log::error($e->getMessage());
                return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
            }
        }

        try {
            $rvc = Rvc::findOrfail($id);

            $path = 'images/rvcs/' . $id;

            //delete photos folder
            if (FadercoHelper::deleteDirectory($path)) {
                $rvc->delete();

                Flash::overlay('Point de vente supprimé avec succès', 'Faderco Information');

                return Redirect::route('rvcs.index');
            }

            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            return Redirect::intended('/');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function pdfGenerator($id)
    {

      try {
          $rvc = Rvc::join('users', 'users.id', '=', 'user_id')
              ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
              ->join('familles', 'familles.id', '=', 'famille_id')
              ->join('action_type_mechanism', 'action_type_mechanism.id', '=', 'action_type_mechanism_id')
              ->join('action_types', 'action_types.id', '=', 'action_type_mechanism.action_type_id')
              ->join('mechanisms', 'mechanisms.id', '=', 'action_type_mechanism.mechanism_id')
              ->where('rvcs.id', '=', $id)
              ->firstOrFail(array(
                  'rvcs.id',
                  'rvcs.reference',
                  'rvcs.autre',
                  'rvcs.periode',
                  'rvcs.observation',
                  'pdvs.id as pdv_id', 'pdvs.nom as pdv_nom', 'pdvs.localisation_lat as pdv_localisation_lat', 'pdvs.localisation_long as pdv_localisation_long',
                  'familles.id as famille_id', 'familles.nom as famille_nom',
                  'users.id as user_id', 'users.nom as user_nom', 'users.username as user_username',
                  'users.telephone as user_telephone', 'users.avatar as user_avatar',
                  'action_types.nom as action_type_nom',
                  'mechanisms.nom as mechanism_nom',
                  'rvcs.created_at as created_at'));


          //map render
          $content = '<h4 class="box-title"><a href="/pdvs/' . $rvc->pdv_id . '" target="_blank">' . HTML::entities($rvc->pdv_nom) . '</a></h4><p style="color:#333;">Crée par : ' . HTML::entities($rvc->user_nom) . '</p>';
          Mapper::map($rvc->pdv_localisation_lat, $rvc->pdv_localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($rvc->pdv_nom), 'content' => $content, 'animation' => 'DROP']]);
          $map = Mapper::render();

          //dd($map);
          //photos
          $photos = RvcPhoto::where('rvc_id', '=', $rvc->id)->get(array('url'));

          $photos = $photos->each(function ($item, $key) {
              if (strpos($item['url'], ".jpg") === false && strpos($item['url'], ".png") === false) {
                  $item['thumbs'] = $item['url'] . "_320x240";
              } elseif (strpos($item['url'], ".jpg") !== false) {
                  $url = str_replace(".jpg", "", $item['url']);
                  $item['thumbs'] = $url . "_320x240.jpg";
              } else {
                  $url = str_replace(".png", "", $item['url']);
                  $item['thumbs'] = $url . "_320x240.png";
              }

          });

          if (count($photos) == 0)
          {
            $pdf = PDF::loadView('rvcs.pdf', compact('rvc', 'map'));
            return $pdf->download('rvcs.pdf');
          }

          $pdf = PDF::loadView('rvcs.pdf', compact('rvc', 'map', 'photos'));
          return $pdf->download('rvcs.pdf');
          //return view('rvcs.show', compact('rvc', 'map', 'photos'));
      } catch (ModelNotFoundException $e) {
          Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/rvcs');
      } catch (Exception $e) {
          Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/');
      }

    }
}
