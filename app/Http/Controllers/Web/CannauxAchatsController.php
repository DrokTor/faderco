<?php

namespace App\Http\Controllers\Web;

use Flash;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Redirect;
use Validator;
use Response;
use App\Models\CanalAchat;
use Exception;
use Log;

class CannauxAchatsController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {

            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('id', 'nom');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = CanalAchat::count();

                        $cannaux = CanalAchat::orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    } else {
                        //recherche
                        $sTotal = CanalAchat::where('nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $cannaux = CanalAchat::where('nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    }

                    if (count($cannaux) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($cannaux as $key => $row) {

                        $cannaux[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $cannaux[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $cannaux));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json($e->getMessage() . 'Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('cannaux.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        try {
            return view('cannaux.create');//,'familles'
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();

            CanalAchat::create($data);

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/cannaux');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }


    public function edit($id)
    {
        try {
            $canal = CanalAchat::findOrFail($id);

            return view('cannaux.edit', compact('canal'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();

            $canal = CanalAchat::findOrFail($id);
            $canal->nom = $data['nom'];

            $canal->save();

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/cannaux');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $cannaux = CanalAchat::findOrFail($id);
            $cannaux->delete();

            //versioning
            FadercoHelper::setVersioning();

            return Response::json('Canal supprimé', 200);

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }


    }

}
