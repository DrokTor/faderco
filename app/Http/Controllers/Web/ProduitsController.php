<?php

namespace App\Http\Controllers\Web;

use Flash;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Redirect;
use Validator;
use Response;
use App\Models\Produit;
use App\Models\Famille;
use App\Models\Marque;
use App\Models\Famille_Marque;
use Exception;
use Log;

class ProduitsController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',
            'up' => 'required|integer',
            'pc' => 'required|integer',
            'largeur' => 'required',
            'longueur' => 'required',
            'hauteur' => 'required',
            'marque' => 'required',
            'famille' => 'required',

        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        //
        try {

            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('produits.id', 'produits.nom as pNom', 'marques.nom as mNom', 'familles.nom as fNom', 'up', 'pc', 'largeur', 'longueur', 'hauteur');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = Produit::count();

                        $produits = Produit::join('famille_marque', 'famille_marque.id', '=', 'famille_marque_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->join('familles', 'familles.id', '=', 'famille_id')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    } else {
                        //recherche
                        $sTotal = Produit::join('famille_marque', 'famille_marque.id', '=', 'famille_marque_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->join('familles', 'familles.id', '=', 'famille_id')
                            ->where('produits.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('familles.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('up', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('pc', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('largeur', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('longueur', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('hauteur', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $produits = Produit::join('famille_marque', 'famille_marque.id', '=', 'famille_marque_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->join('familles', 'familles.id', '=', 'famille_id')
                            ->where('produits.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('familles.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('up', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('pc', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('largeur', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('longueur', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('hauteur', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    }

                    if (count($produits) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($produits as $key => $row) {

                        $produits[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $produits[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $produits));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('produits.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        try {
            $marques = Marque::all();
            return view('produits.create', compact('marques'));//,'familles'
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();

            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $marque = $data['marque'];
            $famille = $data['famille'];
            $id = Famille_Marque::where(['marque_id' => $marque, 'famille_id' => $famille])->first();
            $data['famille_marque_id'] = $id->id;
            $produit = Produit::create($data);

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/produits');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $product = Produit::with('famille_marque')->where(['id' => $id])->first();
            $product->famille_marque->marque_nom = Marque::findOrFail($product->famille_marque->marque_id)->nom;
            $product->famille_marque->famille_nom = Famille::findOrFail($product->famille_marque->famille_id)->nom;


            return view('produits.show', compact('product'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $marques = Marque::all();
            $produit = Produit::findOrFail($id);
            $pivot = Famille_Marque::findOrFail($produit->famille_marque_id);
            $marq = $pivot->marque_id;
            $fam = $pivot->famille_id;
            $familles = DB::table('marques')
                ->join('famille_marque', 'famille_marque.marque_id', '=', 'marques.id')
                ->join('familles', 'familles.id', '=', 'famille_marque.famille_id')
                ->select('*')
                ->where(['famille_marque.marque_id' => $pivot->marque_id])
                ->get();

            //dd($familles[0]->nom);
            return view('produits.edit', compact('marques', 'familles', 'produit', 'marq', 'fam'));


        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $data = $request->all();

            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $data = $request->all();
            $produit = Produit::findOrFail($id);
            $id = Famille_Marque::where(['marque_id' => $data['marque'], 'famille_id' => $data['famille']])->first();

            $produit->nom = $data['nom'];
            $produit->up = $data['up'];
            $produit->pc = $data['pc'];
            $produit->largeur = $data['largeur'];
            $produit->longueur = $data['longueur'];
            $produit->hauteur = $data['hauteur'];
            $produit->famille_marque_id = $id->id;

            $produit->save();

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/produits');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $produit = Produit::findOrFail($id);
            $produit->delete();

            //versioning
            FadercoHelper::setVersioning();

            return Response::json('Produit supprimé', 200);

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function serveFamillies($id)
    {
        try {

            if($id=='all')
            {
              $familles = Famille::all();
            }
            else
            {
              $marque = Marque::findOrFail($id);
              $familles = $marque->familles()->get();//Famille::with('marques')->where(['marque_id'=>$id])->get();//->where(['marque_id'=>$id])->get();
            }

            $tabFam = [];
            foreach ($familles as $key => $value) {
                $tabFam[] = '<option value=' . $value->id . '>' . $value->nom . '</option>';
            }
            $tabFam = count($tabFam) == 0 ? '<option>cette marque ne contient aucune famille</option>' : $tabFam;
            return $tabFam;

        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            return Redirect::intended('/');
        }

    }
}
