<?php

namespace App\Http\Controllers\Web;

use Flash;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Redirect;
use Validator;
use Response;
use App\Models\Wilaya;
use App\Models\Region;
use Exception;
use Log;

class WilayasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',
            'code' => 'required|max:255',
            'region_id' => 'required|max:255',
        ]);
    }


    public function index(Request $request)
    {
        try {

            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('wilayas.id', 'wilayas.nom', 'code', 'regions.nom as rnom');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = Wilaya::count();

                        $wilayas = Wilaya::orderBy($sOrderName, $sOrderDir)
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->orWhere('regions.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('code', 'LIKE', '%' . $sSearch . '%')
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    } else {
                        //recherche
                        $sTotal = Wilaya::join('regions', 'regions.id', '=', 'region_id')
                            ->where('wilayas.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('regions.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('code', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $wilayas = Wilaya::join('regions', 'regions.id', '=', 'region_id')
                            ->where('wilayas.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('regions.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('code', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    }

                    if (count($wilayas) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($wilayas as $key => $row) {

                        $wilayas[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $wilayas[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $wilayas));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('wilayas.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        try {
            $regions = Region::all();
            return view('wilayas.create', compact('regions'));//,'familles'
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();
            Wilaya::create($data);

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/wilayas');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }


    public function edit($id)
    {
        try {
            $wilaya = Wilaya::findOrFail($id);
            $regions = Region::all();

            return view('wilayas.edit', compact('wilaya', 'regions'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();

            $wilaya = Wilaya::findOrFail($id);
            $wilaya->nom = $data['nom'];
            $wilaya->code = $data['code'];
            $wilaya->region_id = $data['region_id'];

            $wilaya->save();

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/wilayas');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $wilayas = Wilaya::findOrFail($id);
            $wilayas->delete();

            //versioning
            FadercoHelper::setVersioning();

            return Response::json('Wilaya supprimée', 200);

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }


    }

    public function serveWilayas($id)
    {
        try {
            $region = Region::findOrFail($id);

            $wilayas = $region->wilayas()->get();//Famille::with('marques')->where(['marque_id'=>$id])->get();//->where(['marque_id'=>$id])->get();
            $tabWil = ['<option  disabled selected>Sélectionner la commune</option>'];
            foreach ($wilayas as $key => $value) {
                $tabWil[] = '<option value=' . $value->id . '>' . $value->nom . '</option>';
            }
            $tabWil = count($tabWil) == 0 ? '<option>cette région ne contient aucune wilaya</option>' : $tabWil;
            return $tabWil;

        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            return Redirect::intended('/');
        }

    }

}
