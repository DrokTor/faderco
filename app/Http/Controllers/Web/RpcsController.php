<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Famille_Marque;
use App\Models\Pc;
use App\Models\Rpc;
use FadercoHelper;
use HTML;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mapper;
use Response;
use Flash;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Exception;
use Log;
use DB;
use PDF;

class RpcsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            $data = $request->all();

            if ($request->ajax()) {

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('rpcs.id', 'pdvs.nom', 'marques.nom', 'users.nom', 'rpcs.created_at');

                $getColumns = array('rpcs.id', 'pdvs.nom as pdv', 'marques.nom as marque', 'users.nom as utilisateur', 'rpcs.created_at as created_at');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];

                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = Rpc::count();

                        $rpcs = Rpc::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    } else { //recherche

                        $sTotal = Rpc::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->where('pdvs.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('users.nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $rpcs = Rpc::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->where('pdvs.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('users.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }

                    if (count($rpcs) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($rpcs as $key => $row) {
                        $rpcs[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $rpcs[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $rpcs));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('rpcs.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $rpc = Rpc::join('users', 'users.id', '=', 'user_id')
                ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                ->join('marques', 'marques.id', '=', 'marque_id')
                ->where('rpcs.id', '=', $id)
                ->firstOrFail(array(
                    'rpcs.id',
                    'pdvs.id as pdv_id', 'pdvs.nom as pdv_nom', 'pdvs.localisation_lat as pdv_localisation_lat', 'pdvs.localisation_long as pdv_localisation_long',
                    'marques.id as marque_id', 'marques.nom as marque_nom',
                    'users.id as user_id', 'users.nom as user_nom', 'users.username as user_username',
                    'users.telephone as user_telephone', 'users.avatar as user_avatar',
                    'rpcs.created_at as created_at'));

            //map render
            $content = '<h4 class="box-title"><a href="/pdvs/' . $rpc->pdv_id . '" target="_blank">' . HTML::entities($rpc->pdv_nom) . '</a></h4><p style="color:#333;">Crée par : ' . HTML::entities($rpc->user_nom) . '</p>';
            Mapper::map($rpc->pdv_localisation_lat, $rpc->pdv_localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($rpc->pdv_nom), 'content' => $content, 'animation' => 'DROP']]);
            $map = Mapper::render();

            //pix concurrent
            $pcs = Pc::where('rpc_id', '=', $rpc->id)
                ->get(array('rpc_id',
                    'pcs.marque', 'pcs.reference', 'pcs.pad', 'pcs.pvc'));

            return view('rpcs.show', compact('rpc', 'map', 'pcs'));
        } catch (ModelNotFoundException $e) {
            Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/rps');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $rpc = Rpc::findOrfail($id);

                $rpc->delete();

                return Response::json('rapport merchandising supprimé', 200);
            } catch (Exception $e) {
                Log::error($e->getMessage());
                return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
            }
        }

        try {
            $rpc = Rpc::findOrfail($id);

            $rpc->delete();

            Flash::overlay('Point de vente supprimé avec succès', 'Faderco Information');

            return Redirect::route('rpms.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function pdfGenerator($id)
    {
      try {
          $rpc = Rpc::join('users', 'users.id', '=', 'user_id')
              ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
              ->join('marques', 'marques.id', '=', 'marque_id')
              ->where('rpcs.id', '=', $id)
              ->firstOrFail(array(
                  'rpcs.id',
                  'pdvs.id as pdv_id', 'pdvs.nom as pdv_nom', 'pdvs.localisation_lat as pdv_localisation_lat', 'pdvs.localisation_long as pdv_localisation_long',
                  'marques.id as marque_id', 'marques.nom as marque_nom',
                  'users.id as user_id', 'users.nom as user_nom', 'users.username as user_username',
                  'users.telephone as user_telephone', 'users.avatar as user_avatar',
                  'rpcs.created_at as created_at'));

          //map render
          $content = '<h4 class="box-title"><a href="/pdvs/' . $rpc->pdv_id . '" target="_blank">' . HTML::entities($rpc->pdv_nom) . '</a></h4><p style="color:#333;">Crée par : ' . HTML::entities($rpc->user_nom) . '</p>';
          Mapper::map($rpc->pdv_localisation_lat, $rpc->pdv_localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($rpc->pdv_nom), 'content' => $content, 'animation' => 'DROP']]);
          $map = Mapper::render();

          //pix concurrent
          $pcs = Pc::where('rpc_id', '=', $rpc->id)
              ->get(array('rpc_id',
                  'pcs.marque', 'pcs.reference', 'pcs.pad', 'pcs.pvc'));

          $pdf = PDF::loadView('rpcs.pdf', compact('rpc', 'map', 'pcs'));
          return $pdf->download('rpcs.pdf');

          //return view('rpcs.show', compact('rpc', 'map', 'pcs'));
      } catch (ModelNotFoundException $e) {
          Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/rps');
      } catch (Exception $e) {
          Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/');
      }
    }
}
