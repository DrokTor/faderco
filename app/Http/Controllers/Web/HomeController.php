<?php

namespace App\Http\Controllers\Web;

use App\Models\Activity;
use App\Models\Message;
use App\Models\Pdv;
use App\Models\Rpm;
use App\Models\User;
use Exception;
use Flash;
use HTML;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Mapper;
use Redirect;
use Response;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            {{}}

            $pdvs = Pdv::count();
            $rpms = Rpm::count();
            $users = User::count();
            $messages = Message::count();

            $data = collect([
                'pdvs'=> $pdvs,
                'rpms'=> $rpms,
                'users'=> $users,
                'messages'=> $messages
            ]);

            $activities = Activity::join('users', 'users.id', '=', 'user_id')
                ->orderBy('activities.id', 'desc')->take(20)
                ->get(array('activities.id as activity_id', 'message', 'users.nom as user_nom', 'users.avatar as user_avatar', 'activities.created_at'))
                ->toArray();

            //map render
            $pdvs = Pdv::join('users', 'users.id', '=', 'user_id')->get(array('pdvs.id as id', 'pdvs.nom as nom', 'localisation_lat', 'localisation_long', 'users.nom as user_nom'));

            $first = Pdv::join('users', 'users.id', '=', 'user_id')->first(array('pdvs.id as id', 'pdvs.nom as nom', 'localisation_lat', 'localisation_long', 'users.nom as user_nom'));

            $content = '<h4 class="box-title"><a href="/pdvs/'.$first->id.'" target="_blank">'.HTML::entities($first->nom).'</a></h4><p style="color:#333;">Crée par : '.HTML::entities($first->user_nom).'</p>';
            Mapper::map($first->localisation_lat, $first->localisation_long, ['zoom' => 9, 'center' => true, 'markers' => ['title' => $first->nom, 'content' => $content, 'animation' => 'DROP']]);

            foreach($pdvs as $key=>$pdv) {
                if($key != 0)
                {
                    $content = '<h4 class="box-title"><a href="/pdvs/'.$pdv->id.'" target="_blank">'.HTML::entities($pdv->nom).'</a></h4><p style="color:#333;">Crée par : '.HTML::entities($pdv->user_nom).'</p>';
                    Mapper::informationWindow($pdv->localisation_lat, $pdv->localisation_long, $content);
                }
            }

            $map = Mapper::render();

            return view('index', compact('data', 'activities', 'map'));
        }
        catch(Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
