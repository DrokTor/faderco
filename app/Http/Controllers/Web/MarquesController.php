<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\Marque;
use Flash;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Exception;
use Log;

class MarquesController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',
            'loclin' => 'required',
            'logo' => 'image'

        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {
            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('marques.id', 'marques.nom as mNom', 'familles.nom as fNom', 'loclin');//, 'familles','logo'

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = Marque::count();

                        $marques = Marque::with('familles')->select(['id', 'nom', 'loclin'])->get()->toArray();

                        foreach ($marques as $key => $value) {

                            $marques[$key]['familles'] = collect($value['familles'])->implode('nom', ',');
                            $marques[$key]['loclin'] = ($marques[$key]['loclin'] == '0') ? 'non' : 'oui';
                        }

                    } else {
                        //recherche
                        $sTotal = Marque::leftJoin('famille_marque', 'marques.id', '=', 'marque_id')
                            ->leftJoin('familles', 'familles.id', '=', 'famille_id')
                            ->where('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('familles.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orwhere('loclin', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $marques = Marque::leftJoin('famille_marque', 'marques.id', '=', 'marque_id')
                            ->leftJoin('familles', 'familles.id', '=', 'famille_id')
                            ->where('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orwhere('loclin', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('familles.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                        //dd($marques);

                        if ($marques) {
                            $marqueArr = [];
                            $j = 0;
                            $marqueArr[$j]['id'] = $marques[0]['id'];
                            $marqueArr[$j]['nom'] = $marques[0]['mNom'];
                            $marqueArr[$j]['loclin'] = ($marques[0]['loclin'] == '0') ? 'non' : 'oui';
                            $marqueArr[$j]['familles'] = $marques[0]['fNom'];

                            for ($i = 1; $i < count($marques); ++$i) {
                                if ($marques[$i]['id'] == $marqueArr[$j]['id']) {
                                    $marqueArr[$j]['familles'] = $marqueArr[$j]['familles'] . ',' . $marques[$i]['fNom'];
                                } else {
                                    $j++;
                                    $marqueArr[$j]['id'] = $marques[$i]['id'];
                                    $marqueArr[$j]['nom'] = $marques[$i]['mNom'];
                                    $marqueArr[$j]['loclin'] = ($marques[0]['loclin'] == '0') ? 'non' : 'oui';
                                    $marqueArr[$j]['familles'] = $marques[$i]['fNom'];

                                }
                            }

                            $marques = $marqueArr;
                        }

                    }

                    if (count($marques) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($marques as $key => $row) {

                        $marques[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $marques[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $marques));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }


            return view('marques.index');

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('marques.create');
        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            if ($request->hasFile('logo'))
                $photo = $request->file('logo');
            else $photo = null;

            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $marque = Marque::create($data);

            //upload photos
            if ($request->hasFile('logo') && $photo->isValid()) {

                $nom = $marque->id . '.' . $photo->getClientOriginalExtension();

                $photo->move(public_path('images/marques/'), $nom);
            }

            //versioning
            FadercoHelper::setVersioning();

            return Redirect::to('/marques');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $marque = Marque::with('familles')->find($id);
            $familles = [];
            foreach ($marque->familles as $key => $value) {
                $familles[] = $value->nom;
            }

            return view('marques.show', compact('marque', 'familles'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {

            $marque = Marque::findOrFail($id);
            return view('marques.edit', compact('marque'));

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            if ($request->hasFile('logo'))
                $photo = $request->file('logo');
            else $photo = null;

            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $marque = Marque::findOrFail($id);

            $data = $request->all();

            //upload photos
            if ($request->hasFile('logo') && $photo->isValid()) {

                //Storage::delete(public_path('images/marques/'.$marque->id.'.*'));
                //unlink(glob(public_path('images/marques/'.$marque->id.'.*')));
                array_map("unlink", glob(public_path('images/marques/' . $marque->id . '.*')));
                $nom = $marque->id . '.' . $photo->getClientOriginalExtension();

                $photo->move(public_path('images/marques/'), $nom);
            }

            if ($marque->update($data)) {

                //versioning
                FadercoHelper::setVersioning();

                return Redirect::to('/marques');//,compact('marque')
            }
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $marque = Marque::findOrFail($id);
            if ($marque->delete()) {
                //versioning
                FadercoHelper::setVersioning();

                return Response::json('Marque supprimée', 200);
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
        }
    }

    /* récupérer les marques en tant qu'options d'un select*/
    public function serveMarques()
    {
        try {


            $marques = Marque::all();

            $tabFam = [];
            foreach ($marques as $key => $value) {
                $tabFam[] = '<option value=' . $value->id . '>' . $value->nom . '</option>';
            }
            $tabFam = count($tabFam) == 0 ? '<option></option>' : $tabFam;
            return $tabFam;

        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            return Redirect::intended('/');
        }

    }
}
