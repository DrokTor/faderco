<?php

namespace App\Http\Controllers\Web;

use App\Models\Message;
use App\Models\Message_User;
use App\Models\User;
use Auth;
use Exception;
use FadercoHelper;
use Flash;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Redirect;
use Response;
use Validator;

class MessagesController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'destinataires' => 'required',
            'objet' => 'required',
            'contenu' => 'required'
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if($request->ajax()) {
                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('messages.id', 'users.nom', 'messages.objet', 'messages.created_at');

                $getColumns = array('messages.id', 'users.nom as utilisateur', 'messages.objet as objet', 'messages.created_at as created_at');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName =$aColumns[$data['iSortCol_0']];
                $sOrderDir =$data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];

                try
                {
                    if(empty($sSearch)) { //pas de recherche

                        $sTotal = Message::count();

                        $messages = Message::join('users', 'users.id', '=', 'user_id')
                            ->with(array('users' => function ($q) {
                                $q->get(array('id', 'nom'));
                            }))
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($getColumns);

                        $messages = $messages->each(function ($item, $key) {
                            $item['destinataires'] = '';
                            foreach($item->users as $user) {
                                //$item['user_'.$user->id] = $user->nom;
                                //$item['etat_'.$user->id] = $user->etat;
                                $item['destinataires'] = $item['destinataires'].$user->nom.', ';
                            }
                            $item['destinataires'] = substr($item['destinataires'], 0, strlen($item['destinataires'])-2);
                            unset($item['users']);
                        })->toArray();
                    }
                    else {
                        //recherche
                        $sTotal = Message::join('users', 'users.id', '=', 'user_id')
                            ->where('username', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('objet', 'LIKE', '%'.$sSearch.'%')
                            ->count();

                        $messages = Message::join('users', 'users.id', '=', 'user_id')
                            ->with(array('users' => function ($q) {
                                $q->get(array('id', 'nom'));
                            }))
                            ->where('username', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('objet', 'LIKE', '%'.$sSearch.'%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns);

                        $messages = $messages->each(function ($item, $key) {
                            $item['destinataires'] = '';
                            foreach($item->users as $user) {
                                //$item['user_'.$user->id] = $user->nom;
                                //$item['etat_'.$user->id] = $user->etat;
                                $item['destinataires'] = $item['destinataires'].$user->nom.', ';
                            }
                            $item['destinataires'] = substr($item['destinataires'], 0, strlen($item['destinataires'])-2);
                            unset($item['users']);
                        })->toArray();
                    }

                    if(count($messages) == 0 ) return Response::json(array('draw' => 0, 'recordsTotal'=>0, 'recordsFiltered' =>0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach($messages as $key => $row) {

                        $messages[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $messages[$key]['DT_RowId'] = 'row_'.$row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal'=>$sTotal, 'recordsFiltered' =>$sTotal, 'data' => $messages));
                }
                catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('messages.index');
        }
        catch(Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users= User::where('role_id', '!=', '1')->get();

        return view('messages.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->except('destinataires');
            $data['user_id'] = Auth::user()->id;

            $users  = $request->get('destinataires');

            $message = Message::create($data);

            foreach($users as $user) {
                Message_User::create(array('message_id'=> $message->id, 'user_id'=> $user, 'etat' => false));
            }

            //versioning
            FadercoHelper::setVersioning();

            return Redirect::route('messages.index');
        }
        catch (Exception $e)
        {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {


            /*$messages = $messages->each(function ($item, $key) {
                $item['destinataires'] = '';
                foreach($item->users as $user) {
                    //$item['user_'.$user->id] = $user->nom;
                    //$item['etat_'.$user->id] = $user->etat;
                    $item['destinataires'] = $item['destinataires'].$user->nom.', ';
                }
                $item['destinataires'] = substr($item['destinataires'], 0, strlen($item['destinataires'])-2);
                unset($item['users']);*/


            $message = Message::join('users', 'users.id', '=', 'messages.user_id')
                ->with(array('users' => function ($q) {
                    $q->get(array('id', 'nom'));
                }))
                ->where('messages.id', '=', $id)
                ->firstOrFail(array(
                    'messages.id',
                    'messages.objet',
                    'messages.contenu',
                    'users.nom as user_nom',
                    'users.email as user_email',
                    'messages.created_at'
                ));

            return view('messages.show', compact('message'));
        }
        catch (ModelNotFoundException $e) {
            Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/messages');
        }
        catch(Exception $e)
        {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            try
            {
                $message= Message::findOrfail($id);

                $message->delete();

                //versioning
                FadercoHelper::setVersioning();

                return Response::json('message supprimé', 200);
            }
            catch (Exception $e) {
                Log::error($e->getMessage());
                return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
            }
        }

        try {
            $message= Message::findOrfail($id);

            $message->delete();

            //versioning
            FadercoHelper::setVersioning();

            Flash::overlay('Message supprimé avec succès', 'Faderco Information');

            return Redirect::route('messages.index');
        }
        catch(Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }
}
