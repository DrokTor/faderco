<?php

namespace App\Http\Controllers\Web;

use App\Models\ActivityType;
use App\Models\CanalAchat;
use App\Models\LocLin;
use App\Models\Pdv;
use App\Models\PdvType;
use App\Models\PdvSurface;
use App\Models\ZoneChalandise;
use App\Models\Plv;
use App\Models\Region;
use App\Models\Wilaya;
use App\Models\Commune;
use App\Models\Marque;
use Auth;
use FadercoHelper;
use Flash;
use HTML;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Exception;
use Log;
use Mapper;
use Redirect;
use Response;


class PdvsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $data = $request->all();

            if($request->ajax()) {

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('pdvs.id', 'pdvs.nom', 'users.nom', 'regions.nom', 'wilayas.nom', 'communes.nom', 'pdvs.created_at', 'pdvs.etat');
                $getColumns = array('pdvs.id', 'pdvs.nom', 'users.nom as utilisateur', 'regions.nom as region', 'wilayas.nom as wilaya', 'communes.nom as commune', 'pdvs.created_at', 'pdvs.etat as etat');


                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName =$aColumns[$data['iSortCol_0']];
                $sOrderDir =$data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];

                try
                {
                    if(empty($sSearch)) { //pas de recherche

                        $sTotal = Pdv::count();

                        $pdvs = Pdv::join('users', 'users.id', '=', 'user_id')
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->join('communes', 'communes.id', '=', 'commune_id')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }
                    else { //recherche
                        $sTotal = Pdv::join('users', 'users.id', '=', 'user_id')
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->join('communes', 'communes.id', '=', 'commune_id')
                            ->where('pdvs.nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('users.nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('regions.nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('wilayas.nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('communes.nom', 'LIKE', '%'.$sSearch.'%')
                            ->count();


                        $pdvs = Pdv::join('users', 'users.id', '=', 'user_id')
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->join('communes', 'communes.id', '=', 'commune_id')
                            ->where('pdvs.nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('users.nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('regions.nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('wilayas.nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('communes.nom', 'LIKE', '%'.$sSearch.'%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }

                    if(count($pdvs) == 0 ) return Response::json(array('draw' => 0, 'recordsTotal'=>0, 'recordsFiltered' =>0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach($pdvs as $key => $row) {

                        $pdvs[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $pdvs[$key]['DT_RowId'] = 'row_'.$row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal'=>$sTotal, 'recordsFiltered' =>$sTotal, 'data' => $pdvs));
                }
                catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('pdvs.index');
        }
        catch(Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $pdv = Pdv::join('regions', 'regions.id', '=', 'pdvs.region_id') //with('LocLins', 'LinDevs')
                ->join('wilayas', 'wilayas.id', '=', 'pdvs.wilaya_id')
                ->join('communes', 'communes.id', '=', 'pdvs.commune_id')
                ->join('users', 'users.id', '=', 'pdvs.user_id')
                ->join('canal_achats', 'canal_achats.id', '=', 'pdvs.canal_achat_id')
                ->join('type_pdvs', 'type_pdvs.id', '=', 'pdvs.type_pdv_id')
                ->join('surface_pdvs', 'surface_pdvs.id', '=', 'pdvs.surface_pdv_id')
                ->join('zone_chalandises', 'zone_chalandises.id', '=', 'pdvs.zone_chalandise_id')
                ->where('pdvs.id', '=', $id)
                //->where('pdvs.user_id', $user_id)
                ->with('LinDevs', 'LocLins')
                ->firstOrFail(array(
                    'pdvs.id',
                    'pdvs.nom',
                    'pdvs.etat',
                    'localisation_lat',
                    'localisation_long',
                    'nb_sortie_caisse',
                    'regions.id as region_id','regions.nom as region_nom',
                    'wilayas.id as wilaya_id','wilayas.nom as wilaya_nom',
                    'communes.id as commune_id','communes.nom as commune_nom',
                    'users.id as user_id','users.nom as user_nom','users.username as user_username',
                    'users.telephone as user_telephone','users.avatar as user_avatar',
                    'canal_achats.id as canal_achat_id','canal_achats.nom as canal_achat_nom',
                    'type_pdvs.id as type_pdv_id','type_pdvs.nom as type_pdv_nom',
                    'surface_pdvs.id as surface_pdv_id','surface_pdvs.nom as surface_pdv_nom',
                    'zone_chalandises.id as zone_chalandise_id','zone_chalandises.nom as zone_chalandise_nom',
                ));

            //lin dev collection
            $lin_devs_collection = collect();

            foreach($pdv->LinDevs as $lin_dev) {
                $lin_devs_collection->push([
                    'famille'=> ['id'=> $lin_dev->id, 'nom'=> $lin_dev->nom],
                    'lineaire_developpe'=> $lin_dev->pivot->lineaire_developpe,
                ]);
            }

            //loc lin collection
            $loc_lins_collection = collect();

            foreach($pdv->LocLins as $loc_lin) {

                //init plv_in/out store collection
                $plv_in_store = collect();
                $plv_out_store = collect();

                //liste loclin_plv
                $marque_pdv = LocLin::where('id', '=', $loc_lin->pivot->id)
                    ->with('plvs')
                    ->first();

                //plvs store in/out collections
                foreach ($marque_pdv->plvs as $marque_pdv_plv) {
                    if($marque_pdv_plv->type == "in") {
                        $plv_in_store->push([
                            'id' => $marque_pdv_plv->id,
                            'nom' => $marque_pdv_plv->nom,
                            'type' => $marque_pdv_plv->type,
                            'quantite' => $marque_pdv_plv->pivot->quantite,
                            'date' => $marque_pdv_plv->pivot->date,
                        ]);
                    }
                    else {
                        $plv_out_store->push([
                            'id' => $marque_pdv_plv->id,
                            'nom' => $marque_pdv_plv->nom,
                            'type' => $marque_pdv_plv->type,
                            'quantite' => $marque_pdv_plv->pivot->quantite,
                            'date' => $marque_pdv_plv->pivot->date,
                        ]);
                    }
                }

                $loc_lins_collection->push([
                    'marque'=> ['id'=> $loc_lin->id, 'nom'=> $loc_lin->nom],
                    'duree'=> $loc_lin->pivot->duree,
                    'prix'=> $loc_lin->pivot->prix,
                    'plv_in_store' => $plv_in_store,
                    'plv_out_store' => $plv_out_store
                ]);
            }

            //data pdv collection
            $data = collect([
                'id'=> $pdv->id,
                'nom'=> $pdv->nom,
                'etat'=> $pdv->etat,
                'localisation_lat'=> $pdv->localisation_lat,
                'localisation_long'=> $pdv->localisation_long,
                'nb_sortie_caisse'=> $pdv->nb_sortie_caisse,
                'region'=> ['id'=> $pdv->region_id, 'nom'=> $pdv->region_nom],
                'wilaya'=> ['id'=> $pdv->wilaya_id, 'nom'=> $pdv->wilaya_nom],
                'commune'=> ['id'=> $pdv->commune_id, 'nom'=> $pdv->commune_nom],
                'user'=> ['id'=> $pdv->user_id, 'nom'=> $pdv->user_nom, 'username'=> $pdv->user_username, 'telephone'=> $pdv->user_telephone, 'avatar'=> $pdv->user_avatar],
                'canal_achat'=> ['id'=> $pdv->canal_achat_id, 'nom'=> $pdv->canal_achat_nom],
                'type_pdv'=> ['id'=> $pdv->type_pdv_id, 'nom'=> $pdv->type_pdv_nom],
                'surface_pdv'=> ['id'=> $pdv->surface_pdv_id, 'nom'=> $pdv->surface_pdv_nom],
                'zone_chalandise'=> ['id'=> $pdv->zone_chalandise_id, 'nom'=> $pdv->zone_chalandise_nom],
                'lin_devs' => $lin_devs_collection,
                'loc_lins' => $loc_lins_collection,
            ]);

            $plvs_in = Plv::where('type', '=', 'in')->get(array('nom'));
            $plvs_out = Plv::where('type', '=', 'out')->get(array('nom'));

            $data_marques_plvs = collect();

            foreach($loc_lins_collection as $loc_lin) {
                //plv_in all
                $data_plv_in = collect();
                foreach($plvs_in as $plv_in) {
                    $isAvailable = false;
                    foreach($loc_lin['plv_in_store'] as $plv_in_store)
                    {
                        if($plv_in['nom'] == $plv_in_store['nom']) {
                            $isAvailable = true;
                        }
                    }

                    if($isAvailable) {
                        $data_plv_in->put($plv_in['nom'], "oui");
                    }
                    else {
                        $data_plv_in->put($plv_in['nom'], "non");
                    }
                }

                //plv_out all
                $data_plv_out = collect();
                foreach($plvs_out as $plv_out) {
                    $isAvailable = false;
                    foreach($loc_lin['plv_out_store'] as $plv_out_store)
                    {
                        if($plv_out['nom'] == $plv_out_store['nom']) {
                            $isAvailable = true;
                        }
                    }

                    if($isAvailable) {
                        $data_plv_out->put($plv_out['nom'], "oui");
                    }
                    else {
                        $data_plv_out->put($plv_out['nom'], "non");
                    }
                }

                //marque
                $data_marques_plvs->push([
                   "marque" => $loc_lin['marque']['nom'],
                    "plv_in" => $data_plv_in,
                    "plv_out" => $data_plv_out
                ]);
            }

            //map render
            $content = '<h4 class="box-title"><a href="/pdvs/'.$pdv->id.'">'.HTML::entities($pdv->nom).'</a></h4><p style="color:#333;">Crée par : '.HTML::entities($pdv->user_nom).'</p>';
            Mapper::map($pdv->localisation_lat, $pdv->localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($pdv->nom), 'content' => $content, 'animation' => 'DROP']]);
            $map = Mapper::render();
            //$map = Mapper::map($pdv->localisation_lat, $pdv->localisation_long, ['markers' => ['title' => 'Title']])->render();

            return view('pdvs.show', compact('data', 'map', 'plvs_in', 'plvs_out', 'data_marques_plvs'));
        }
        catch (ModelNotFoundException $e) {
            Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/pdvs');
        }
        catch(Exception $e)
        {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      try
      {


          $pdv = Pdv::join('regions', 'regions.id', '=', 'pdvs.region_id') //with('LocLins', 'LinDevs')
              ->join('wilayas', 'wilayas.id', '=', 'pdvs.wilaya_id')
              ->join('communes', 'communes.id', '=', 'pdvs.commune_id')
              ->join('users', 'users.id', '=', 'pdvs.user_id')
              ->join('canal_achats', 'canal_achats.id', '=', 'pdvs.canal_achat_id')
              ->join('type_pdvs', 'type_pdvs.id', '=', 'pdvs.type_pdv_id')
              ->join('surface_pdvs', 'surface_pdvs.id', '=', 'pdvs.surface_pdv_id')
              ->join('zone_chalandises', 'zone_chalandises.id', '=', 'pdvs.zone_chalandise_id')
              ->where('pdvs.id', '=', $id)
              //->where('pdvs.user_id', $user_id)
              ->with('LinDevs', 'LocLins')
              ->firstOrFail(array(
                  'pdvs.id',
                  'pdvs.nom',
                  'pdvs.etat',
                  'localisation_lat',
                  'localisation_long',
                  'nb_sortie_caisse',
                  'regions.id as region_id','regions.nom as region_nom',
                  'wilayas.id as wilaya_id','wilayas.nom as wilaya_nom',
                  'communes.id as commune_id','communes.nom as commune_nom',
                  'users.id as user_id','users.nom as user_nom','users.username as user_username',
                  'users.telephone as user_telephone','users.avatar as user_avatar',
                  'canal_achats.id as canal_achat_id','canal_achats.nom as canal_achat_nom',
                  'type_pdvs.id as type_pdv_id','type_pdvs.nom as type_pdv_nom',
                  'surface_pdvs.id as surface_pdv_id','surface_pdvs.nom as surface_pdv_nom',
                  'zone_chalandises.id as zone_chalandise_id','zone_chalandises.nom as zone_chalandise_nom',
              ));

              $regions= Region::all();
              $wilayas= Wilaya::Where(['region_id'=>$pdv['region_id']])->get();
              $communes= Commune::Where(['wilaya_id'=>$pdv['wilaya_id']])->get();
              $cannaux= CanalAchat::all();
              $PdvType= PdvType::all();
              $PdvSurface= PdvSurface::all();
              $ZoneChalandises= ZoneChalandise::all();


          //lin dev collection
          $lin_devs_collection = collect();

          foreach($pdv->LinDevs as $lin_dev) {
              $lin_devs_collection->push([
                  'famille'=> ['id'=> $lin_dev->id, 'nom'=> $lin_dev->nom],
                  'lineaire_developpe'=> $lin_dev->pivot->lineaire_developpe,
              ]);
          }

          //loc lin collection
          $loc_lins_collection = collect();

          foreach($pdv->LocLins as $loc_lin) {

              //init plv_in/out store collection
              $plv_in_store = collect();
              $plv_out_store = collect();

              //liste loclin_plv
              $marque_pdv = LocLin::where('id', '=', $loc_lin->pivot->id)
                  ->with('plvs')
                  ->first();

              //plvs store in/out collections
              foreach ($marque_pdv->plvs as $marque_pdv_plv) {
                  if($marque_pdv_plv->type == "in") {
                      $plv_in_store->push([
                          'id' => $marque_pdv_plv->id,
                          'nom' => $marque_pdv_plv->nom,
                          'type' => $marque_pdv_plv->type,
                          'quantite' => $marque_pdv_plv->pivot->quantite,
                          'date' => $marque_pdv_plv->pivot->date,
                      ]);
                  }
                  else {
                      $plv_out_store->push([
                          'id' => $marque_pdv_plv->id,
                          'nom' => $marque_pdv_plv->nom,
                          'type' => $marque_pdv_plv->type,
                          'quantite' => $marque_pdv_plv->pivot->quantite,
                          'date' => $marque_pdv_plv->pivot->date,
                      ]);
                  }
              }

              $loc_lins_collection->push([
                  'marque'=> ['id'=> $loc_lin->id, 'nom'=> $loc_lin->nom],
                  'duree'=> $loc_lin->pivot->duree,
                  'prix'=> $loc_lin->pivot->prix,
                  'plv_in_store' => $plv_in_store,
                  'plv_out_store' => $plv_out_store
              ]);
          }

          //data pdv collection
          $data = collect([
              'id'=> $pdv->id,
              'nom'=> $pdv->nom,
              'etat'=> $pdv->etat,
              'localisation_lat'=> $pdv->localisation_lat,
              'localisation_long'=> $pdv->localisation_long,
              'nb_sortie_caisse'=> $pdv->nb_sortie_caisse,
              'region'=> ['id'=> $pdv->region_id, 'nom'=> $pdv->region_nom],
              'wilaya'=> ['id'=> $pdv->wilaya_id, 'nom'=> $pdv->wilaya_nom],
              'commune'=> ['id'=> $pdv->commune_id, 'nom'=> $pdv->commune_nom],
              'user'=> ['id'=> $pdv->user_id, 'nom'=> $pdv->user_nom, 'username'=> $pdv->user_username, 'telephone'=> $pdv->user_telephone, 'avatar'=> $pdv->user_avatar],
              'canal_achat'=> ['id'=> $pdv->canal_achat_id, 'nom'=> $pdv->canal_achat_nom],
              'type_pdv'=> ['id'=> $pdv->type_pdv_id, 'nom'=> $pdv->type_pdv_nom],
              'surface_pdv'=> ['id'=> $pdv->surface_pdv_id, 'nom'=> $pdv->surface_pdv_nom],
              'zone_chalandise'=> ['id'=> $pdv->zone_chalandise_id, 'nom'=> $pdv->zone_chalandise_nom],
              'lin_devs' => $lin_devs_collection,
              'loc_lins' => $loc_lins_collection,
          ]);

          $plvs_in = Plv::where('type', '=', 'in')->get(array('nom','id'));
          $plvs_out = Plv::where('type', '=', 'out')->get(array('nom','id'));

          $data_marques_plvs = collect();

          foreach($loc_lins_collection as $loc_lin) {
              //plv_in all
              $data_plv_in = collect();
              foreach($plvs_in as $plv_in) {
                  $isAvailable = false;
                  foreach($loc_lin['plv_in_store'] as $plv_in_store)
                  {
                      if($plv_in['nom'] == $plv_in_store['nom']) {
                          $isAvailable = true;
                      }
                  }

                  if($isAvailable) {
                      $data_plv_in->put($plv_in['nom'], "oui");
                  }
                  else {
                      $data_plv_in->put($plv_in['nom'], "non");
                  }
              }

              //plv_out all
              $data_plv_out = collect();
              foreach($plvs_out as $plv_out) {
                  $isAvailable = false;
                  foreach($loc_lin['plv_out_store'] as $plv_out_store)
                  {
                      if($plv_out['nom'] == $plv_out_store['nom']) {
                          $isAvailable = true;
                      }
                  }

                  if($isAvailable) {
                      $data_plv_out->put($plv_out['nom'], "oui");
                  }
                  else {
                      $data_plv_out->put($plv_out['nom'], "non");
                  }
              }

              //marque
              $data_marques_plvs->push([
                 "marque" => $loc_lin['marque']['nom'],
                  "plv_in" => $data_plv_in,
                  "plv_out" => $data_plv_out
              ]);
           }

           //map render
          $content = '<h4 class="box-title"><a href="/pdvs/'.$pdv->id.'">'.HTML::entities($pdv->nom).'</a></h4><p style="color:#333;">Crée par : '.HTML::entities($pdv->user_nom).'</p>';
          Mapper::map($pdv->localisation_lat, $pdv->localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($pdv->nom), 'content' => $content, 'animation' => 'DROP']]);
          $map = Mapper::render();
          //$map = Mapper::map($pdv->localisation_lat, $pdv->localisation_long, ['markers' => ['title' => 'Title']])->render();


          /* update data for loclin-plv */
          /*$loclinsUpdate= DB::table('pdvs')
            ->join('marques', 'marques.id', '=', 'pdvs.marque_id')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->select('users.*', 'contacts.phone', 'orders.price')
            ->get();*/

          $loclinsUpdate=LocLin::where(['pdv_id'=>$id])->with('plvs')->get()->toArray();//->toArray();

          /*
          $loclinsUpdate=LocLin::with(array('user'=>function($query){
              $query->select('id','username');
          }))->get();*/
          //dd($plvs_in->toArray());
          //dd($loclinsUpdate);
          $loclinDataUpdate=[];
          //dd(in_array('34',['34','35']));
          foreach ($loclinsUpdate as $key=>$locUpdate)
          {
            $marque=Marque::findOrFail($locUpdate['marque_id']);
            $loclinDataUpdate[$key]['marque']['nom']=$marque->nom;
            $loclinDataUpdate[$key]['marque']['id']=$marque->id;

            foreach ($locUpdate['plvs'] as $plvkey=>$plvUpdate) {

              if($plvUpdate['type']=='in')
              {
                $loclinDataUpdate[$key]['plvsIn'][$plvkey]['id']=$plvUpdate['id'];
                $loclinDataUpdate[$key]['plvsIn'][$plvkey]['nom']=$plvUpdate['nom'];
                $loclinDataUpdate[$key]['plvsIn'][$plvkey]['quantite']=$plvUpdate['pivot']['quantite'];
                $loclinDataUpdate[$key]['plvsIn'][$plvkey]['date']=$plvUpdate['pivot']['date'];
              }
              elseif($plvUpdate['type']=='out')
              {
                $loclinDataUpdate[$key]['plvsOut'][$plvkey]['id']=$plvUpdate['id'];
                $loclinDataUpdate[$key]['plvsOut'][$plvkey]['nom']=$plvUpdate['nom'];
                $loclinDataUpdate[$key]['plvsOut'][$plvkey]['quantite']=$plvUpdate['pivot']['quantite'];
                $loclinDataUpdate[$key]['plvsOut'][$plvkey]['date']=$plvUpdate['pivot']['date'];
              }


            }
            foreach ($plvs_in->toArray() as  $value) {
              //dd($loclinDataUpdate[$key]['plvsIn']);
              $exist=false;
              if(isset($loclinDataUpdate[$key]['plvsIn']))
              foreach ($loclinDataUpdate[$key]['plvsIn'] as $val) {
                //dd(['dd','dd']);
                 //dd($value);
                if(in_array($value['nom'],$val))
                {
                  $exist=true;  //['nom'=>$value['nom']];
                  //array_add($loclinDataUpdate[$key]['plvsIn'],($value['nom']));

                }
              }
              if(!$exist)
              $loclinDataUpdate[$key]['plvsIn'][]=['id'=>$value['id'] ,'nom'=>$value['nom'],'quantite'=>null,'date'=>null];
              //dd($newTab);

            }

            foreach ($plvs_out->toArray() as  $value) {
              //dd($loclinDataUpdate[$key]['plvsIn']);
              $exist=false;
              if(isset($loclinDataUpdate[$key]['plvsOut']))
              foreach ($loclinDataUpdate[$key]['plvsOut'] as $val) {
                //dd(['dd','dd']);
                 //dd($val);
                if(in_array($value['nom'],$val))
                {
                  $exist=true;  //['nom'=>$value['nom']];
                  //array_add($loclinDataUpdate[$key]['plvsIn'],($value['nom']));

                }
              }
              if(!$exist)
              $loclinDataUpdate[$key]['plvsOut'][]=['id'=>$value['id'] ,'nom'=>$value['nom'],'quantite'=>null,'date'=>null];
              //dd($newTab);

            }


          }
          //dd($plvs_in->toArray());
          //dd($loclinDataUpdate);
          return view('pdvs.edit', compact('regions','wilayas','communes','cannaux','PdvType','PdvSurface','ZoneChalandises','data', 'map', 'plvs_in', 'plvs_out', 'data_marques_plvs','loclinDataUpdate'));
      }
      catch (ModelNotFoundException $e) {
          Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/pdvs');
      }
      catch(Exception $e)
      {
          Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/');
      }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();



      if(isset($data['webupdate']))
      {
        //dd($data);
        $pdv= Pdv::findOrfail($id);

        $pdv->update($data);

        if(isset($data['lindev']))
        {
          $lindevs=$data['lindev'];
          $lindevsync=[];

          foreach ($lindevs as $key => $value) {
            $lindevsync[$key]=['lineaire_developpe'=>$value];
          }
          //dd($lindevsync);
        }
        else {
          $lindevsync=[];
        }
        $pdv->LinDevs()->sync($lindevsync);


        if(isset($data['loclin']))
        {
          /*$loclins=$data['loclin'];
          $loclinsync=[];

          foreach ($loclins as $key => $value) {
            $loclinsync[$key]=['lineaire_developpe'=>$value];
          }*/
          //dd($lindevsync);
          $loclinsync=$data['loclin'];
         }
        else {
          $loclinsync=[];
        }
        $pdv->LocLins()->sync($loclinsync);

        if(isset($data['plv']))
        {//dd($data['plv']);
          foreach ($data['plv'] as $key => $value)
          {
            $loclin=LocLin::where(['marque_id'=>$key,'pdv_id'=>$id])->first();
            //dd($value);

            foreach ($value as $key2 => $raw)
            {
              if($raw['quantite']=='' && $raw['date']=='') unset($value[$key2]);
            }
            //dd($value);
            $loclin->plvs()->sync($value);
            //dd($value);
          }
        }


        //versioning
        FadercoHelper::setVersioning();

        return Redirect::to('/pdvs/'.$id);//,compact('marque')
      }
      else
      {
        //type de validation ou désactivation
        if(isset($data['type'])) {
            $enable = false;
            $disable = false;
            if($data['type'] == "enable") {
                $enable = true;
            }
            elseif($data['type'] == "disable") {
                $disable = true;
            }
        }

        if($request->ajax()){
            try
            {
                $pdv= Pdv::findOrfail($id);

                if($enable) $pdv->etat = true;
                elseif($disable) $pdv->etat = false;

                $pdv->save();

                if($enable) {
                    //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
                    $type_activity_id = ActivityType::where('nom', '=', 'Validation')->first()->id;
                    $message = 'Vient d\'approuver un point de vente. <a href="/pdvs/'.$pdv->id.'">Consulter</a>';
                    FadercoHelper::setActivity($request->getClientIp(), null , null , $message, $type_activity_id, Auth::user()->id);
                }
                elseif ($disable) {
                    //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
                    $type_activity_id = ActivityType::where('nom', '=', 'Validation')->first()->id;
                    $message = 'Vient de désapprouver un point de vente. <a href="/pdvs/'.$pdv->id.'">Consulter</a>';
                    FadercoHelper::setActivity($request->getClientIp(), null , null , $message, $type_activity_id, Auth::user()->id);
                }

                return Response::json('pdv approuvé', 200);
            }
            catch (Exception $e) {
                return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
            }
        }

        return Redirect::intended('/');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            try
            {
                $pdv= Pdv::findOrfail($id);

                $pdv->delete();

                return Response::json('pdv supprimé', 200);
            }
            catch (Exception $e) {
                Log::error($e->getMessage());
                return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
            }
        }

        try {
            $pdv= Pdv::findOrfail($id);

            $pdv->delete();

            Flash::overlay('Point de vente supprimé avec succès', 'Faderco Information');

            return Redirect::route('pdvs.index');
        }
        catch(Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pendingIndex(Request $request)
    {
        try {
            $data = $request->all();

            if($request->ajax()) {

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('pdvs.id', 'pdvs.nom', 'users.nom', 'regions.nom', 'wilayas.nom', 'communes.nom', 'pdvs.created_at', 'pdvs.etat');
                $getColumns = array('pdvs.id', 'pdvs.nom', 'users.nom as utilisateur', 'regions.nom as region', 'wilayas.nom as wilaya', 'communes.nom as commune', 'pdvs.created_at', 'pdvs.etat as etat');


                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName =$aColumns[$data['iSortCol_0']];
                $sOrderDir =$data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];

                try
                {
                    if(empty($sSearch)) { //pas de recherche
                        $sTotal = Pdv::where('etat', '=', false)->count();

                        $pdvs = Pdv::join('users', 'users.id', '=', 'user_id')
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->join('communes', 'communes.id', '=', 'commune_id')
                            ->where('etat', '=', false)
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }
                    else { //recherche

                        $sTotal = Pdv::join('users', 'users.id', '=', 'user_id')
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->join('communes', 'communes.id', '=', 'commune_id')
                            ->where('pdvs.etat', '=', false)
                            ->where(function($query) use ($sSearch) {
                                $query->where('pdvs.nom', 'LIKE', '%'.$sSearch.'%')
                                    ->orWhere('users.nom', 'LIKE', '%'.$sSearch.'%')
                                    ->orWhere('regions.nom', 'LIKE', '%'.$sSearch.'%')
                                    ->orWhere('wilayas.nom', 'LIKE', '%'.$sSearch.'%')
                                    ->orWhere('communes.nom', 'LIKE', '%'.$sSearch.'%');
                            })
                            ->count();

                        $pdvs = Pdv::join('users', 'users.id', '=', 'user_id')
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->join('communes', 'communes.id', '=', 'commune_id')
                            ->where('pdvs.etat', '=', false)
                            ->where(function($query) use ($sSearch) {
                                $query->where('pdvs.nom', 'LIKE', '%'.$sSearch.'%')
                                    ->orWhere('users.nom', 'LIKE', '%'.$sSearch.'%')
                                    ->orWhere('regions.nom', 'LIKE', '%'.$sSearch.'%')
                                    ->orWhere('wilayas.nom', 'LIKE', '%'.$sSearch.'%')
                                    ->orWhere('communes.nom', 'LIKE', '%'.$sSearch.'%');
                            })
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }

                    if(count($pdvs) == 0 ) return Response::json(array('draw' => 0, 'recordsTotal'=>0, 'recordsFiltered' =>0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach($pdvs as $key => $row) {

                        $pdvs[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $pdvs[$key]['DT_RowId'] = 'row_'.$row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal'=>$sTotal, 'recordsFiltered' =>$sTotal, 'data' => $pdvs));
                }
                catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('pdvs.waiting');

        }
        catch(Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function advancedSearch(Request $request) {
        try {
            $data = $request->all();

            if($request->ajax()) {

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('pdvs.id', 'pdvs.nom', 'users.nom', 'regions.nom', 'wilayas.nom', 'communes.nom', 'canal_achats.nom', 'type_pdvs.nom', 'pdvs.created_at', 'pdvs.etat');
                $getColumns = array('pdvs.id', 'pdvs.nom', 'users.nom as utilisateur', 'regions.nom as region', 'wilayas.nom as wilaya', 'communes.nom as commune', 'canal_achats.nom as canal', 'type_pdvs.nom as type', 'pdvs.created_at', 'pdvs.etat as etat');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName =$aColumns[$data['iSortCol_0']];
                $sOrderDir =$data['sSortDir_0'];

                //search
                //$sSearch = $data['sSearch'];
                foreach($aColumns as $key=>$v) {
                    ${'sSearch_'.$key} = $data['sSearch_'.$key];
                }

                try
                {
                    //initialisation des variables
                    $nom = $region_id = $wilaya_id = $commune_id = $type_id = $canal_id = "";

                    if(empty($sSearch_1) and empty($sSearch_3) and empty($sSearch_4) and empty($sSearch_5) and empty($sSearch_6) and empty($sSearch_7)) { //pas de recherche

                        $sTotal = Pdv::count();

                        $pdvs = Pdv::join('users', 'users.id', '=', 'user_id')
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->join('communes', 'communes.id', '=', 'commune_id')
                            ->join('canal_achats', 'canal_achats.id', '=', 'canal_achat_id')
                            ->join('type_pdvs', 'type_pdvs.id', '=', 'type_pdv_id')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }
                    else { //recherche
                        $sTotal = Pdv::join('users', 'users.id', '=', 'user_id')
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->join('communes', 'communes.id', '=', 'commune_id')
                            ->join('canal_achats', 'canal_achats.id', '=', 'canal_achat_id')
                            ->join('type_pdvs', 'type_pdvs.id', '=', 'type_pdv_id');

                        if(!empty($sSearch_1)) $sTotal = $sTotal->where('pdvs.nom', 'LIKE', '%'.$sSearch_1.'%');
                        if(!empty($sSearch_3)) $sTotal = $sTotal->where('regions.id', 'LIKE', '%'.$sSearch_3.'%');
                        if(!empty($sSearch_4)) $sTotal = $sTotal->where('wilayas.id', 'LIKE', '%'.$sSearch_4.'%');
                        if(!empty($sSearch_5)) $sTotal = $sTotal->where('communes.id', 'LIKE', '%'.$sSearch_5.'%');
                        if(!empty($sSearch_6)) $sTotal = $sTotal->where('canal_achats.id', 'LIKE', '%'.$sSearch_6.'%');
                        if(!empty($sSearch_7)) $sTotal = $sTotal->where('type_pdvs.id', 'LIKE', '%'.$sSearch_7.'%');

                        $sTotal = $sTotal->count();


                        $pdvs = Pdv::join('users', 'users.id', '=', 'user_id')
                            ->join('regions', 'regions.id', '=', 'region_id')
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->join('communes', 'communes.id', '=', 'commune_id')
                            ->join('canal_achats', 'canal_achats.id', '=', 'canal_achat_id')
                            ->join('type_pdvs', 'type_pdvs.id', '=', 'type_pdv_id')
                            ->join('surface_pdvs', 'surface_pdvs.id', '=', 'surface_pdv_id')
                            ->join('zone_chalandises', 'zone_chalandises.id', '=', 'zone_chalandise_id');


                        if(!empty($sSearch_1)) $pdvs = $pdvs->where('pdvs.nom', 'LIKE', '%'.$sSearch_1.'%');
                        if(!empty($sSearch_3)) $pdvs = $pdvs->where('regions.id', 'LIKE', '%'.$sSearch_3.'%');
                        if(!empty($sSearch_4)) $pdvs = $pdvs->where('wilayas.id', 'LIKE', '%'.$sSearch_4.'%');
                        if(!empty($sSearch_5)) $pdvs = $pdvs->where('communes.id', 'LIKE', '%'.$sSearch_5.'%');
                        if(!empty($sSearch_6)) $pdvs = $pdvs->where('canal_achats.id', 'LIKE', '%'.$sSearch_6.'%');
                        if(!empty($sSearch_7)) $pdvs = $pdvs->where('type_pdvs.id', 'LIKE', '%'.$sSearch_7.'%');

                        $pdvs = $pdvs->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }

                    if(count($pdvs) == 0 ) return Response::json(array('draw' => 0, 'recordsTotal'=>0, 'recordsFiltered' =>0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach($pdvs as $key => $row) {

                        $pdvs[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $pdvs[$key]['DT_RowId'] = 'row_'.$row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal'=>$sTotal, 'recordsFiltered' =>$sTotal, 'data' => $pdvs));
                }
                catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            $regions= Region::all();
            $types = PdvType::all();
            $canaux = CanalAchat::all();

            return view('pdvs.advanced', compact('regions', 'types', 'canaux'));
        }
        catch(Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function servePlvs()
    {
      $plvs=Plv::all()->groupBy('type')->toArray();

      return Response::json($plvs);

    }
}
