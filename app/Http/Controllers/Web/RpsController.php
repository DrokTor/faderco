<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Famille_Marque;
use FadercoHelper;
use FadercoResponse;
use HTML;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mapper;
use Validator;
use Response;
use App\Models\Marque;
use App\Models\Rp;
use App\Models\Produit_Rp;
use Flash;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Exception;
use Log;
use DB;
use PDF;

class RpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            $data = $request->all();

            if ($request->ajax()) {

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('rps.id', 'pdvs.nom', 'marques.nom', 'users.nom', 'rps.created_at');

                $getColumns = array('rps.id', 'pdvs.nom as pdv', 'marques.nom as marque', 'users.nom as utilisateur', 'rps.created_at as created_at');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];

                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = Rp::count();

                        $rps = Rp::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    } else { //recherche

                        $sTotal = Rp::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->where('pdvs.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('users.nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();


                        $rps = Rp::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->where('pdvs.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('users.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }

                    if (count($rps) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($rps as $key => $row) {
                        $rps[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $rps[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $rps));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('rps.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $rp = Rp::join('users', 'users.id', '=', 'user_id')
                ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                ->join('marques', 'marques.id', '=', 'marque_id')
                ->where('rps.id', '=', $id)
                ->firstOrFail(array(
                    'rps.id',
                    'pdvs.id as pdv_id', 'pdvs.nom as pdv_nom', 'pdvs.localisation_lat as pdv_localisation_lat', 'pdvs.localisation_long as pdv_localisation_long',
                    'marques.id as marque_id', 'marques.nom as marque_nom',
                    'users.id as user_id', 'users.nom as user_nom', 'users.username as user_username',
                    'users.telephone as user_telephone', 'users.avatar as user_avatar',
                    'rps.created_at as created_at'));

            //map render
            $content = '<h4 class="box-title"><a href="/pdvs/' . $rp->pdv_id . '" target="_blank">' . HTML::entities($rp->pdv_nom) . '</a></h4><p style="color:#333;">Crée par : ' . HTML::entities($rp->user_nom) . '</p>';
            Mapper::map($rp->pdv_localisation_lat, $rp->pdv_localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($rp->pdv_nom), 'content' => $content, 'animation' => 'DROP']]);
            $map = Mapper::render();

            //product-rpm
            $produits_rp = Produit_Rp::join('produits', 'produits.id', '=', 'produit_id')
                ->where('rp_id', '=', $rp->id)
                ->get(array('produit_id', 'rp_id',
                    'produit_rp.pad', 'produit_rp.pvc', 'produits.nom as produit_nom', 'famille_marque_id'));

            $data_collection = collect();
            foreach ($produits_rp as $produit_rp) {

                $famille = Famille_Marque::find($produit_rp->famille_marque_id)->famille->nom;

                $data_collection->push([
                    'famille' => $famille,
                    'produit' => ['produit_nom' => $produit_rp->produit_nom, 'pad' => $produit_rp->pad, 'pvc' => $produit_rp->pvc,
                    ],
                ]);
            }

            $data_collection = $data_collection->groupBy('famille');


            return view('rps.show', compact('rp', 'map', 'data_collection'));
        } catch (ModelNotFoundException $e) {
            Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/rps');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $rp = Rp::findOrfail($id);

                $rp->delete();

                return Response::json('rapport merchandising supprimé', 200);
            } catch (Exception $e) {
                Log::error($e->getMessage());
                return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
            }
        }

        try {
            $rp = Rp::findOrfail($id);

            $rp->delete();

            Flash::overlay('Point de vente supprimé avec succès', 'Faderco Information');

            return Redirect::route('rpms.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function pdfGenerator($id)
    {
      try {
          $rp = Rp::join('users', 'users.id', '=', 'user_id')
              ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
              ->join('marques', 'marques.id', '=', 'marque_id')
              ->where('rps.id', '=', $id)
              ->firstOrFail(array(
                  'rps.id',
                  'pdvs.id as pdv_id', 'pdvs.nom as pdv_nom', 'pdvs.localisation_lat as pdv_localisation_lat', 'pdvs.localisation_long as pdv_localisation_long',
                  'marques.id as marque_id', 'marques.nom as marque_nom',
                  'users.id as user_id', 'users.nom as user_nom', 'users.username as user_username',
                  'users.telephone as user_telephone', 'users.avatar as user_avatar',
                  'rps.created_at as created_at'));

          //map render
          $content = '<h4 class="box-title"><a href="/pdvs/' . $rp->pdv_id . '" target="_blank">' . HTML::entities($rp->pdv_nom) . '</a></h4><p style="color:#333;">Crée par : ' . HTML::entities($rp->user_nom) . '</p>';
          Mapper::map($rp->pdv_localisation_lat, $rp->pdv_localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($rp->pdv_nom), 'content' => $content, 'animation' => 'DROP']]);
          $map = Mapper::render();

          //product-rpm
          $produits_rp = Produit_Rp::join('produits', 'produits.id', '=', 'produit_id')
              ->where('rp_id', '=', $rp->id)
              ->get(array('produit_id', 'rp_id',
                  'produit_rp.pad', 'produit_rp.pvc', 'produits.nom as produit_nom', 'famille_marque_id'));

          $data_collection = collect();
          foreach ($produits_rp as $produit_rp) {

              $famille = Famille_Marque::find($produit_rp->famille_marque_id)->famille->nom;

              $data_collection->push([
                  'famille' => $famille,
                  'produit' => ['produit_nom' => $produit_rp->produit_nom, 'pad' => $produit_rp->pad, 'pvc' => $produit_rp->pvc,
                  ],
              ]);
          }

          $data_collection = $data_collection->groupBy('famille');

          $pdf = PDF::loadView('rps.pdf', compact('rp', 'map', 'data_collection'));
          return $pdf->download('rps.pdf');

          //return view('rps.show', compact('rp', 'map', 'data_collection'));
      } catch (ModelNotFoundException $e) {
          Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/rps');
      } catch (Exception $e) {
          Flash::overlay($e->getMessage().'Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/');
      }
    }
}
