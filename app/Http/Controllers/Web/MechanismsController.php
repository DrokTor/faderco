<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\Mechanism;
use App\Models\ActionType;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Redirect;
use Exception;
use Log;

class mechanismsController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',

        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('mechanisms.id', 'mechanisms.nom as fNom', 'action_types.nom as mNom');//, 'mechanisms','logo'

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = Mechanism::count();

                        $mechanisms = Mechanism::with('actiontypes')->select(['id', 'nom'])
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($aColumns)
                            ->toArray();

                        foreach ($mechanisms as $key => $value) {

                            $mechanisms[$key]['actiontypes'] = collect($value['actiontypes'])->implode('nom', ', ');
                        }


                    } else {
                        //recherche
                        $sTotal = Mechanism::leftJoin('action_type_mechanism', 'mechanisms.id', '=', 'mechanism_id')
                            ->leftJoin('action_types', 'action_types.id', '=', 'action_type_id')
                            ->where('action_types.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('mechanisms.nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $mechanisms = Mechanism::leftJoin('action_type_mechanism', 'mechanisms.id', '=', 'mechanism_id')
                            ->leftJoin('action_types', 'action_types.id', '=', 'action_type_id')
                            ->where('action_types.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('mechanisms.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                        //dd($mechanisms);

                        if ($mechanisms) {
                            $mechanismArr = [];
                            $j = 0;
                            $mechanismArr[$j]['id'] = $mechanisms[0]['id'];
                            $mechanismArr[$j]['nom'] = $mechanisms[0]['fNom'];
                            $mechanismArr[$j]['actiontypes'] = $mechanisms[0]['mNom'];

                            for ($i = 1; $i < count($mechanisms); ++$i) {
                                if ($mechanisms[$i]['id'] == $mechanismArr[$j]['id']) {
                                    $mechanismArr[$j]['actiontypes'] = $mechanismArr[$j]['actiontypes'] . ',' . $mechanisms[$i]['mNom'];
                                } else {
                                    $j++;
                                    $mechanismArr[$j]['id'] = $mechanisms[$i]['id'];
                                    $mechanismArr[$j]['nom'] = $mechanisms[$i]['fNom'];
                                    $mechanismArr[$j]['actiontypes'] = $mechanisms[$i]['mNom'];

                                }
                            }

                            $mechanisms = $mechanismArr;
                        }

                    }

                    if (count($mechanisms) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($mechanisms as $key => $row) {

                        $mechanisms[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $mechanisms[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $mechanisms));
                } catch (Exception $e) {
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }


            return view('mechanisms.index');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $actiontypes = ActionType::All();
            return view('mechanisms.create', compact('actiontypes'));

        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();

            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $mechanism = Mechanism::create($data);
            $actiontypes = isset($data['actiontypes'])?$data['actiontypes']:null;
            if($actiontypes)
            foreach ($actiontypes as $key => $value) {
                $typeaction = ActionType::findOrFail($key);
                $typeaction->mechanisms()->attach($mechanism);
            }

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/mechanisms');//view('mechanisms.view',compact('mechanism'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $mechanism = Mechanism::with('actiontypes')->find($id);//with('actiontypes')->
            $actiontypes = [];
            foreach ($mechanism->actiontypes as $key => $value) {
                $actiontypes[] = $value->nom;
            }

            return view('mechanisms.show', compact('mechanism', 'actiontypes'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $actiontypes = ActionType::All();
            $mechanism = Mechanism::with('actiontypes')->find($id);
            $attached = [];
            foreach ($mechanism->actiontypes as $key => $value) {
                $attached[] = $value->id;
            }
            return view('mechanisms.edit', compact('mechanism', 'actiontypes', 'attached'));

        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $data = $request->all();

            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $mechanism = Mechanism::findOrFail($id);

            $data = $request->all();
            $actiontypes = isset($data['actiontypes'])?$data['actiontypes']:null;
            $vals = [];
            if($actiontypes)
            foreach ($actiontypes as $key => $value) {
                $vals[] = $key;
            }
            //print_r($vals);exit();
            $mechanism->update($data);
            $mechanism->actiontypes()->sync($vals);
            /*$new_typeaction= ActionType::findOrFail($data['action_type_id']);
            $old_typeaction= ActionType::findOrFail($data['old_action_type_id']);

            //$mechanism= Mechanism::findOrFail($id);

            $old_typeaction->mechanisms()->detach($mechanism);
            $new_typeaction->mechanisms()->attach($mechanism);*/

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/mechanisms');
            //return view('mechanisms.view',compact('mechanism'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $mechanism = Mechanism::findOrFail($id);
            if ($mechanism->delete()) {

                //versioning
                FadercoHelper::setVersioning();

                return Response::json('mécanisme supprimé', 200);
            }
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }
}
