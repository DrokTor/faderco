<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\ActivityType;
use App\Models\Role;
use App\Models\User;
use App\Models\Oauth;
use FadercoHelper;
use Flash;
use Hash;
use Log;
use Redirect;
use Illuminate\Http\Request;
use Response;
use Validator;
use Exception;
use Auth;


class UsersController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',
            'username' => 'required|min:4|max:255|unique:users',
            'password' => 'required|min:4',
            'telephone' => 'required|min:10',
            'email' => 'required|email',
            'secteur' => 'required',
            'fonction' => 'required',
            'responsable' => 'required',
            'role_id' => 'required'
        ]);
    }

    public function index(Request $request)
    {
        try {

            if($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('users.id', 'nom', 'username', 'telephone', 'email', 'secteur', 'fonction', 'responsable', 'designation', 'users.updated_at');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName =$aColumns[$data['iSortCol_0']];
                $sOrderDir =$data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];
                
                
                try
                {
                    if(empty($sSearch)) { //pas de recherche

                        $sTotal = User::count();

                        $users = User::join('roles', 'roles.id', '=', 'role_id')
                                ->orderBy($sOrderName, $sOrderDir)
                                ->take($sLimit)
                                ->skip($sStart)
                                ->get($aColumns)
                                ->toArray();
                    }
                    else {
                        //recherche
                        $sTotal = User::join('roles', 'roles.id', '=', 'role_id')
                            ->where('nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('username', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('telephone', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('email', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('secteur', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('fonction', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('responsable', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('designation', 'LIKE', '%'.$sSearch.'%')
                            ->count();

                        $users = User::join('roles', 'roles.id', '=', 'role_id')
                            ->where('nom', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('username', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('telephone', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('email', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('secteur', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('fonction', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('responsable', 'LIKE', '%'.$sSearch.'%')
                            ->orWhere('designation', 'LIKE', '%'.$sSearch.'%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    }

                    if(count($users) == 0 ) return Response::json(array('draw' => 0, 'recordsTotal'=>0, 'recordsFiltered' =>0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach($users as $key => $row) {

                        $users[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $users[$key]['DT_RowId'] = 'row_'.$row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal'=>$sTotal, 'recordsFiltered' =>$sTotal, 'data' => $users));
                }
                catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('users.index');
        }
        catch(Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function show($id)
    {
        try
        {
            $user = User::with('role')->find($id);

            return view('users.show', compact('user'));
        }
        catch(Exception $e)
        {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function create()
    {
        try {
            $roles = Role::all();
            $options[] = array('id' =>'0', 'value' => 'Selectionner un role');

            foreach ($roles as $role) {
                array_push($options, array('id' =>$role->id, 'value' => $role->designation));
            }
            return view('users.create', compact('options','roles'));
        }
        catch(Exception $e)
        {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();
            $data['password'] = bcrypt($data['password']);
            $data['avatar'] = 'assets/img/avatar_default.png';

            $user = User::create($data);

            //oauth client
            $id = base64_encode($user->username);
            $secret = md5($user->username.'overgen'.$user->username);

            Oauth::create(array('id' => $id, 'secret' => $secret, 'name' => $user->username));

            //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
            $type_activity_id = ActivityType::where('nom', '=', 'Insertion')->first()->id;
            $message = 'Vient d\'ajouter un nouveau utilisateur. <a href="/users">Consulter</a>';
            FadercoHelper::setActivity($request->getClientIp(), null , null , $message, $type_activity_id, $user->id);

            return Redirect::route('users.index');
        }
        catch (Exception $e)
        {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function edit($id)
    {
      try
      {
        $user= User::findOrfail($id);
        $roles = Role::all();

        return view('users.edit',compact('user','roles'));
      }
      catch(Exception $e)
      {
          Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/');
      }
    }

    public function update(Request $request,$id)
    {
        try
        {
            $user= User::findOrfail($id);

            $oauth_id = base64_encode($user->username);
            $oauth_client = Oauth::findOrFail($oauth_id);

            $rules = [
                'nom' => 'required|max:255',
                'username' => 'required|min:4|max:255|unique:users,username,'.$user->id,
                'password' => 'required|min:4',
                'telephone' => 'required|min:10',
                'email' => 'required|email',
                'secteur' => 'required',
                'fonction' => 'required',
                'responsable' => 'required',
                'role_id' => 'required'
            ];


            if(!$request->has('password')) $request->merge(array('password' => $user->password));

            $validator = Validator::make($request->toArray(), $rules);

            if($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            if (Hash::needsRehash($request['password']))
            {
                $request['password'] = Hash::make($request['password']);
            }

            $user->nom=$request['nom'];
            $user->username=$request['username'];
            $user->password=$request['password'];
            $user->telephone=$request['telephone'];
            $user->email=$request['email'];
            $user->secteur=$request['secteur'];
            $user->fonction=$request['fonction'];
            $user->responsable=$request['responsable'];
            $user->role_id=$request['role_id'];

            $user->save();

            //oauth client
            $id = base64_encode($user->username);
            $secret = md5($user->username.'overgen'.$user->username);

            $oauth_client->id = $id;
            $oauth_client->secret = $secret;
            $oauth_client->name = $user->username;
            $oauth_client->save();

            return Redirect::route('users.index');
        }
        catch (Exception $e)
        {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    public function destroy($id)
    {
        try
        {
            $user= User::findOrfail($id);
            $oauth_id = base64_encode($user->username);
            $oauth_client = Oauth::findOrFail($oauth_id);

            $user->delete();
            $oauth_client->delete();

            return Response::json('utilisateur supprimée', 200);
        }
        catch (Exception $e) {
            Log::error($e->getMessage());
            return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
        }
    }
}
