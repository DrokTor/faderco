<?php

namespace App\Http\Controllers\Web;

use App\Models\Famille_Marque;
use App\Models\Produit_Rpm;
use App\Models\Rpm;
use App\Models\RpmPhoto;
use Exception;
use FadercoHelper;
use Flash;
use HTML;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Mapper;
use PDF;
use Redirect;
use Response;
use Validator;
use View;

class RpmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $data = $request->all();

            if ($request->ajax()) {

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('rpms.id', 'pdvs.nom', 'marques.nom', 'users.nom',
                    'execution_freins.nom', 'assortiment_recommande', 'bloc_produit', 'planogramme', 'mise_avant_tg',
                    'rpms.created_at');

                $getColumns = array('rpms.id', 'pdvs.nom as pdv', 'marques.nom as marque', 'users.nom as utilisateur',
                    'execution_freins.nom as execution_frein', 'assortiment_recommande', 'bloc_produit', 'planogramme', 'mise_avant_tg',
                    'rpms.created_at as created_at');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];

                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = Rpm::count();

                        $rpms = Rpm::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->join('execution_freins', 'execution_freins.id', '=', 'execution_frein_id')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    } else { //recherche

                        $sTotal = Rpm::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->join('execution_freins', 'execution_freins.id', '=', 'execution_frein_id')
                            ->where('execution_freins.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('observation', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('pdvs.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('users.nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();


                        $rpms = Rpm::join('users', 'users.id', '=', 'user_id')
                            ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                            ->join('marques', 'marques.id', '=', 'marque_id')
                            ->join('execution_freins', 'execution_freins.id', '=', 'execution_frein_id')
                            ->where('execution_freins.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('observation', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('pdvs.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('users.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($getColumns)
                            ->toArray();
                    }

                    if (count($rpms) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($rpms as $key => $row) {
                        $rpms[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $rpms[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $rpms));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('rpms.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $rpm = Rpm::join('users', 'users.id', '=', 'user_id')
                ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
                ->join('marques', 'marques.id', '=', 'marque_id')
                ->join('execution_freins', 'execution_freins.id', '=', 'execution_frein_id')
                ->where('rpms.id', '=', $id)
                ->firstOrFail(array(
                    'rpms.id',
                    'pdvs.id as pdv_id', 'pdvs.nom as pdv_nom', 'pdvs.localisation_lat as pdv_localisation_lat', 'pdvs.localisation_long as pdv_localisation_long',
                    'marques.id as marque_id', 'marques.nom as marque_nom',
                    'users.id as user_id', 'users.nom as user_nom', 'users.username as user_username',
                    'users.telephone as user_telephone', 'users.avatar as user_avatar',
                    'execution_freins.nom as execution_frein', 'assortiment_recommande', 'bloc_produit', 'planogramme', 'mise_avant_tg', 'observation',
                    'rpms.created_at as created_at'));

            //map render
            $content = '<h4 class="box-title"><a href="/pdvs/' . $rpm->pdv_id . '" target="_blank">' . HTML::entities($rpm->pdv_nom) . '</a></h4><p style="color:#333;">Crée par : ' . HTML::entities($rpm->user_nom) . '</p>';
            Mapper::map($rpm->pdv_localisation_lat, $rpm->pdv_localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($rpm->pdv_nom), 'content' => $content, 'animation' => 'DROP']]);
            $map = Mapper::render();

            //product-rpm
            $produits_rpm = Produit_Rpm::join('produits', 'produits.id', '=', 'produit_id')
                ->leftJoin('rupture_reasons', 'rupture_reasons.id', '=', 'rupture_reason_id')
                ->where('rpm_id', '=', $rpm->id)
                ->get(array('produit_id', 'rpm_id',
                    'produit_rpm.up', 'produit_rpm.pc', 'produit_rpm.pvc', 'produit_rpm.facing', 'produit_rpm.facing_ma', 'produit_rpm.dispo', 'produit_rpm.stock_reserve', 'produit_rpm.rupture_reason_id as rupture_reason_id', 'rupture_reasons.nom as rupture_reason_nom', 'produits.nom as produit_nom', 'famille_marque_id'));

            $data_collection = collect();
            foreach ($produits_rpm as $produit_rpm) {

                $famille = Famille_Marque::find($produit_rpm->famille_marque_id)->famille->nom;

                $data_collection->push([
                    'famille' => $famille,
                    'produit' => ['produit_nom' => $produit_rpm->produit_nom, 'up' => $produit_rpm->up, 'pc' => $produit_rpm->pc,
                        'pvc' => $produit_rpm->pvc, 'facing' => $produit_rpm->facing, 'facing_ma' => $produit_rpm->facing_ma,
                        'dispo' => $produit_rpm->dispo, 'stock_reserve' => $produit_rpm->stock_reserve, 'rupture_reason_nom' => $produit_rpm->rupture_reason_nom, /*'rupture_reason_id' =>$produit_rpm->rupture_reason_id*/
                    ],
                ]);
            }

            $data_collection = $data_collection->groupBy('famille');

            //photos
            $photos = RpmPhoto::where('rpm_id', '=', $rpm->id)->get(array('url'));

            if (count($photos) == 0) return view('rpms.show', compact('rpm', 'map', 'data_collection'));

            //dd(View::make('rpms.show')->with(array('rpm'=> $rpm, 'map'=> $map, 'data_collection'=> $data_collection))->render());

            //$pdf = PDF::loadView('auth.login', array('rpm'=> $rpm, 'map'=> $map, 'data_collection'=> $data_collection));
            //return $pdf->stream();
            //return $pdf->download('rpm.pdf');

            return view('rpms.show', compact('rpm', 'map', 'data_collection', 'photos'));
        } catch (ModelNotFoundException $e) {
            Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/rpms');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            try {
                $rpm = Rpm::findOrfail($id);

                $path = 'images/rpms/' . $id;

                //delete photos folder
                if (FadercoHelper::deleteDirectory($path)) {
                    $rpm->delete();

                    return Response::json('rapport merchandising supprimé', 200);
                }

                return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
            } catch (Exception $e) {
                Log::error($e->getMessage());
                return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
            }
        }

        try {
            $rpm = Rpm::findOrfail($id);

            $path = 'images/rpms/' . $id;

            //delete photos folder
            if (FadercoHelper::deleteDirectory($path)) {
                $rpm->delete();

                Flash::overlay('Point de vente supprimé avec succès', 'Faderco Information');

                return Redirect::route('rpms.index');
            }

            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            return Redirect::intended('/');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    public function pdfGenerator($id)
    {

      try {
          $rpm = Rpm::join('users', 'users.id', '=', 'user_id')
              ->join('pdvs', 'pdvs.id', '=', 'pdv_id')
              ->join('marques', 'marques.id', '=', 'marque_id')
              ->join('execution_freins', 'execution_freins.id', '=', 'execution_frein_id')
              ->where('rpms.id', '=', $id)
              ->firstOrFail(array(
                  'rpms.id',
                  'pdvs.id as pdv_id', 'pdvs.nom as pdv_nom', 'pdvs.localisation_lat as pdv_localisation_lat', 'pdvs.localisation_long as pdv_localisation_long',
                  'marques.id as marque_id', 'marques.nom as marque_nom',
                  'users.id as user_id', 'users.nom as user_nom', 'users.username as user_username',
                  'users.telephone as user_telephone', 'users.avatar as user_avatar',
                  'execution_freins.nom as execution_frein', 'assortiment_recommande', 'bloc_produit', 'planogramme', 'mise_avant_tg', 'observation',
                  'rpms.created_at as created_at'));

          //map render
          $content = '<h4 class="box-title"><a href="/pdvs/' . $rpm->pdv_id . '" target="_blank">' . HTML::entities($rpm->pdv_nom) . '</a></h4><p style="color:#333;">Crée par : ' . HTML::entities($rpm->user_nom) . '</p>';
          Mapper::map($rpm->pdv_localisation_lat, $rpm->pdv_localisation_long, ['zoom' => 10, 'markers' => ['title' => HTML::entities($rpm->pdv_nom), 'content' => $content, 'animation' => 'DROP']]);
          $map = Mapper::render();

          //product-rpm
          $produits_rpm = Produit_Rpm::join('produits', 'produits.id', '=', 'produit_id')
              ->leftJoin('rupture_reasons', 'rupture_reasons.id', '=', 'rupture_reason_id')
              ->where('rpm_id', '=', $rpm->id)
              ->get(array('produit_id', 'rpm_id',
                  'produit_rpm.up', 'produit_rpm.pc', 'produit_rpm.pvc', 'produit_rpm.facing', 'produit_rpm.facing_ma', 'produit_rpm.dispo', 'produit_rpm.stock_reserve', 'produit_rpm.rupture_reason_id as rupture_reason_id', 'rupture_reasons.nom as rupture_reason_nom', 'produits.nom as produit_nom', 'famille_marque_id'));

          $data_collection = collect();
          foreach ($produits_rpm as $produit_rpm) {

              $famille = Famille_Marque::find($produit_rpm->famille_marque_id)->famille->nom;

              $data_collection->push([
                  'famille' => $famille,
                  'produit' => ['produit_nom' => $produit_rpm->produit_nom, 'up' => $produit_rpm->up, 'pc' => $produit_rpm->pc,
                      'pvc' => $produit_rpm->pvc, 'facing' => $produit_rpm->facing, 'facing_ma' => $produit_rpm->facing_ma,
                      'dispo' => $produit_rpm->dispo, 'stock_reserve' => $produit_rpm->stock_reserve, 'rupture_reason_nom' => $produit_rpm->rupture_reason_nom, /*'rupture_reason_id' =>$produit_rpm->rupture_reason_id*/
                  ],
              ]);
          }

          $data_collection = $data_collection->groupBy('famille');

          //photos
          $photos = RpmPhoto::where('rpm_id', '=', $rpm->id)->get(array('url'));

          if (count($photos) == 0)
          {
            $pdf = PDF::loadView('rpms.pdf', compact('rpm', 'map','data_collection'));
            return $pdf->download('rpms.pdf');
          } //return view('rpms.show', compact('rpm', 'map', 'data_collection'));

          //dd(View::make('rpms.show')->with(array('rpm'=> $rpm, 'map'=> $map, 'data_collection'=> $data_collection))->render());

          //$pdf = PDF::loadView('auth.login', array('rpm'=> $rpm, 'map'=> $map, 'data_collection'=> $data_collection));
          //return $pdf->stream();
          //return $pdf->download('rpm.pdf');
          //dd($photos);
          //return view('rpms.show', compact('rpm', 'map', 'data_collection', 'photos'));
          $pdf = PDF::loadView('rpms.pdf', compact('rpm', 'map','data_collection', 'photos'));
          return $pdf->download('rpms.pdf');

      } catch (ModelNotFoundException $e) {
          Flash::overlay('Votre requete n\'a retourné aucun résultat', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/rpms');
      } catch (Exception $e) {
          Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
          Log::error($e->getMessage());
          return Redirect::intended('/');
      }



        //$pdf = PDF::loadView('rpms.pdf');
        //return $pdf->stream();
        //return $pdf->download('rpm.pdf');
        //return view('rpms.pdf');
    }

}
