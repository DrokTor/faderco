<?php

namespace App\Http\Controllers\Web;

use Flash;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Redirect;
use Validator;
use Response;
use App\Models\Commune;
use App\Models\Wilaya;
use Exception;
use Log;

class communesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',
            'code' => 'required|max:255',
            'wilaya_id' => 'required|max:255',
        ]);
    }


    public function index(Request $request)
    {
        try {

            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('communes.id', 'communes.nom', 'communes.code', 'wilayas.nom as rnom');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = commune::count();

                        $communes = commune::orderBy($sOrderName, $sOrderDir)
                            ->join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->orWhere('communes.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('communes.code', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('wilayas.nom', 'LIKE', '%' . $sSearch . '%')
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    } else {
                        //recherche
                        $sTotal = commune::join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->where('communes.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('communes.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('communes.code', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('wilayas.nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $communes = commune::join('wilayas', 'wilayas.id', '=', 'wilaya_id')
                            ->where('communes.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('communes.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('communes.code', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('wilayas.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    }

                    if (count($communes) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($communes as $key => $row) {

                        $communes[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $communes[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $communes));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json($e->getMessage() . 'Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('communes.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        try {
            $wilayas = wilaya::all();
            return view('communes.create', compact('wilayas'));//,'familles'
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();
            commune::create($data);

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/communes');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }


    public function edit($id)
    {
        try {
            $commune = commune::findOrFail($id);
            $communes = wilaya::all();

            return view('communes.edit', compact('commune', 'communes'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();

            $commune = commune::findOrFail($id);
            $commune->nom = $data['nom'];
            $commune->code = $data['code'];
            $commune->wilaya_id = $data['wilaya_id'];

            $commune->save();

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/communes');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $communes = commune::findOrFail($id);
            $communes->delete();

            //versioning
            FadercoHelper::setVersioning();

            return Response::json('Commune supprimée', 200);

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }


    }

    public function serveCommunes($id)
    {
        try {
            $wilaya = Wilaya::findOrFail($id);

            $communes = $wilaya->communes()->get();//Famille::with('marques')->where(['marque_id'=>$id])->get();//->where(['marque_id'=>$id])->get();
            $tabCom = [];
            foreach ($communes as $key => $value) {
                $tabCom[] = '<option value=' . $value->id . '>' . $value->nom . '</option>';
            }
            $tabCom = count($tabCom) == 0 ? '<option>cette wilaya ne contient aucune commune</option>' : $tabCom;
            return $tabCom;

        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            return Redirect::intended('/');
        }

    }

}
