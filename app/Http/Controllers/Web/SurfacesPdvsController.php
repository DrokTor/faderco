<?php

namespace App\Http\Controllers\Web;

use Flash;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Redirect;
use Validator;
use Response;
use App\Models\PdvSurface;
use Exception;
use Log;

class SurfacesPdvsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',
        ]);
    }


    public function index(Request $request)
    {
        //
        try {

            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('id', 'nom');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = PdvSurface::count();

                        $surfaces = PdvSurface::orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    } else {
                        //recherche
                        $sTotal = PdvSurface::where('nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $surfaces = PdvSurface::where('nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    }

                    if (count($surfaces) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($surfaces as $key => $row) {

                        $surfaces[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $surfaces[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $surfaces));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('surfaces.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        try {
            return view('surfaces.create');//,'familles'
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();
            PdvSurface::create($data);

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/surfaces');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }


    public function edit($id)
    {
        try {
            $surface = PdvSurface::findOrFail($id);
            return view('surfaces.edit', compact('surface'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();

            $surface = PdvSurface::findOrFail($id);
            $surface->nom = $data['nom'];

            $surface->save();

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/surfaces');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $surfaces = PdvSurface::findOrFail($id);
            $surfaces->delete();

            //versioning
            FadercoHelper::setVersioning();

            return Response::json('Surface supprimée', 200);

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

}
