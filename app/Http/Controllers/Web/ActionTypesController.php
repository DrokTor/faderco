<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\ActionType;
use Flash;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Exception;
use Log;

class actionTypesController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',


        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {
            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('action_types.id', 'action_types.nom as mNom', 'mechanisms.nom as fNom');//, 'mechanisms','logo'

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = ActionType::count();

                        $actiontypes = ActionType::with('mechanisms')->select(['id', 'nom'])->get()->toArray();

                        foreach ($actiontypes as $key => $value) {
                            if(isset($actiontypes[$key]['mechanisms']))
                            $actiontypes[$key]['mechanisms'] = collect($value['mechanisms'])->implode('nom', ',');
                        }

                    } else {
                        //recherche
                        $sTotal = ActionType::leftJoin('action_type_mechanism', 'action_types.id', '=', 'action_type_id')
                            ->leftJoin('mechanisms', 'mechanisms.id', '=', 'mechanism_id')
                            ->where('action_types.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('mechanisms.nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $actiontypes = ActionType::leftJoin('action_type_mechanism', 'action_types.id', '=', 'action_type_id')
                            ->leftJoin('mechanisms', 'mechanisms.id', '=', 'mechanism_id')
                            ->where('action_types.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('mechanisms.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                        //dd($actiontypes);

                        if ($actiontypes) {
                            $actiontypeArr = [];
                            $j = 0;
                            $actiontypeArr[$j]['id'] = $actiontypes[0]['id'];
                            $actiontypeArr[$j]['nom'] = $actiontypes[0]['mNom'];
                            $actiontypeArr[$j]['mechanisms'] = $actiontypes[0]['fNom'];

                            for ($i = 1; $i < count($actiontypes); ++$i) {
                                if ($actiontypes[$i]['id'] == $actiontypeArr[$j]['id']) {
                                    $actiontypeArr[$j]['mechanisms'] = $actiontypeArr[$j]['mechanisms'] . ',' . $actiontypes[$i]['fNom'];
                                } else {
                                    $j++;
                                    $actiontypeArr[$j]['id'] = $actiontypes[$i]['id'];
                                    $actiontypeArr[$j]['nom'] = $actiontypes[$i]['mNom'];
                                    $actiontypeArr[$j]['mechanisms'] = $actiontypes[$i]['fNom'];

                                }
                            }

                            $actiontypes = $actiontypeArr;
                        }

                    }

                    if (count($actiontypes) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($actiontypes as $key => $row) {

                        $actiontypes[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $actiontypes[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $actiontypes));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json($e->getMessage().'Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }


            return view('actiontypes.index');

        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            return view('actiontypes.create');
        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();


            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $actiontype = ActionType::create($data);



            //versioning
            FadercoHelper::setVersioning();

            return Redirect::to('/actiontypes');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $actiontype = ActionType::with('mechanisms')->find($id);
            $mechanisms = [];
            foreach ($actiontype->mechanisms as $key => $value) {
                $mechanisms[] = $value->nom;
            }

            return view('actiontypes.show', compact('actiontype', 'mechanisms'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {

            $actiontype = ActionType::findOrFail($id);
            return view('actiontypes.edit', compact('actiontype'));

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();

            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $actiontype = ActionType::findOrFail($id);

            $data = $request->all();

            if ($actiontype->update($data)) {

                //versioning
                FadercoHelper::setVersioning();

                return Redirect::to('/actiontypes');//,compact('actiontype')
            }
        } catch (Exception $e) {
            Flash::overlay($e->getMessage() . 'Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $actiontype = ActionType::findOrFail($id);
            if ($actiontype->delete()) {
                //versioning
                FadercoHelper::setVersioning();

                return Response::json('action supprimée', 200);
            }
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
        }
    }
}
