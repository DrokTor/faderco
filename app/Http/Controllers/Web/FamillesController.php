<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\Famille;
use App\Models\Marque;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Redirect;
use Exception;
use Log;

class FamillesController extends Controller
{

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',

        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('familles.id', 'familles.nom as fNom', 'marques.nom as mNom');//, 'familles','logo'

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = Famille::count();

                        $familles = Famille::with('marques')->select(['id', 'nom'])
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($aColumns)
                            ->toArray();


                        foreach ($familles as $key => $value) {
                          if(isset($familles[$key]['marques']))
                          {
                            $familles[$key]['marques'] = collect($value['marques'])->implode('nom', ', ');
                          }
                        }


                    } else {
                        //recherche
                        $sTotal = Famille::leftJoin('famille_marque', 'familles.id', '=', 'famille_id')
                            ->leftJoin('marques', 'marques.id', '=', 'marque_id')
                            ->where('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('familles.nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $familles = Famille::leftJoin('famille_marque', 'familles.id', '=', 'famille_id')
                            ->leftJoin('marques', 'marques.id', '=', 'marque_id')
                            ->where('marques.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orWhere('familles.nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                        //dd($familles);

                        if ($familles) {
                            $familleArr = [];
                            $j = 0;
                            $familleArr[$j]['id'] = $familles[0]['id'];
                            $familleArr[$j]['nom'] = $familles[0]['fNom'];
                            $familleArr[$j]['marques'] = $familles[0]['mNom'];

                            for ($i = 1; $i < count($familles); ++$i) {
                                if ($familles[$i]['id'] == $familleArr[$j]['id']) {
                                    $familleArr[$j]['marques'] = $familleArr[$j]['marques'] . ',' . $familles[$i]['mNom'];
                                } else {
                                    $j++;
                                    $familleArr[$j]['id'] = $familles[$i]['id'];
                                    $familleArr[$j]['nom'] = $familles[$i]['fNom'];
                                    $familleArr[$j]['marques'] = $familles[$i]['mNom'];

                                }
                            }

                            $familles = $familleArr;
                        }

                    }

                    if (count($familles) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($familles as $key => $row) {

                        $familles[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $familles[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $familles));
                } catch (Exception $e) {

                  Log::error($e->getMessage());
                  return Response::json($e->getMessage().'Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }


            return view('familles.index');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $marques = Marque::All();
            return view('familles.create', compact('marques'));

        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();

            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $famille = Famille::create($data);
            $marques = isset($data['marques'])?$data['marques']:null;

            if(isset($data['marques']))
            foreach ($marques as $key => $value) {
                $marque = Marque::findOrFail($key);
                $marque->familles()->attach($famille);
            }

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/familles');//view('familles.view',compact('famille'));
        } catch (Exception $e) {
            Flash::overlay($e->getMessage().'Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $famille = Famille::with('marques')->find($id);//with('marques')->
            $marques = [];
            foreach ($famille->marques as $key => $value) {
                $marques[] = $value->nom;
            }

            return view('familles.show', compact('famille', 'marques'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            $marques = Marque::All();
            $famille = Famille::with('marques')->find($id);
            $attached = [];
            foreach ($famille->marques as $key => $value) {
                $attached[] = $value->id;
            }
            return view('familles.edit', compact('famille', 'marques', 'attached'));

        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $data = $request->all();

            $validator = $this->validator($data);

            if ($validator->fails())
                return Redirect::back()->withErrors($validator)->withInput();

            $famille = Famille::findOrFail($id);

            $data = $request->all();

            $marques = isset($data['marques'])?$data['marques']:null;
            $vals = [];
            if(isset($data['marques']))
            foreach ($marques as $key => $value) {
                $vals[] = $key;
            }
            //print_r($vals);exit();
            $famille->update($data);
            $famille->marques()->sync($vals);
            /*$new_marque= Marque::findOrFail($data['marque_id']);
            $old_marque= Marque::findOrFail($data['old_marque_id']);

            //$famille= Famille::findOrFail($id);

            $old_marque->familles()->detach($famille);
            $new_marque->familles()->attach($famille);*/

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/familles');
            //return view('familles.view',compact('famille'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $famille = Famille::findOrFail($id);
            if ($famille->delete()) {

                //versioning
                FadercoHelper::setVersioning();

                return Response::json('Famille supprimée', 200);
            }
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }
}
