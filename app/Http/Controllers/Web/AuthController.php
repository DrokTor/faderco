<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;

use App\Http\Requests;
use Laracasts\Flash\Flash;
use Redirect;
use Validator;
use View;
use Exception;

class AuthController extends Controller
{
    //use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    public function __construct()
    {
        //$this->middleware($this->guestMiddleware(), ['except' => 'deconnexion']);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|min:4|max:255',
            'password' => 'required|min:4',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            if(Auth::check()) return Redirect::intended('/');

            return View::make('auth.login');
        }
        catch (Exception $e) {

            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');

            return Redirect::intended('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            if(Auth::check()) return Redirect::intended('/');

            $validator = $this->validator($request->toArray());

            if($validator->fails()) return Redirect::back()->withErrors($validator)->withInput();

            $remember = false;
            if($request->has('remember-me')) $remember = true;

            if(Auth::attempt($request->only(array('username', 'password')), $remember)) {

                Auth::user()->updated_at = date('Y-m-d H:i:s');
                Auth::user()->save();

                Flash::overlay('Bienvenue '.Auth::user()->nom.' dans votre tableau de bord', 'Faderco Information');

                return Redirect::intended('/');
            }

            return Redirect::to('connexion');
        }
        catch (Exception $e) {

            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');

            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        if(Auth::check()) Auth::logout();

        Flash::overlay('Votre compte a été déconnecté', 'Faderco Information');

        return Redirect::to('connexion');
    }
}
