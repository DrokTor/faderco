<?php

namespace App\Http\Controllers\Web;

use Flash;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Redirect;
use Validator;
use Response;
use App\Models\RuptureReason;
use Exception;
use Log;

class RupturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|max:255',
        ]);
    }


    public function index(Request $request)
    {
        //
        try {

            if ($request->ajax()) {

                $data = $request->all();

                //page actuel
                $draw = $data['sEcho'];

                //columns
                $aColumns = array('id', 'nom');

                //paginate
                $sLimit = $data['iDisplayLength'];
                $sStart = $data['iDisplayStart'];

                //order by
                $sOrderName = $aColumns[$data['iSortCol_0']];
                $sOrderDir = $data['sSortDir_0'];

                //search
                $sSearch = $data['sSearch'];


                try {
                    if (empty($sSearch)) { //pas de recherche

                        $sTotal = RuptureReason::count();

                        $ruptures = RuptureReason::orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)
                            ->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    } else {
                        //recherche
                        $sTotal = RuptureReason::where('nom', 'LIKE', '%' . $sSearch . '%')
                            ->count();

                        $ruptures = RuptureReason::where('nom', 'LIKE', '%' . $sSearch . '%')
                            ->orderBy($sOrderName, $sOrderDir)
                            ->take($sLimit)->skip($sStart)
                            ->get($aColumns)
                            ->toArray();
                    }

                    if (count($ruptures) == 0) return Response::json(array('draw' => 0, 'recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => 0));

                    //change arrays keys to numeric
                    foreach ($ruptures as $key => $row) {

                        $ruptures[$key] = array_values($row);

                        //ajout d'un champ pour appliquer le css sur la ligne du datatables
                        $ruptures[$key]['DT_RowId'] = 'row_' . $row['id'];
                    }

                    return Response::json(array('draw' => $draw, 'recordsTotal' => $sTotal, 'recordsFiltered' => $sTotal, 'data' => $ruptures));
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                    return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
                }
            }

            return view('ruptures.index');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application, veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        try {
            return view('ruptures.create');//,'familles'
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();
            RuptureReason::create($data);

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/ruptures');
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }


    public function edit($id)
    {
        try {
            $rupture = RuptureReason::findOrFail($id);
            return view('ruptures.edit', compact('rupture'));
        } catch (Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            $data = $request->all();

            $rupture = RuptureReason::findOrFail($id);
            $rupture->nom = $data['nom'];

            $rupture->save();

            //versioning
            FadercoHelper::setVersioning();

            return redirect('/ruptures');

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $ruptures = RuptureReason::findOrFail($id);
            $ruptures->delete();

            //versioning
            FadercoHelper::setVersioning();

            return Response::json('Raison de rupture supprimée', 200);

        } catch (\Exception $e) {
            Flash::overlay('Une erreur s\'est produite avec l\'application\n Veuillez contacter l\'administrateur', 'Faderco Information');
            Log::error($e->getMessage());
            return Redirect::intended('/');
        }
    }

}
