<?php

namespace App\Http\Controllers\Api;



use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\PdvSurface;
use Illuminate\Http\Request;
use Exception;

class PdvSurfacesController extends Controller
{
    //
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required',

        ]);
    }

    public function index()
    {
        try
        {
            $pdvSurface= PdvSurface::get(array("id", "nom"));

            return Response::json($pdvSurface);
        }
        catch(Exception $e) {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function show($id)
    {
        try
        {
            $pdvSurface= PdvSurface::findOrFail($id,array("id", "nom"));

            return Response::json(array($pdvSurface));
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }
}
