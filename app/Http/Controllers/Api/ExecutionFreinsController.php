<?php

namespace App\Http\Controllers\Api;



use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\ExecutionFrein;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Input;

class ExecutionFreinsController extends Controller
{
    //

    public function index()
    {

        $freins=  ExecutionFrein::get(array("id", "nom"));

        return Response::json($freins);
    }
}
