<?php

namespace App\Http\Controllers\Api;

use App\Models\ActivityType;
use App\Models\Pc;
use App\Models\Rpc;
use Exception;
use FadercoHelper;
use FadercoResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Response;
use Validator;

class RpcsController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'access_token' => 'required',
            'pdv_id' => 'required|integer',
            'marque_id' => 'required|integer',
            'user_id' => 'required|integer',
            'pcs' => 'required|json'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $validator = $this->validator($request->toArray());

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID(Input::get('access_token'));

            $dataInfoGen = $request->except('access_token', 'pcs');

            //pair token user_id invalide
            if($user_id != $dataInfoGen['user_id']) return Response::json(FadercoResponse::getAUTHERR());

            $dataPcs = json_decode($request['pcs'], true); 

            //si le decodage json n'a pas retourné null
            if($dataPcs != null) {

                //rpc creation
                $rpc = Rpc::create($dataInfoGen);
                $rpc_id = $rpc->id;

                //prix concurrence pc
                foreach($dataPcs as $row) {
                    //insertion id pdv dans dataLinDev
                    $row['rpc_id']= $rpc_id;

                    //store to table
                    Pc::create($row);
                }

                //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
                $type_activity_id = ActivityType::where('nom', '=', 'Insertion')->first()->id;
                $message = 'Vient d\'ajouter un nouveau relevé de prix concurrentiel. <a href="/rpcs/'.$rpc_id.'">Consulter</a>';
                FadercoHelper::setActivity($request->getClientIp(), null , null , $message, $type_activity_id, $user_id);

                return Response::json(FadercoResponse::getSUCCCESS());
            }

            return Response::json(FadercoResponse::getINPUTVALIDERR());
        }
        catch(Exception $ex)
        {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
