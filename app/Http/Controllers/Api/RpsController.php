<?php

namespace App\Http\Controllers\Api;

use App\Models\ActivityType;
use App\Models\Produit_Rp;
use App\Models\Rp;
use FadercoHelper;
use FadercoResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Log;
use Exception;
use Response;
use Validator;

class RpsController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'access_token' => 'required',
            'pdv_id' => 'required|integer',
            'marque_id' => 'required|integer',
            'user_id' => 'required|integer',
            'produit_rp' => 'required|json',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID(Input::get('access_token'));

            $dataInfoGen = $request->except('access_token', 'produit_rp');

            //pair token user_id invalide
            if($user_id != $dataInfoGen['user_id']) return Response::json(FadercoResponse::getAUTHERR());

            //produit rpm
            $dataProduitRp = json_decode($request['produit_rp'], true);


            //validation du json produit rpm
            if($dataProduitRp == null) return Response::json(FadercoResponse::getINPUTVALIDERR());

            //rp creation
            $rp = Rp::create($dataInfoGen);
            $rp_id = $rp->id;

            //produit rp
            foreach($dataProduitRp as $row) {

                //compatibilité avec l'ancienne version mobile
                $row['pad'] = $row['pad'] == '' ? 0 : $row['pad'];
                $row['pvc'] = $row['pvc'] == '' ? 0 : $row['pvc'];

                //if($row['pad'] !='' and $row['pvc']!= '') {

                //insertion id rpm dans ProduitRpm
                $row['rp_id']= $rp_id;

                //produit_id
                $row['produit_id'] = $row["produit"]["id"];
                unset( $row["produit"]);

                //store to table
                Produit_Rp::create($row);
                //}
            }

            //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
            $type_activity_id = ActivityType::where('nom', '=', 'Insertion')->first()->id;
            $message = 'Vient d\'ajouter un nouveau relevé des prix. <a href="/rps/'.$rp_id.'">Consulter</a>';
            FadercoHelper::setActivity($request->getClientIp(), null , null , $message, $type_activity_id, $user_id);

            return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch (Exception $e) {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
