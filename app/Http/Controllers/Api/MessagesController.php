<?php
/**
 * Created by PhpStorm.
 * User: Messi89
 * Date: 16-04-2016
 * Time: 22:35
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\User;
use FadercoHelper;
use FadercoResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use Log;
use Validator;
use Response;
use Illuminate\Http\Request;
use Exception;

class MessagesController extends Controller
{
    public function __construct()
    {

    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'access_token' => 'required',
        ]);
    }

    public function index()
    {
        try
        {
            $data = Input::get();

            $validator = $this->validator($data);

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID($data['access_token']);

            //recuperer les messages du user (envoyé par l'admin)
            $messages=  User::with('messages')->findOrFail($user_id)->messages()->join('users', 'users.id', '=', 'messages.user_id')
                ->get(array(
                    'messages.id',
                    'messages.objet',
                    'messages.contenu',
                    'users.id as expediteur_id',
                    'users.nom as expediteur_nom',
                    'users.email as expediteur_email',
                    'messages.created_at'
                ));

            $messages = $messages->each(function($item, $key){

                //etat message
                $item['etat'] = $item['pivot']['etat'];

                //truncate contenu du message
                $item['contenu'] = FadercoHelper::truncateMessage( $item['contenu'], 40, ' ');

                //expediteur du message
                $item['expediteur'] = array('id'=> $item['expediteur_id'], 'nom' =>  $item['expediteur_nom'], 'email' =>  $item['expediteur_email']);
                unset( $item['expediteur_id']);
                unset( $item['expediteur_nom']);
                unset( $item['expediteur_email']);
                unset($item['pivot']);
            });

            if($messages->isEmpty()) return Response::json(FadercoResponse::getSGBDROWNOTFOUND());

            return Response::json($messages);
        }
        catch(ModelNotFoundException $e) {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
        catch(Exception $e) {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        try
        {
            $data = Input::get();

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID($data['access_token']);

            $message= Message::join('users', 'users.id', '=', 'messages.user_id')
                ->with(array('users'=> function($query) use ($user_id) {
                    $query->where('id', '=', $user_id)->first();
                }))
                ->findOrFail($id, array(
                    'messages.id',
                    'messages.objet',
                    'messages.contenu',
                    'users.id as user_id',
                    'users.nom as user_nom',
                    'users.email as user_email',
                    'messages.created_at'
                ));


            $data = collect([
                'id' => $message->id,
                'objet' => $message->objet,
                'contenu' => $message->contenu,
                'created_at' => $message->created_at,
                'expediteur' => array('id'=>$message->user_id, 'nom' => $message->user_nom, 'email' => $message->user_email)
            ]);

            //set to read
            if(! $message->users->first()->pivot->etat) {
                $message->users->first()->pivot->etat = true;
                $message->users->first()->pivot->save();
            }

            return Response::json(array($data));
        }
        catch(ModelNotFoundException $e) {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
        catch (Exception $e)
        {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function update($id)
    {

    }

    public function destroy($id)
    {

    }

}