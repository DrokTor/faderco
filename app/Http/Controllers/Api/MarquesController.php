<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Produit;
use FadercoHelper;
use FadercoResponse;
use Illuminate\Support\Facades\Input;
use Log;
use Validator;
use Response;
use App\Models\Marque;
use Illuminate\Http\Request;
use Exception;

class MarquesController extends Controller
{
    public function __construct()
    {

    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required',
            'access_token'=>'required',
        ]);
    }

    public function index()
    {
        try
        {
            $data = Input::except('access_token');
            if(!empty($data) && $data['type'] == "loclin") {

                $marques= Marque::where('loclin', '=', true)->get(array("id", "nom", "loclin"));

                return Response::json($marques);
            }
            else {
                
                $marques= Marque::get(array("id", "nom", "loclin"));

                return Response::json($marques);
            }

        }
        catch(Exception $e) {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function store(Request $request)
    {
        try
        {
          $data= $request->all();

          $validator = $this->validator($data);

          if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

          Marque::create($data);

          return Response::json(FadercoResponse::getSUCCCESS());

        }
        catch(Exception $ex)
        {
          return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function show($id)
    {
        try
        {
            $data = Input::except('access_token');

            if(!empty($data) && $data['type'] == "rpm") {

                $marque= Marque::with(array('familles'=> function($query){
                    $query->get(array('familles.id', 'nom'));
                }))->findOrFail($id, array("marques.id", "nom", "loclin"));

                foreach($marque->familles as $famille) {
                    $produits = Produit::where('famille_marque_id', '=', $famille->pivot->id)->get(array('id', 'nom', 'up', 'pc'));

                    $famille['produits'] = $produits;

                    unset($famille->pivot);
                }

                return Response::json($marque->familles);
            }
            else {
                $marque= Marque::findOrFail($id, array("id", "nom", "loclin"));//with('familles')->

                return Response::json(array($marque));
            }
        }
        catch (Exception $e)
        {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }

    public function update( Request $request ,$id)
    {
        try
        {
            $marque= Marque::findOrFail($id);

            $data= $request->all();

            if($marque->update($data))
                return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch (Exception $e)
        {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }

    public function destroy($id)
    {
        try
        {
            $marque= Marque::findOrFail($id);
            if($marque->delete())
                return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch (Exception $e)
        {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }
}
