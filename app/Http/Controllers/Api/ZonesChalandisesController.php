<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\ZoneChalandise;
use Illuminate\Http\Request;
use Exception;

class ZonesChalandisesController extends Controller
{
    //

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required',
        ]);
    }

    public function index()
    {
        try
        {
            $zone= ZoneChalandise::get(array("id", "nom"));

            return Response::json($zone);
        }
        catch(Exception $e) {
            return Response::json(FadercoResponse::getSGBDERR());
        }


    }

    public function show($id)
    {
        try
        {
            $zone= ZoneChalandise::findOrFail($id, array("id", "nom"));

            return Response::json(array($zone));
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }
}
