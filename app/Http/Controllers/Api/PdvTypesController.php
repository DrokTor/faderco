<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\PdvType;
use Illuminate\Http\Request;
use Exception;

class PdvTypesController extends Controller
{
    //
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required',

        ]);
    }

    public function index()
    {
        try
        {
            $pdvType= PdvType::get(array("id", "nom"));

            return Response::json($pdvType);
        }
        catch(Exception $e) {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function show($id)
    {
        try
        {
            $pdvType= PdvType::findOrFail($id, array("id", "nom"));

            return Response::json(array($pdvType));
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }
}
