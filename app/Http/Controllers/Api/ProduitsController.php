<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Illuminate\Support\Facades\Input;
use Validator;
use Response;
use App\Models\Produit;
use App\Models\Famille;
use App\Models\Marque;
use Exception;
class ProduitsController extends Controller
{
    //
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required',
            'famille_id'=>'required',
            'marque_id'=>'required',
            'access_token'=>'required',
            //'old_marque_id'=>'required',
        ]);
    }

    public function index()
    {
        try
        {
            $data = Input::except('access_token');

            if(!empty($data) && $data['type'] == "rpm") {

                $produits = Produit::with(array('famille_marque.famille' => function($query){
                    $query->get(array('id','nom'));
                }))->get(array('id', 'nom', 'famille_marque_id'));

                //aucun pdv n'est trouvé
                if($produits->isEmpty()) return Response::json(FadercoResponse::getSGBDROWNOTFOUND());

                //collection data
                $data = collect();

                foreach($produits as $produit) {
                    $data->push([
                        'id'=> $produit->id,
                        'nom'=> $produit->nom,
                        'famille'=> ['id'=> $produit->famille_marque->famille_id, 'nom'=> $produit->famille_marque->famille->nom],
                    ]);
                }

                return Response::json($data);
            }
            else {
                $produits = Produit::with('famille_marque')->get();

                //ajout du nom de la marque et de la famille associes
                foreach($produits as $produit){
                    $produit->famille_marque->marque_nom = Marque::findOrFail($produit->famille_marque->marque_id)->nom;
                    $produit->famille_marque->famille_nom = Famille::findOrFail($produit->famille_marque->famille_id)->nom;
                }

                return Response::json($produits);
            }
        }
        catch( Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function store(Request $request)
    {
        try
        {
            $data= $request->all();

            $validator = $this->validator($data);

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            Produit::create($data);

            return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch(Exception $ex)
        {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function show($id)
    {
        try
        {
            $product= Produit::with('famille','marque')->where(['id'=>$id])->get();

            return Response::json(array($product));
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }

    public function update( Request $request ,$id)
    {
        try
        {
            $product= Produit::findOrFail($id);

            $data= $request->all();


            $product->update($data);


            return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }

    public function destroy($id)
    {
        // add concept of relation with 'Marque' when deleting a 'Famille'
        try
        {
            $product= Produit::findOrFail($id);
            if($product->delete())
                return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }
}
