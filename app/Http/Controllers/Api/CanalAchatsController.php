<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\CanalAchat;
use Illuminate\Http\Request;
use Exception;

class CanalAchatsController extends Controller
{
    //

    public function index()
    {
        try
        {
            $canals= CanalAchat::get(array("id", "nom"));

            return Response::json($canals);
        }
        catch(Exception $e) {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function show($id)
    {
        try
        {
            $canal= CanalAchat::findOrFail($id, array("id", "nom"));
            return Response::json(array($canal));
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }
}
