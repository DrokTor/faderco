<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\RuptureReason;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Input;

class RuptureReasonsController extends Controller
{
    //
    public function index()
    {
        $rupture=  RuptureReason::get(array("id", "nom"));

        return Response::json($rupture);
    }
}
