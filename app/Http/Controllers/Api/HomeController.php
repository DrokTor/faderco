<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Versioning;
use Carbon\Carbon;
use FadercoHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Log;
Use Validator;
use Response;
Use FadercoResponse;
use HttpClient;
use Exception;

class HomeController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'localisation_lat' => 'required',
            'localisation_long' => 'required'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            $data = Input::get();

            $validator = $this->validator($data);

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID($data['access_token']);

            //recuperer les messages du user (envoyé par l'admin)
            $unread =  User::with('messages')->findOrFail($user_id)->messages()->where('etat', '=', false)->count();

            $weekago = Carbon::today()->subWeek();
            $unreadweek =  User::with('messages')->findOrFail($user_id)->messages()
                ->where('etat', '=', false)
                ->where('messages.created_at', '>', $weekago)
                ->count();
            $inbox =  User::with('messages')->findOrFail($user_id)->messages()->count();

            //méteo
            $request = [
                'url' => 'http://api.apixu.com/v1/current.json',
                'params' => [
                    'key'     => '2d6c2275af8a4685903223602160405',
                    'q'   => $data['localisation_lat'].','.$data['localisation_long']
                ]
            ];

            $response = HttpClient::get($request)->content();
            $meteo = json_decode($response, true);

            //db version
            $version = Versioning::first()->version;

            $data = array(
                'id' => "-200",
                'version' => $version,
                'inbox' => $inbox,
                'unread' => $unread,
                'unreadweek' => $unreadweek,
                'name' => $meteo['location']['name'],
                'region' => $meteo['location']['region'],
                'temparature'=>$meteo['current']['temp_c'],
                'icon' => 'http://'.substr($meteo['current']['condition']['icon'], 2),
                'wind' =>$meteo['current']['wind_kph'],
                'humidity' =>$meteo['current']['humidity'],
            );

            return Response::json(array($data));
        }
        catch (Exception $e)
        {
            Log::info($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
