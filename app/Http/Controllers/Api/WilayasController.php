<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Wilaya;
use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use Exception;
use FadercoResponse;
use Validator;
use Illuminate\Support\Facades\Input;

class WilayasController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'region_id' => 'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            $data = Input::get();

            $validator = $this->validator($data);

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            $wilayas = Wilaya::where('region_id', '=', $data['region_id'])->get(array("id", "nom", "region_id"));

            if($wilayas->isEmpty()) return Response::json(FadercoResponse::getSGBDROWNOTFOUND());

            return Response::json($wilayas);
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
