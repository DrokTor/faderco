<?php

namespace App\Http\Controllers\Api;

use App\Models\Activity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Validator;
use Exception;
use Response;
use FadercoResponse;

class ActivitiesController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'access_token' => 'required',
            'adresse_ip' => 'required|max:255',
            'localisation_lat' => 'required|max:255',
            'localisation_long' => 'required|max:255',
            'message' => 'required|max:255',
            'type_activity_id' => 'required|integer',
            'user_id' => 'required|integer'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            $activities= Activity::with('user', 'type_activity')->get();

            return Response::json($activities);
        }
        catch(Exception $e) {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            $validator = $this->validator($request->toArray());

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            $data = $request->except('access_token');

            Activity::create($data);

            return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch(Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $activity= Activity::with('user', 'type_activity')->findOrFail($id);

            return Response::json(array($activity));
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
