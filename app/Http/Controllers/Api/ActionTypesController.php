<?php

namespace App\Http\Controllers\Api;

use App\Models\ActionType;
use Exception;
use FadercoResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Response;

class ActionTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            $canals= ActionType::get(array("id", "nom"));

            return Response::json($canals);
        }
        catch(Exception $e) {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $actiontype= ActionType::with(array('mechanisms'=> function($query){
                $query->get(array('mechanisms.id', 'nom', 'prec'));
            }))->findOrFail($id);

            $data = $actiontype->mechanisms;

            $data = $data->each(function ($item, $key) {
                if($item['prec'] == "0") $item['prec'] = false;
                else $item['prec'] = true;
                unset($item['pivot']);
            });

            return Response::json($data);
        }
        catch(ModelNotFoundException $e) {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
        catch (Exception $e)
        {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
