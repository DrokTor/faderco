<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\UploadPhotoException;
use App\Models\ActionType_Mechanism;
use App\Models\ActivityType;
use App\Models\Rvc;
use App\Models\RvcPhoto;
use DB;
use Exception;
use FadercoHelper;
use FadercoResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Image;
use Intervention\Image\Exception\NotReadableException;
use Log;
use Response;
use Validator;

class RvcsController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'access_token' => 'required',
            'reference' => 'required',
            'periode' => 'required|integer',
            /*'autre' => 'required',*/
            /*'observation' => 'required',*/
            /*'action_type_mechanism_id' => 'required|integer',*/
            'action_type_id' => 'required|integer',
            'mechanism_id' => 'required|integer',
            'pdv_id' => 'required|integer',
            'famille_id' => 'required|integer',
            'user_id' => 'required|integer',
            /*'photos' => 'required',*/
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID(Input::get('access_token'));

            $dataInfoGen = $request->except('access_token', 'action_type_id', 'mechanism_id', 'photos');

            //pair token user_id invalide
            if ($user_id != $dataInfoGen['user_id']) return Response::json(FadercoResponse::getAUTHERR());

            //get action_type_mechanism_id
            $dataInfoGen['action_type_mechanism_id'] = ActionType_Mechanism::where('action_type_id', '=', $request->get('action_type_id'))
                ->where('mechanism_id', '=', $request->get('mechanism_id'))
                ->first()->id;

            //photos envoyées
            if ($request->hasFile('photos')) $photos = $request['photos'];
            else $photos = null;

            // Start transaction
            DB::beginTransaction();

            //rpm creation
            $rvc = Rvc::create($dataInfoGen);
            $rvc_id = $rvc->id;

            //upload photos
            if ($photos != null) {
                $photos_count = count($photos);

                //repertoire des photos
                $path = 'images/rvcs/' . $rvc_id;

                if ($photos_count > 0) {

                    FadercoHelper::createDirectory($path);

                    foreach ($photos as $photo) {

                        if($photo->isValid()) {

                            $name = strtolower(preg_replace('/\s+/', '', $photo->getClientOriginalName()));
                            $extension =$photo->getClientOriginalExtension();

                            if($extension != "" && $extension != null) {
                                if(strpos($name, $extension) === false) $photoname = time() . '_' . rand() . '_' .$name.'.'.$extension;
                                else $photoname = time() . '_' . rand() . '_' .$name;
                            }
                            else $photoname = time() . '_' . rand() . '_' .$name;

                            if (FadercoHelper::moveFile($photo, $photoname, $path)) {

                                $rvc_photo = RvcPhoto::create(array('url' => $path . '/' . $photoname, 'rvc_id' => $rvc_id));

                                // open an image file
                                $img = Image::make($rvc_photo->url);
                                $img->resize(320, 240);

                                if($extension != "" && $extension != null) {
                                    $resized_url = str_replace("." . $extension, "", $rvc_photo->url);
                                    $resized_url = $resized_url . "_320x240." . $extension;
                                }
                                else {
                                    $resized_url = $rvc_photo->url."_320x240";
                                }

                                $img->save($resized_url);
                            }
                        }
                        else {
                            FadercoHelper::deleteDirectory($path);
                            throw new UploadPhotoException("File Corrupted on upload.");
                            break;
                        }
                    }
                }
            }

            //commit transaction
            DB::commit();

            //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
            $type_activity_id = ActivityType::where('nom', '=', 'Insertion')->first()->id;
            $message = 'Vient d\'ajouter un nouveau rapport de veille concurrentielle. <a href="/rvcs/' . $rvc_id . '">Consulter</a>';
            FadercoHelper::setActivity($request->getClientIp(), null, null, $message, $type_activity_id, $user_id);

            return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch (UploadPhotoException $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getFILEUPLOADERR());
        }
        catch (NotReadableException $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getFILEUPLOADERR());
        }
        catch (Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }
}
