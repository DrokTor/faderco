<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ActivityType;
use DB;
use FadercoHelper;
use FadercoResponse;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Input;
use Log;
use Validator;
use Response;
use Redirect;
use App\Models\Pdv;
use App\Models\LocLin;
use App\Models\LinDev;
use Illuminate\Http\Request;
use Exception;

class PdvsController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'access_token' => 'required',
            'nom' => 'required|max:255',
            'localisation_lat' => 'required|max:255',
            'localisation_long' => 'required|max:255',
            'nb_sortie_caisse' => 'required|max:255',
            'user_id' => 'required|integer',
            'region_id' => 'required|integer',
            'wilaya_id' => 'required|integer',
            'commune_id' => 'required|integer',
            'canal_achat_id' => 'required|integer',
            'type_pdv_id' => 'required|integer',
            'surface_pdv_id' => 'required|integer',
            'zone_chalandise_id' => 'required|integer',
            'lin_dev' => 'required',
            'loc_lin' => 'required'
        ]);
    }

    public function index()
    {
        try
        {
            $data = Input::except('access_token', 'mypdvs');

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID(Input::get('access_token'));

            //mes pdvs
            $mypdvs = Input::get('mypdvs');

            if(empty($data)) { //pas de recherche

                if($mypdvs != 1) {
                    $pdvs= Pdv::join('users', 'users.id', '=', 'user_id')
                        ->join('regions', 'regions.id', '=', 'pdvs.region_id') //with('LocLins', 'LinDevs')
                        ->join('wilayas', 'wilayas.id', '=', 'pdvs.wilaya_id')
                        ->join('communes', 'communes.id', '=', 'pdvs.commune_id')
                        ->where('pdvs.etat', '=', true)
                        ->orWhere('pdvs.user_id', '=', $user_id)
                        ->get(array(
                            'pdvs.id',
                            'pdvs.nom',
                            'pdvs.created_at',
                            'users.nom as utilisateur',
                            'regions.id as region_id','regions.nom as region_nom',
                            'wilayas.id as wilaya_id','wilayas.nom as wilaya_nom',
                            'communes.id as commune_id','communes.nom as commune_nom',
                        ));
                }
                else {
                    $pdvs= Pdv::join('users', 'users.id', '=', 'user_id')
                        ->join('regions', 'regions.id', '=', 'pdvs.region_id') //with('LocLins', 'LinDevs')
                        ->join('wilayas', 'wilayas.id', '=', 'pdvs.wilaya_id')
                        ->join('communes', 'communes.id', '=', 'pdvs.commune_id')
                        ->where('pdvs.user_id', '=', $user_id)
                        ->get(array(
                            'pdvs.id',
                            'pdvs.nom',
                            'pdvs.created_at',
                            'users.nom as utilisateur',
                            'regions.id as region_id','regions.nom as region_nom',
                            'wilayas.id as wilaya_id','wilayas.nom as wilaya_nom',
                            'communes.id as commune_id','communes.nom as commune_nom',
                        ));
                }

                //aucun pdv n'est trouvé
                if($pdvs->isEmpty()) return Response::json(FadercoResponse::getSGBDROWNOTFOUND());

                //collection data
                $data = collect();

                foreach($pdvs as $pdv) {
                    $data->push([
                        'id'=> $pdv->id,
                        'nom'=> $pdv->nom,
                        'created_at'=> $pdv->created_at,
                        'user' => ['nom'=> $pdv->utilisateur ],
                        'region'=> ['id'=> $pdv->region_id, 'nom'=> $pdv->region_nom],
                        'wilaya'=> ['id'=> $pdv->wilaya_id, 'nom'=> $pdv->wilaya_nom],
                        'commune'=> ['id'=> $pdv->commune_id, 'nom'=> $pdv->commune_nom],
                    ]);
                }

                return Response::json($data);
            }
            else {
                //initialisation des variables
                $nom = $region_id = $wilaya_id = $commune_id = $latitude = $longtitude = $canal_achat_id = $type_pdv_id = "";

                //verification des filtres envoyés
                if(array_key_exists("nom", $data)) $nom  = $data['nom'];
                if(array_key_exists("region_id", $data)) $region_id  = $data['region_id'];
                if(array_key_exists("wilaya_id", $data)) $wilaya_id  = $data['wilaya_id'];
                if(array_key_exists("commune_id", $data)) $commune_id  = $data['commune_id'];
                if(array_key_exists("latitude", $data)) $latitude  = $data['latitude'];
                if(array_key_exists("longtitude", $data)) $longtitude  = $data['longtitude'];
                if(array_key_exists("canal_achat_id", $data)) $canal_achat_id  = $data['canal_achat_id'];
                if(array_key_exists("type_pdv_id", $data)) $type_pdv_id  = $data['type_pdv_id'];


                //création de la requete sur pdv
                $pdvs = Pdv::where('pdvs.nom', 'LIKE', '%'.$nom.'%')  //with('LocLins', 'LinDevs')
                    ->where('localisation_lat', 'LIKE', $latitude.'%')
                    ->where('localisation_long', 'LIKE', $longtitude.'%');

                //ignorer les filtres vide dans la requete
                if(!empty($region_id)) $pdvs = $pdvs->where('pdvs.region_id', '=', $region_id);
                if(!empty($wilaya_id)) $pdvs = $pdvs->where('pdvs.wilaya_id', '=', $wilaya_id);
                if(!empty($commune_id)) $pdvs = $pdvs->where('pdvs.commune_id', '=', $commune_id);
                if(!empty($canal_achat_id)) $pdvs = $pdvs->where('pdvs.canal_achat_id', '=', $canal_achat_id);
                if(!empty($type_pdv_id)) $pdvs = $pdvs->where('pdvs.type_pdv_id', '=', $type_pdv_id);

                $pdvs->join('users', 'users.id', '=', 'user_id');
                $pdvs->join('regions', 'regions.id', '=', 'pdvs.region_id');
                $pdvs->join('wilayas', 'wilayas.id', '=', 'pdvs.wilaya_id');
                $pdvs->join('communes', 'communes.id', '=', 'pdvs.commune_id');

                //executer la requete
                if($mypdvs != 1) {
                    $pdvs = $pdvs->where(function($query) use ($user_id) {
                        $query->where('pdvs.etat', '=', true)
                            ->orWhere('pdvs.user_id', '=', $user_id);
                    })

                        //$pdvs = $pdvs->where('pdvs.etat', '=', true)
                        //->orWhere('pdvs.user_id', '=', $user_id)
                        ->get(array(
                            'pdvs.id',
                            'pdvs.nom',
                            'pdvs.created_at',
                            'users.nom as utilisateur',
                            'regions.id as region_id','regions.nom as region_nom',
                            'wilayas.id as wilaya_id','wilayas.nom as wilaya_nom',
                            'communes.id as commune_id','communes.nom as commune_nom',
                        ));
                }
                else {
                    $pdvs = $pdvs->where(function($query) use ($user_id) {
                        $query->where('pdvs.user_id', '=', $user_id);
                    })
                    ->get(array(
                        'pdvs.id',
                        'pdvs.nom',
                        'pdvs.created_at',
                        'users.nom as utilisateur',
                        'regions.id as region_id','regions.nom as region_nom',
                        'wilayas.id as wilaya_id','wilayas.nom as wilaya_nom',
                        'communes.id as commune_id','communes.nom as commune_nom',
                    ));
                }


                //aucun pdv n'est trouvé
                if($pdvs->isEmpty()) return Response::json(FadercoResponse::getSGBDROWNOTFOUND());

                //collection data
                $data = collect();

                foreach($pdvs as $pdv) {
                    $data->push([
                        'id'=> $pdv->id,
                        'nom'=> $pdv->nom,
                        'created_at'=> $pdv->created_at,
                        'user' => ['nom'=> $pdv->utilisateur ],
                        'region'=> ['id'=> $pdv->region_id, 'nom'=> $pdv->region_nom],
                        'wilaya'=> ['id'=> $pdv->wilaya_id, 'nom'=> $pdv->wilaya_nom],
                        'commune'=> ['id'=> $pdv->commune_id, 'nom'=> $pdv->commune_nom],
                    ]);
                }

                return Response::json($data);
            }
        }
        catch(Exception $e) {
            Log::info($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function store(Request $request)
    {
        try
        {
            $validator = $this->validator($request->toArray());

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID(Input::get('access_token'));

            $dataInfoGen = $request->except('access_token', 'lin_dev', 'loc_lin');

            //pair token user_id invalide
            if($user_id != $dataInfoGen['user_id']) return Response::json(FadercoResponse::getAUTHERR());

            $dataLinDev = json_decode($request['lin_dev'], true); //+ id pdv
            $dataLocLin = json_decode($request['loc_lin'], true); // + id pdv

            //si le decodage json n'a pas retourné null
            if($dataLinDev != null and $dataLocLin!= null) {

                // Start transaction
                DB::beginTransaction();

                //pdv creation
                $pdv = Pdv::create($dataInfoGen);
                $pdv_id = $pdv->id;

                //lineaire dev (famille_pdv)
                foreach($dataLinDev as $row) {
                    //insertion id pdv dans dataLinDev
                    $row['pdv_id']= $pdv_id;

                    //famille_id
                    $row['famille_id'] = $row["famille"]["id"];
                    unset( $row["famille"]);

                    //store to table
                    LinDev::create($row);
                }

                //location lin (marque_pdv)
                foreach($dataLocLin as $row) {
                    //insertion id pdv dans dataLocLin
                    $row['pdv_id']= $pdv_id;

                    //marque_id
                    $row['marque_id'] = $row["marque"]["id"];
                    unset( $row["marque"]);

                    //pivot
                    $dataPlvStore = array_merge($row["plv_out_store"], $row["plv_in_store"] ); //json_decode($request['plv_store'], true); // + id marque_pdv (loc_lin)
                    unset($row['plv_in_store']);
                    unset($row['plv_out_store']);

                    // change id to plv_id
                    foreach($dataPlvStore as $key=>$dataPlv) {
                        $dataPlvStore[$key]['plv_id'] = $dataPlvStore[$key]['id'];
                        unset($dataPlvStore[$key]['id']);
                    } 

                    //store to table loc lin
                    $locLin = LocLin::create($row);

                    //pivot plv-store marque_pdv_plv)
                    foreach($dataPlvStore as $row) {
                        $locLin->plvs()->attach($row['plv_id'],['quantite'=>$row['quantite'],'date'=>$row['date']]);
                    }
                }

                //commit transaction
                DB::commit();

                //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
                $type_activity_id = ActivityType::where('nom', '=', 'Insertion')->first()->id;
                $message = 'Vient d\'ajouter un nouveau point de vente. <a href="/pdvs/'.$pdv_id.'">Consulter</a>'; 
                FadercoHelper::setActivity($request->getClientIp(), $dataInfoGen['localisation_lat'] , $dataInfoGen['localisation_long'] , $message, $type_activity_id, $user_id);

                return Response::json(array(array('id' => $pdv_id)));
                //return Response::json(FadercoResponse::getSUCCCESS());
            }

            return Response::json(FadercoResponse::getINPUTVALIDERR());
        }
        catch(Exception $e)
        {
            DB::rollback();
            Log::info($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function show($id)
    {
        try
        {
            //recuperer le id du user
            $user_id = FadercoHelper::getUserID(Input::get('access_token'));

            $pdv= Pdv::join('regions', 'regions.id', '=', 'pdvs.region_id')
                ->join('wilayas', 'wilayas.id', '=', 'pdvs.wilaya_id')
                ->join('communes', 'communes.id', '=', 'pdvs.commune_id')
                ->join('users', 'users.id', '=', 'pdvs.user_id')
                ->join('canal_achats', 'canal_achats.id', '=', 'pdvs.canal_achat_id')
                ->join('type_pdvs', 'type_pdvs.id', '=', 'pdvs.type_pdv_id')
                ->join('surface_pdvs', 'surface_pdvs.id', '=', 'pdvs.surface_pdv_id')
                ->join('zone_chalandises', 'zone_chalandises.id', '=', 'pdvs.zone_chalandise_id')
                ->where('pdvs.id', '=', $id)
                //->where('pdvs.user_id', $user_id)
                ->with('LinDevs', 'LocLins')
                ->firstOrFail(array(
                    'pdvs.id',
                    'pdvs.nom',
                    'localisation_lat',
                    'localisation_long',
                    'nb_sortie_caisse',
                    'regions.id as region_id','regions.nom as region_nom',
                    'wilayas.id as wilaya_id','wilayas.nom as wilaya_nom',
                    'communes.id as commune_id','communes.nom as commune_nom',
                    'users.id as user_id','users.nom as user_nom','users.username as user_username',
                    'users.telephone as user_telephone','users.avatar as user_avatar',
                    'canal_achats.id as canal_achat_id','canal_achats.nom as canal_achat_nom',
                    'type_pdvs.id as type_pdv_id','type_pdvs.nom as type_pdv_nom',
                    'surface_pdvs.id as surface_pdv_id','surface_pdvs.nom as surface_pdv_nom',
                    'zone_chalandises.id as zone_chalandise_id','zone_chalandises.nom as zone_chalandise_nom',
                ));

            //lin dev collection
            $lin_devs_collection = collect();

            foreach($pdv->LinDevs as $lin_dev) {
                $lin_devs_collection->push([
                    'famille'=> ['id'=> $lin_dev->id, 'nom'=> $lin_dev->nom],
                    'lineaire_developpe'=> $lin_dev->pivot->lineaire_developpe,
                ]);
            }

            //loc lin collection
            $loc_lins_collection = collect();

            foreach($pdv->LocLins as $loc_lin) {

                //init plv_in/out store collection
                $plv_in_store = collect();
                $plv_out_store = collect();

                //liste loclin_plv
                $marque_pdv = LocLin::where('id', '=', $loc_lin->pivot->id)
                    ->with('plvs')
                    ->first();

                //plvs store in/out collections
                foreach ($marque_pdv->plvs as $marque_pdv_plv) {
                    if($marque_pdv_plv->type == "in") {
                        $plv_in_store->push([
                            'id' => $marque_pdv_plv->id,
                            'nom' => $marque_pdv_plv->nom,
                            'type' => $marque_pdv_plv->type,
                            'quantite' => $marque_pdv_plv->pivot->quantite,
                            'date' => $marque_pdv_plv->pivot->date,
                        ]);
                    }
                    else {
                        $plv_out_store->push([
                            'id' => $marque_pdv_plv->id,
                            'nom' => $marque_pdv_plv->nom,
                            'type' => $marque_pdv_plv->type,
                            'quantite' => $marque_pdv_plv->pivot->quantite,
                            'date' => $marque_pdv_plv->pivot->date,
                        ]);
                    }
                }

                $loc_lins_collection->push([
                    'marque'=> ['id'=> $loc_lin->id, 'nom'=> $loc_lin->nom],
                    'duree'=> $loc_lin->pivot->duree,
                    'prix'=> $loc_lin->pivot->prix,
                    'plv_in_store' => $plv_in_store,
                    'plv_out_store' => $plv_out_store
                ]);
            }

            //data pdv collection
            $data = collect([
                'id'=> $pdv->id,
                'nom'=> $pdv->nom,
                'localisation_lat'=> $pdv->localisation_lat,
                'localisation_long'=> $pdv->localisation_long,
                'nb_sortie_caisse'=> $pdv->nb_sortie_caisse,
                'region'=> ['id'=> $pdv->region_id, 'nom'=> $pdv->region_nom],
                'wilaya'=> ['id'=> $pdv->wilaya_id, 'nom'=> $pdv->wilaya_nom],
                'commune'=> ['id'=> $pdv->commune_id, 'nom'=> $pdv->commune_nom],
                'user'=> ['id'=> $pdv->user_id, 'nom'=> $pdv->user_nom, 'username'=> $pdv->user_username, 'telephone'=> $pdv->user_telephone, 'avatar'=> $pdv->user_avatar],
                'canal_achat'=> ['id'=> $pdv->canal_achat_id, 'nom'=> $pdv->canal_achat_nom],
                'type_pdv'=> ['id'=> $pdv->type_pdv_id, 'nom'=> $pdv->type_pdv_nom],
                'surface_pdv'=> ['id'=> $pdv->surface_pdv_id, 'nom'=> $pdv->surface_pdv_nom],
                'zone_chalandise'=> ['id'=> $pdv->zone_chalandise_id, 'nom'=> $pdv->zone_chalandise_nom],
                'lin_devs' => $lin_devs_collection,
                'loc_lins' => $loc_lins_collection,
            ]);

            return Response::json(array($data));
        }
        catch (ModelNotFoundException $e) {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function update( Request $request ,$id)
    {
        try
        {
            $validator = $this->validator($request->toArray());

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID(Input::get('access_token'));

            $pdv = Pdv::findOrFail($id);

            $dataInfoGen = $request->except('access_token', 'lin_dev', 'loc_lin');

            //pair token user_id invalide
            if($user_id != $dataInfoGen['user_id']) return Response::json(FadercoResponse::getAUTHERR());

            $dataLinDev = json_decode($request['lin_dev'], true); //+ id pdv
            $dataLocLin = json_decode($request['loc_lin'], true); // + id pdv

            //si le decodage json n'a pas retourné null
            if($dataLinDev != null and $dataLocLin!= null) {

                // Start transaction
                DB::beginTransaction();

                //remettre le pdv pour validation
                $dataInfoGen['etat'] = false;

                //pdv update
                $pdv->update($dataInfoGen);
                $pdv_id = $pdv->id;

                /////////////////////////////////lin dev
                //delete lineaire dev
                $pdvLinDev = LinDev::where('pdv_id', '=',$pdv_id)->get(array('famille_id', 'pdv_id'))->toArray();

                foreach($pdvLinDev as $key=>$row) {
                    $delete = true;
                    foreach($dataLinDev as $item) {
                        if($row['famille_id'] == $item["famille"]["id"] ) $delete = false;
                    }

                    if($delete) LinDev::where('pdv_id', '=',$pdv_id)->where('famille_id', '=', $row['famille_id'])->delete();
                }

                //update/add lineaire dev (famille_pdv)
                foreach($dataLinDev as $row) {
                    //insertion id pdv dans dataLinDev
                    $row['pdv_id']= $pdv_id;

                    //famille_id
                    $row['famille_id'] = $row["famille"]["id"];
                    unset( $row["famille"]);

                    //update to table
                    $linDev = LinDev::firstOrNew(array('pdv_id'=>$row['pdv_id'], 'famille_id'=>$row['famille_id']));
                    $linDev->lineaire_developpe = $row['lineaire_developpe'];
                    $linDev->save();

                    //LinDev::create($row);
                }

                //////////////////////////loclin
                //delete lineaire dev
                $pdvaLocLin = LocLin::where('pdv_id', '=',$pdv_id)->get(array('marque_id', 'pdv_id'))->toArray();

                foreach($pdvaLocLin as $key=>$row) {
                    $delete = true;
                    foreach($dataLocLin as $item) {
                        if($row['marque_id'] == $item["marque"]["id"] ) $delete = false;
                    }

                    if($delete) LocLin::where('pdv_id', '=',$pdv_id)->where('marque_id', '=', $row['marque_id'])->delete();
                }

                //location lin (marque_pdv)
                foreach($dataLocLin as $row) {
                    //insertion id pdv dans dataLocLin
                    $row['pdv_id']= $pdv_id;

                    //marque_id
                    $row['marque_id'] = $row["marque"]["id"];
                    unset( $row["marque"]);

                    //pivot
                    $dataPlvStore = array_merge($row["plv_out_store"], $row["plv_in_store"] ); //json_decode($request['plv_store'], true); // + id marque_pdv (loc_lin)
                    unset($row['plv_in_store']);
                    unset($row['plv_out_store']);

                    // change id to plv_id
                    foreach($dataPlvStore as $key=>$dataPlv) {
                        $dataPlvStore[$key]['plv_id'] = $dataPlvStore[$key]['id'];
                        unset($dataPlvStore[$key]['id']);

                        //delete unused index
                        unset($dataPlvStore[$key]['nom']);
                        unset($dataPlvStore[$key]['type']);
                    }

                    //update to table loc lin
                    $locLin= LocLin::firstOrNew(array('pdv_id'=>$row['pdv_id'], 'marque_id'=>$row['marque_id']));
                    $locLin->duree = $row['duree'];
                    $locLin->prix = $row['prix'];
                    $locLin->save();

                    //pivot plv-store marque_pdv_plv)
                    $locLin->plvs()->detach();
                    foreach($dataPlvStore as $row) {
                        $locLin->plvs()->attach($row['plv_id'],['quantite'=>$row['quantite'],'date'=>$row['date']]);
                    }

                }

                //commit transaction
                DB::commit();

                //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
                $type_activity_id = ActivityType::where('nom', '=', 'Insertion')->first()->id;
                $message = 'Vient de modifier un point de vente. <a href="/pdvs/'.$pdv_id.'">Consulter</a>';
                FadercoHelper::setActivity($request->getClientIp(), $dataInfoGen['localisation_lat'] , $dataInfoGen['localisation_long'] , $message, $type_activity_id, $user_id);

                return Response::json(array(array('id' => $pdv_id)));
                //return Response::json(FadercoResponse::getSUCCCESS());
            }

            return Response::json(FadercoResponse::getINPUTVALIDERR());
        }
        catch(ModelNotFoundException $e) {
            Log::info($e->getMessage());
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
        catch(Exception $e)
        {
            DB::rollback();
            Log::info($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    /*
        public function destroy($id)
        {
            // add concept of relation with 'Marque' when deleting a 'Famille'
            try
            {
                 $product= Produit::findOrFail($id);
                if($product->delete())
                return Response::json(FadercoResponse::getSUCCCESS());
            }
            catch (Exception $e)
            {
                return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
            }
        }*/

}
