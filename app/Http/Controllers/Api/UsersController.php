<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Oauth;
use Exception;
use FadercoHelper;
use FadercoResponse;
use Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Log;
use Response;
use Validator;

class UsersController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID(Input::get('access_token'));

            //pair token user_id invalide
            if ($user_id != $id) return Response::json(FadercoResponse::getAUTHERR());

            $user= User::findOrfail($id);

            $oauth_id = base64_encode($user->username);
            $oauth_client = Oauth::findOrFail($oauth_id);

            $rules = [
                'nom' => 'required|max:255',
                'username' => 'required|min:4|max:255|unique:users,username,'.$user->id,
                'password' => 'required|min:4',
                'telephone' => 'required|min:10',
                'email' => 'required|email',
            ];

            if(!$request->has('password')) $request->merge(array('password' => $user->password));

            $validator = Validator::make($request->toArray(), $rules);

            if($validator->fails()) {
                return Response::json(FadercoResponse::getINPUTVALIDERR());
            }

            if (Hash::needsRehash($request['password']))
            {
                $request['password'] = Hash::make($request['password']);
            }

            $user->nom=$request['nom'];
            $user->username=$request['username'];
            $user->password=$request['password'];
            $user->telephone=$request['telephone'];
            $user->email=$request['email'];

            $user->save();

            //oauth client
            $id = base64_encode($user->username);
            $secret = md5($user->username.'overgen'.$user->username);

            $oauth_client->id = $id;
            $oauth_client->secret = $secret;
            $oauth_client->name = $user->username;
            $oauth_client->save();

            return Response::json(FadercoResponse::getSUCCCESS());

        }
        catch(Exception $e) {
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }
}
