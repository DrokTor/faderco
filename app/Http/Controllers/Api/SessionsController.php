<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ActivityType;
use App\Models\Role;
use App\Models\Versioning;
use Authorizer;
use FadercoHelper;
use FadercoResponse;
use Auth;
use DateTime;
use Validator;
use Response;
use Illuminate\Http\Request;
Use Exception;

class SessionsController extends Controller
{
    public function __construct()
    {
        /*$this->middleware('oauth');
        $this->middleware('oauth-user');*/
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|min:4',
            'password' => 'required',
        ]);
    }

    public function store(Request $request)
    {
        try {
            //$data = $request->except('grant_type','client_id', 'client_secret');
            $data = $request->all();

            $validator = $this->validator($data);

            if($validator->fails()) {
                return Response::json(FadercoResponse::getINPUTVALIDERR());
            }

            if(Auth::attempt($data)) {

                //update last login time
                Auth::user()->updated_at = new DateTime();
                Auth::user()->save();

                //récuperer les informations de l'utilisateur authentifié
                $userInfo = Auth::user();
                $userInfo['role'] = Role::findOrFail($userInfo['role_id'])->designation;
                unset($userInfo['role_id']);

                //token info
                $grant_type= 'client_credentials';
                $client_id = base64_encode($userInfo['username']);
                $client_secret = FadercoHelper::getClientSecret($userInfo['username']);

                $request->grant_type = $grant_type;
                $request->client_id = $client_id;
                $request->client_secret = $client_secret;

                $userInfo['access_token'] = FadercoHelper::getToken($userInfo['username'], $grant_type, $client_id, $client_secret);

                if($userInfo['access_token'] == false) return Response::json(FadercoResponse::getAUTHTOKENERR());

                //db version
                $userInfo['version'] = Versioning::first()->version;

                //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
                $type_activity_id = ActivityType::where('nom', '=', 'Connexion')->first()->id;
                $message = 'Vient de se connecter a l\'application. <a href="/users">Consulter la liste des utilisateurs</a>';
                FadercoHelper::setActivity($request->getClientIp(), null , null , $message, $type_activity_id, $userInfo['id']);

                return Response::json(array($userInfo));
            }

            return Response::json(FadercoResponse::getAUTHERR());
        }
        catch (Exception $e) {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function getUser() {
        try {
            $id = Authorizer::getResourceOwnerId();
            $user = User::find($id);
            return response()->json($user);
        }
        catch (Exception $e) {
            return Response::json(FadercoResponse::getSGBDERR());
        }

    }

    public function checkToken($token) {
        try {
            if(FadercoHelper::checkToken($token)) return Response::json(FadercoResponse::getSUCCCESS());

            return Response::json(FadercoResponse::getAUTHTOKENINVALID());
        }
        catch (Exception $e) {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }
}
