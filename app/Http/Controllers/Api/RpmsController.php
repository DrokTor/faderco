<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\UploadPhotoException;
use App\Http\Controllers\Controller;
use App\Models\ActivityType;
use App\Models\Produit_Rpm;
use App\Models\Rpm;
use App\Models\RpmPhoto;
use DB;
use Exception;
use FadercoHelper;
use FadercoResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Image;
use Intervention\Image\Exception\NotReadableException;
use Response;
use Validator;
use Log;

class RpmsController extends Controller
{
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'access_token' => 'required',
            'assortiment_recommande' => 'required',
            'bloc_produit' => 'required',
            'planogramme' => 'required',
            'mise_avant_tg' => 'required',
            /*'observation' => 'required',*/
            'execution_frein_id' => 'required|integer',
            'pdv_id' => 'required|integer',
            'marque_id' => 'required|integer',
            'user_id' => 'required|integer',
            'produit_rpm' => 'required|json',
            /*'photos' => 'required',*/
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = $this->validator($request->toArray());

            if ($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            //recuperer le id du user
            $user_id = FadercoHelper::getUserID(Input::get('access_token'));

            $dataInfoGen = $request->except('access_token', 'produit_rpm', 'photos');

            //todo better
            $dataInfoGen['assortiment_recommande'] = $dataInfoGen['assortiment_recommande'] == "True" ? true : false;
            $dataInfoGen['bloc_produit'] = $dataInfoGen['bloc_produit'] == "True" ? true : false;
            $dataInfoGen['planogramme'] = $dataInfoGen['planogramme'] == "True" ? true : false;
            $dataInfoGen['mise_avant_tg'] = $dataInfoGen['mise_avant_tg'] == "True" ? true : false;

            //execution_frein_id
            //if($dataInfoGen["execution_frein_id"] == 0) $dataInfoGen['execution_frein_id'] =  null;
            if ($dataInfoGen["execution_frein_id"] == 0) $dataInfoGen['execution_frein_id'] = 1;

            //pair token user_id invalide
            if ($user_id != $dataInfoGen['user_id']) return Response::json(FadercoResponse::getAUTHERR());

            //produit rpm
            $dataProduitRpm = json_decode($request['produit_rpm'], true);

            //photos envoyées
            if ($request->hasFile('photos')) $photos = $request['photos'];
            else $photos = null;

            //validation du json produit rpm
            if ($dataProduitRpm == null) return Response::json(FadercoResponse::getINPUTVALIDERR());

            // Start transaction
            DB::beginTransaction();

            //rpm creation
            $rpm = Rpm::create($dataInfoGen);
            $rpm_id = $rpm->id;

            //produit rpm
            foreach ($dataProduitRpm as $row) {

                //insertion id rpm dans ProduitRpm
                $row['rpm_id'] = $rpm_id;

                //produit_id
                $row['produit_id'] = $row["produit"]["id"];
                unset($row["produit"]);

                //rupture_reason_id
                if ($row["rupture_reason"]["id"] != 0) {
                    $row['rupture_reason_id'] = $row["rupture_reason"]["id"];
                } else $row['rupture_reason_id'] = null;

                unset($row["rupture_reason"]);

                //compatibilité avec l'ancienne version mobile
                $row['pvc'] = $row['pvc'] == '' ? 0 : $row['pvc'];
                $row['facing'] = $row['facing'] == '' ? 0 : $row['facing'];
                $row['facing_ma'] = $row['facing_ma'] == '' ? 0 : $row['facing_ma'];

                //store to table
                //if ($row['pvc'] != '') {
                    Produit_Rpm::create($row);
                //}
            }

            //upload photos
            if ($photos != null) {
                $photos_count = count($photos);

                //repertoire des photos
                $path = 'images/rpms/' . $rpm_id;

                if ($photos_count > 0) {

                    FadercoHelper::createDirectory($path);

                    foreach ($photos as $photo) {

                        if($photo->isValid()) {

                            $name = strtolower(preg_replace('/\s+/', '', $photo->getClientOriginalName()));
                            $extension =$photo->getClientOriginalExtension();

                            if($extension != "" && $extension != null) {
                                if(strpos($name, $extension) === false) $photoname = time() . '_' . rand() . '_' .$name.'.'.$extension;
                                else $photoname = time() . '_' . rand() . '_' .$name;
                            }
                            else $photoname = time() . '_' . rand() . '_' .$name;


                            if (FadercoHelper::moveFile($photo, $photoname, $path)) {

                                $rpm_photo = RpmPhoto::create(array('url' => $path . '/' . $photoname, 'rpm_id' => $rpm_id));

                                // open an image file
                                $img = Image::make($rpm_photo->url);
                                $img->resize(320, 240);
                                if($extension != "" && $extension != null) {
                                    $resized_url = str_replace(".".$extension,"", $rpm_photo->url);
                                    $resized_url = $resized_url."_320x240.".$extension;
                                }
                                else {
                                    $resized_url = $rpm_photo->url."_320x240";
                                }

                                $img->save($resized_url);
                            }
                        }
                        else {
                            FadercoHelper::deleteDirectory($path);
                            throw new UploadPhotoException("File Corrupted on upload.");
                            break;
                        }
                    }
                }
            }

            //commit transaction
            DB::commit();

            //Log activity ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression']
            $type_activity_id = ActivityType::where('nom', '=', 'Insertion')->first()->id;
            $message = 'Vient d\'ajouter un nouveau rapport de merchandsing. <a href="/rpms/' . $rpm_id . '">Consulter</a>';
            FadercoHelper::setActivity($request->getClientIp(), null, null, $message, $type_activity_id, $user_id);

            return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch (UploadPhotoException $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getFILEUPLOADERR());
        }
        catch (NotReadableException $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getFILEUPLOADERR());
        }
        catch (Exception $e) {
            DB::rollback();
            Log::error($e->getMessage());
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }
}
