<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\Famille;
use App\Models\Marque;
use Illuminate\Http\Request;
use Exception;

class FamillesController extends Controller
{

    public function __construct()
    {

    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required',
            'marque_id'=>'required',
            'access_token'=>'required',
            //'old_marque_id'=>'required',
        ]);
    }

    public function index()
    {
        $familles= Famille::get(array("id", "nom"));//with('marques')->

        return Response::json($familles);
    }

    public function store(Request $request)
    {
        try
        {
            $data= $request->all();

            $validator = $this->validator($data);

            if($validator->fails()) return Response::json(FadercoResponse::getINPUTVALIDERR());

            $marque= Marque::findOrFail($data['marque_id']);

            $marque->familles()->create($data);

            return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch(Exception $ex)
        {
            return Response::json(FadercoResponse::getSGBDERR());
        }
    }

    public function show($id)
    {
        try
        {
            $famille= Famille::findOrFail($id, array("id", "nom"));//with('marques')->

            return Response::json(array($famille));
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }

    public function update( Request $request ,$id)
    {
        try
        {
            $famille= Famille::findOrFail($id);

            $data= $request->all();

            $famille->update($data);
            $new_marque= Marque::findOrFail($data['marque_id']);
            $old_marque= Marque::findOrFail($data['old_marque_id']);
            $famille= Famille::findOrFail($id);

            $old_marque->familles()->detach($famille);
            $new_marque->familles()->attach($famille);

            return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch (Exception $e)
        {
        return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }

    public function destroy($id)
    {
        // add concept of relation with 'Marque' when deleting a 'Famille'
        try
        {
            $famille= Famille::findOrFail($id);
            if($famille->delete())
                return Response::json(FadercoResponse::getSUCCCESS());
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }

}
