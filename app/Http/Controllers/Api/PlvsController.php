<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use FadercoHelper;
use FadercoResponse;
use Validator;
use Response;
use App\Models\Plv;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Input;

class PlvsController extends Controller
{
    //
    protected function validator(array $data)
    {
        return Validator::make($data, [

            'access_token'=>'required',
        ]);
    }

    public function index()
    {
        $type=Input::get('type');
        $plv= (!$type)?Plv::all():Plv::where(['type'=>$type])->get();//all();

        return Response::json($plv);
    }

    public function show($id)
    {
        try
        {
            $plv= Plv::findOrFail($id);
            return Response::json(array($plv));
        }
        catch (Exception $e)
        {
            return Response::json(FadercoResponse::getSGBDROWNOTFOUND());
        }
    }
}
