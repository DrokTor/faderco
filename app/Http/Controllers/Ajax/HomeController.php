<?php

namespace App\Http\Controllers\Ajax;

use App\Models\Message;
use App\Models\Pdv;
use App\Models\Rp;
use App\Models\Rpc;
use App\Models\Rpm;
use App\Models\Rvc;
use Carbon\Carbon;
use Exception;
use Flash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Log;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            if($request->ajax()) {
                $yesterday = Carbon::today()->subWeek();
                $today = Carbon::today()->subWeek()->addDay();

                $charts =array();

                for($i=0; $i< 8; $i++) {
                    $pdvs = Pdv::where('created_at', '>', $yesterday)->where('created_at', '<=', $today)->count();
                    $validated = Pdv::where('etat', '=', true)->where('updated_at', '>', $yesterday)->where('updated_at', '<=', $today)->count();
                    $rpms = Rpm::where('created_at', '>', $yesterday)->where('created_at', '<=', $today)->count();
                    $rvcs = Rvc::where('created_at', '>', $yesterday)->where('created_at', '<=', $today)->count();
                    $rps = Rp::where('created_at', '>', $yesterday)->where('created_at', '<=', $today)->count();
                    $rpcs = Rpc::where('created_at', '>', $yesterday)->where('created_at', '<=', $today)->count();

                    $value = array('period'=> $yesterday->diffForHumans(Carbon::setLocale("fr")),
                        'pdvs' => $pdvs, 'validated' => $validated, 'rpms'=> $rpms,
                        'rvcs' => $rvcs, 'rps' => $rps, 'rpcs'=> $rpcs
                    );
                    array_push($charts, $value);

                    $today->addDay();
                    $yesterday->addDay();
                }

                return Response::json(array('charts'=> $charts));
            }
            else return Response::json(\FadercoResponse::getREQTYPEERR());
        }
        catch(Exception $e) {
            Log::error($e->getMessage());
            return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
        }
    }
}
