<?php

namespace App\Http\Controllers\Ajax;

use App\Models\Wilaya;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use Response;

class RegionsController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        try {
            if($request->ajax()) {

                $wilayas = Wilaya::where('region_id', '=', $id)->get(array('id as value', 'nom as text'));

                return Response::json($wilayas);
            }
            else return Response::json(\FadercoResponse::getREQTYPEERR());
        }
        catch(Exception $e) {
            Log::error($e->getMessage());
            return Response::json('Une erreur s\'est produite, veuillez réessayer', 500);
        }
    }
}
