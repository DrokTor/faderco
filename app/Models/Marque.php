<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 
class Marque extends Model
{
    protected $fillable = ['nom', 'loclin'];

    public function familles()
    {
        return $this->belongsToMany('App\Models\Famille')
            ->withPivot('id')
            ->withTimestamps();
    }

    public function pdvs()
    {
        return $this->belongsToMany('App\Models\Pdv')
            ->withPivot('duree','prix')
            ->withTimestamps();
    }
}
