<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RpmPhoto extends Model
{
    protected $fillable = [
        'url', 'rpm_id'
    ];

    public function rpm()
    {
        return $this->belongsTo('App\Models\Rpm');
    }
}
