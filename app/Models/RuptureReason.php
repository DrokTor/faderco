<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RuptureReason extends Model
{
    //
    protected $table ="rupture_reasons";

    protected $fillable=[
      'nom'
    ];
}
