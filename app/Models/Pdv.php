<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Pdv extends Model
{
    protected $fillable = [
        'nom', 'localisation_lat', 'localisation_long', 'nb_sortie_caisse',
        'user_id', 'region_id', 'wilaya_id', 'commune_id', 'canal_achat_id',
        'type_pdv_id', 'surface_pdv_id', 'zone_chalandise_id', 'etat'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function wilaya()
    {
        return $this->belongsTo('App\Models\Wilaya');
    }

    public function commune()
    {
        return $this->belongsTo('App\Models\Commune');
    }

    public function canalachat()
    {
        return $this->belongsTo('App\Models\CanalAchat');
    }

    public function pdvtype()
    {
        return $this->belongsTo('App\Models\PdvType');
    }

    public function pdvsurface()
    {
        return $this->belongsTo('App\Models\PdvSurface');
    }

    public function zonechalandise()
    {
        return $this->belongsTo('App\Models\ZoneChalandise');
    }

    //famille-pdv
    public function LinDevs()
    {
        return $this->belongsToMany('App\Models\Famille')
            ->withPivot('id', 'lineaire_developpe')
            ->withTimestamps();
    }

    //marque-pdv
    public function LocLins()
    {
        return $this->belongsToMany('App\Models\Marque')
            ->withPivot('id','duree','prix')
            ->withTimestamps();
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(Carbon::setLocale("fr"));
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(Carbon::setLocale("fr"));
    }
}
