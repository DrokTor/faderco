<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plv extends Model
{
    protected $fillable=['type', 'nom'];

    public function locLins()
    {
        return $this->belongsToMany('App\Models\LocLin','marque_pdv_plv','marque_pdv_id','plv_id')
            ->withPivot('quantite', 'date');
    }
}
