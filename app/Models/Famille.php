<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 
class Famille extends Model
{
    //
    protected $fillable=[
      'nom'
    ];

    public function marques()
    {
      return $this->belongsToMany('App\Models\Marque')
          ->withPivot('id')
          ->withTimestamps();
    }

    public function pdvs()
    {
        return $this->belongsToMany('App\Models\Pdv')
            ->withPivot('id', 'lineaire_developpe')
            ->withTimestamps();
    }
}
