<?php
/**
 * Created by PhpStorm.
 * User: Amine
 * Date: 24/10/2014
 * Time: 12:36
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Oauth_Session extends Model {
    protected $table = 'oauth_sessions';

    public function oauth_acces_token() {
       return $this->hasOne('App\Models\Oauth_Access_Token');
    }
} 