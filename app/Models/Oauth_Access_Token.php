<?php
/**
 * Created by PhpStorm.
 * User: Amine
 * Date: 24/10/2014
 * Time: 12:29
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Oauth_Access_Token extends Model {

    protected $table = 'oauth_access_tokens';

    public function oauth_session() {
        return $this->hasOne('App\Models\Oauth_Session');
    }
} 