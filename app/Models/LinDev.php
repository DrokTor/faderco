<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinDev extends Model
{
    //
    protected $table= 'famille_pdv';

    protected $fillable=['pdv_id', 'famille_id', 'lineaire_developpe'];

    public function pdv()
    {
        return $this->belongsTo('App\Models\Pdv');
    }

    public function famille()
    {
        return $this->belongsTo('App\Models\Famille');
    }

}
