<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    protected $fillable=[
        'nom',
        'up',
        'pc',
        'largeur',
        'longueur',
        'hauteur',
        'famille_marque_id',
        /*'famille_id',
        'marque_id',*/
    ];

    public function famille_marque()
    {
        return $this->belongsTo('App\Models\Famille_Marque');
    }

    public function famille()
    {
        return $this->belongsTo('App\Models\Famille');
    }

    public function marque()
    {
        return $this->belongsTo('App\Models\Marque');
    }
}
