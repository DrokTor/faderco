<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RvcPhoto extends Model
{
    protected $fillable = [
        'url', 'rvc_id'
    ];

    public function rvc()
    {
        return $this->belongsTo('App\Models\Rvc');
    }
}
