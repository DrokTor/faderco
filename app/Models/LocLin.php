<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocLin extends Model
{
    protected $table= 'marque_pdv';

    protected $fillable=['pdv_id', 'marque_id', 'duree', 'prix'];

    public function plvs()
    {
        return $this->belongsToMany('App\Models\Plv','marque_pdv_plv','marque_pdv_id','plv_id' )
            ->withPivot('quantite', 'date')
            ->withTimestamps();
    }
}
