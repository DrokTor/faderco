<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rvc extends Model
{
    protected $fillable = [
        'reference', 'autre', 'periode', 'observation',
        'action_type_mechanism_id', 'pdv_id', 'famille_id', 'user_id'
    ];

    public function action_type_mechanism() {
        return $this->belongsTo('App\Models\ActionType_Mechanism');
    }

    public function pdv()
    {
        return $this->belongsTo('App\Models\Pdv');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function famille()
    {
        return $this->belongsTo('App\Models\Famille');
    }

    public function rvc_photos()
    {
        return $this->hasMany('App\Models\Rpm');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(Carbon::setLocale("fr"));
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(Carbon::setLocale("fr"));
    }
}
