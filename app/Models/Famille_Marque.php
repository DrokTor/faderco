<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Famille_Marque extends Model
{
    protected $table = 'famille_marque';

    public function marque()
    {
        return $this->belongsTo('App\Models\Marque');
    }

    public function famille()
    {
        return $this->belongsTo('App\Models\Famille');
    }

    public function produits()
    {
        return $this->hasMany('App\Models\Produit');
    }
}
