<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit_Rp extends Model
{
    protected $table = 'produit_rp';

    protected $fillable = [
        'produit_id', 'rp_id', 'pad', 'pvc'
    ];
}
