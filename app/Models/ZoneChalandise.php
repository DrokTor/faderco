<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ZoneChalandise extends Model
{
    //
    protected $table ='zone_chalandises';

    protected $fillable=[
      'nom',
    ];
}
