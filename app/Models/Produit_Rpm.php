<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit_Rpm extends Model
{
    protected $table = 'produit_rpm';

    protected $fillable = [
        'produit_id', 'rpm_id', 'up', 'pc',
        'pvc', 'facing', 'facing_ma', 'dispo',
        'stock_reserve', 'rupture_reason_id'
    ];
    public function rupture_reason()
    {
        return $this->belongsTo('App\Models\RuptureReason');
    }
}
