<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mechanism extends Model
{
    protected $fillable=['nom'];
    public function actiontypes()
    {
        return $this->belongsToMany('App\Models\ActionType')
            ->withPivot('id');
    }
}
