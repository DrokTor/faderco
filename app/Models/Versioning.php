<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Versioning extends Model
{
    protected $table ='versioning';

    protected $fillable=['version'];
}
