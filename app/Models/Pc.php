<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pc extends Model
{
    protected $fillable = [
        'marque', 'reference', 'pad', 'pvc', 'rpc_id'
    ];

    public function rpc()
    {
        return $this->belongsTo('App\Models\Rpc');
    }
}