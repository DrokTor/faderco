<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rpc extends Model
{
    protected $fillable = [
        'pdv_id', 'marque_id', 'user_id'
    ];

    public function pcs()
    {
        return $this->hasMany('App\Models\Pc');
    }

    public function pdv()
    {
        return $this->belongsTo('App\Models\Pdv');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function marque()
    {
        return $this->belongsTo('App\Models\Marque');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(Carbon::setLocale("fr"));
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(Carbon::setLocale("fr"));
    }
}
