<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PdvSurface extends Model
{
    //
    protected $table='surface_pdvs';

    protected $fillable=[
      'nom',
    ];
}
