<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rpm extends Model
{
    protected $fillable = [
        'assortiment_recommande', 'bloc_produit', 'planogramme', 'mise_avant_tg', 'observation',
        'execution_frein_id', 'pdv_id', 'marque_id', 'user_id'
    ];

    public function execution_frein()
    {
        return $this->belongsTo('App\Models\ExecutionFrein');
    }

    public function pdv()
    {
        return $this->belongsTo('App\Models\Pdv');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function marque()
    {
        return $this->belongsTo('App\Models\Marque');
    }

    public function produits_rpms()
    {
        return $this->belongsToMany('App\Models\Produit')
            ->withPivot('produit_id', 'rpm_id', 'up', 'pc', 'pvc', 'facing', 'facing_ma', 'dispo', 'stock_reserve', 'rupture_reason_id')
            ->withTimestamps();
    }

    public function rpm_photos()
    {
        return $this->hasMany('App\Models\Rpm');
    }

    public function getCreatedAtAttribute($date)
    {
        /*$d = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        return $d->format('d-m-Y H:i').' ('.$d->diffForHumans(Carbon::setLocale("fr")).')';*/
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(Carbon::setLocale("fr"));
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(Carbon::setLocale("fr"));
    }
}
