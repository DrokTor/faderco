<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityType extends Model
{
    protected $table ='type_activities';

    protected $fillable = ['nom'];

    public function activities()
    {
        return $this->hasMany('App\Models\Activity');
    }
}
