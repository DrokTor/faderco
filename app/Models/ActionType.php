<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActionType extends Model
{
    protected $fillable=['nom'];

    public function mechanisms()
    {
        return $this->belongsToMany('App\Models\Mechanism')
            ->withPivot('id');
    }
}
