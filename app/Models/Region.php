<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{

    protected $fillable=[
      'nom'
    ];
    public function wilayas()
    {
        return $this->hasMany('App\Models\Wilaya');
    }
}
