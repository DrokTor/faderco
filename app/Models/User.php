<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom', 'username', 'password', 'telephone', 'email', 'secteur', 'fonction', 'responsable', 'avatar','role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function activities()
    {
        return $this->hasMany('App\Models\Activity');
    }

    public function pdvs()
    {
        return $this->hasMany('App\Models\Pdv');
    }

    public function messages()
    {
        return $this->belongsToMany('App\Models\Message')
            ->withPivot('etat')
            ->withTimestamps();
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y H:i');
    }

    public function getUpdatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans(Carbon::setLocale("fr"));
    }
}
