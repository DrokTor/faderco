<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{

    protected $fillable=[
      'nom',
      'code',
      'wilaya_id'  
    ];
    public function wilaya()
    {
        return $this->belongsTo('App\Models\Wilaya');
    }
}
