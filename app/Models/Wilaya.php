<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wilaya extends Model
{

    protected $fillable=[
      'nom',
      'code',
      'region_id'
    ];
    public function communes()
    {
        return $this->hasMany('App\Models\Commune');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }
}
