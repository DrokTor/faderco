<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PdvType extends Model
{
    //

    protected $table='type_pdvs';

    protected $fillable=[
      'nom',
    ];
}
