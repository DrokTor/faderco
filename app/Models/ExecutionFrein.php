<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExecutionFrein extends Model
{
    //
    protected $table ="execution_freins";

    protected $fillable=[
      'nom'
    ];
}
