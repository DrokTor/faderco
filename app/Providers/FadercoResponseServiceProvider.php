<?php

namespace App\Providers;


use App\Overgen\Faderco\FadercoResponse;
use Illuminate\Support\ServiceProvider;

class FadercoResponseServiceProvider extends ServiceProvider
{
    protected $defer = true;
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Shortcut so developers don't need to add an Alias in app/config/app.php
        /*$loader = AliasLoader::getInstance();
        $loader->alias('FadercoResponse', 'App\Overgen\Facades\FadercoResponse');*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /*$this->app->bind('FadercoResponse', function() {
            return new FadercoResponse();
        });*/

        $this->app->singleton('FadercoResponse', function()
        {
            return $this->app->make('FadercoResponse');
        });
    }

    public function provides()
    {
        return ['FadercoResponse'];
    }
}
