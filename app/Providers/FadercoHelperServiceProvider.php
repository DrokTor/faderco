<?php

namespace App\Providers;

use App\Overgen\Faderco\FadercoHelper;
use Illuminate\Support\ServiceProvider;

class FadercoHelperServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /*$this->app->bind('FadercoHelper', function() {
            return new FadercoHelper();
        });*/

        $this->app->singleton('FadercoHelper', function()
        {
            return $this->app->make('FadercoHelper');
        });
    }

    public function provides()
    {
        return ['FadercoHelper'];
    }
}
