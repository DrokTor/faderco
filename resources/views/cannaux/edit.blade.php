@extends('layouts.master')
@section('title', 'Cannaux d\'achats')

@section('content_header')
<h3>Modifier un canal</h3>
@stop
@section('content')

  <section class="content-header">
      <h1>
          Cannaux d'achats
          <small>Edit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/cannaux">Cannaux d'achats</a></li>
          <li class="active">Edit</li>
      </ol>
  </section>
  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 id="productID" class="box-title" data-id="{{$canal['id']}}">Canal</h3>
               </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action={{URL::to('/').'/cannaux/'.$canal['id']}}>
                  {{method_field('PATCH')}}
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input value="{{$canal['nom']}}" name="nom" class="form-control" id="" placeholder="nom du canal" type="text">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
  </section>

@stop
