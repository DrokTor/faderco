@extends('layouts.master')
@section('title', 'Wilayas')

@section('content_header')
<h3>Modifier une Wilaya</h3>
@stop
@section('content')

  <section class="content-header">
      <h1>
          Wilaya
          <small>Edit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/wilayas">Wilayas</a></li>
          <li class="active">Edit</li>
      </ol>
  </section>
  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 id="productID" class="box-title" data-id="{{$wilaya['id']}}">Wilaya</h3>
               </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action={{URL::to('/').'/wilayas/'.$wilaya['id']}}>
                  {{method_field('PATCH')}}
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input value="{{$wilaya['nom']}}" name="nom" class="form-control" id="" placeholder="nom de la wilaya" type="text">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Code</label>
                    <input value="{{$wilaya['code']}}" name="code" class="form-control" id="" placeholder="code de la wilaya" type="text">
                    @if ($errors->has('code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('code') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('region_id') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">wilaya</label><br>
                    <select name="region_id">
                    @foreach($regions as $key => $value)
                      <option  @if($wilaya['region_id']==$value['id'] ) {{'selected=selected'}} @endif  value="{{$value['id']}}">{{$value['nom']}}</option>
                    @endforeach
                    </select>
                    @if ($errors->has('region_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('region_id') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
</section>

@stop
