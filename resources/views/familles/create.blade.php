@extends('layouts.master')
@section('title', 'Familles')

@section('content_header')
<h3>Ajouter une famille</h3>
@stop

@section('content')
  <section class="content-header">
      <h1>
          Familles
          <small>Create</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/familles">Familles</a></li>
          <li class="active">Create</li>
      </ol>
  </section>
  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Famille</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action={{URL::to('/').'/familles'}}>
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input name="nom" class="form-control" id="" placeholder="nom de la famille" type="text">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Marques</label>

                    @foreach($marques as $key => $value)
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="marques[{{$value['id']}}]">
                          {{$value['nom']}}
                        </label>
                      </div>
                    @endforeach

                  </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
</section>

@stop
