@extends('layouts.master')
@section('title', 'Utilisateurs')
@section('header')
    @parent
    <!-- page level css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/login.css') }}" />
    <!-- end of page level css -->
@stop

@section('content')
    <div id="login" class="animate form">
        <form action="{{ url('/users') }}" autocomplete="off" method="post" role="form">
            <h3 class="">
                <img src="{{ asset('assets/img/logo.png') }}" alt="faderco logo">
            </h3>
            <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                <label style="margin-bottom:0px;" for="nom" class="youmail">
                    <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    Nom et prénom
                </label>
                <input id="nom" name="nom" required type="text" placeholder="Nom Prénom" />
                @if ($errors->has('nom'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nom') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                <label style="margin-bottom:0px;" for="last_name" class="youmail">
                    <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    Identifiant
                </label>
                <input id="last_name" name="username" required type="text" placeholder="Psoeudonime" />
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                <label style="margin-bottom:0px;" for="telephone" class="youmail">
                    <i class="livicon" data-name="phone" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    Téléphone
                </label>
                <input id="telephone" name="telephone" required type="text" placeholder="0999123456" />
                @if ($errors->has('telephone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('telephone') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <label style="margin-bottom:0px;" for="email" class="youmail">
                    <i class="livicon" data-name="mail" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    E-mail
                </label>
                <input id="email" name="email" required type="email" placeholder="youremail@email.com" />
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <label style="margin-bottom:0px;" for="password" class="youpasswd">
                    <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    Mot de passe
                </label>
                <input id="password" name="password" required type="password" placeholder="eg. X8df!90EO" />
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('secteur') ? ' has-error' : '' }}">
                <label style="margin-bottom:0px;" for="secteur" class="youmail">
                    <i class="livicon" data-name="location" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    Secteur
                </label>
                <input id="secteur" name="secteur" required type="text" placeholder="Alger" />
                @if ($errors->has('secteur'))
                    <span class="help-block">
                        <strong>{{ $errors->first('secteur') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('fonction') ? ' has-error' : '' }}">
                <label style="margin-bottom:0px;" for="fonction" class="youmail">
                    <i class="livicon" data-name="tag" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    Fonction
                </label>
                <input id="fonction" name="fonction" required type="text" placeholder="Merchandiser" />
                @if ($errors->has('fonction'))
                    <span class="help-block">
                        <strong>{{ $errors->first('fonction') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('responsable') ? ' has-error' : '' }}">
                <label style="margin-bottom:0px;" for="responsable" class="youmail">
                    <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    Responsable
                </label>
                <input id="responsable" name="responsable" required type="text" placeholder="Nom Responsable" />
                @if ($errors->has('responsable'))
                    <span class="help-block">
                        <strong>{{ $errors->first('responsable') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('role') ? ' has-error' : '' }}">
                <label style="margin-bottom:0px;" for="role_id" class="youmail">
                    Rôle
                </label>
                <select name="role_id" class="form-control">
                    @foreach($roles as $key => $value)
                        @if($value['designation'] == 'merchandiser') <option value="{{$value['id']}}" selected="selected">{{ ucfirst($value['designation']) }}</option>
                        @else <option value="{{$value['id']}}">{{ ucfirst($value['designation']) }}</option>
                        @endif
                    @endforeach
                </select>
                @if ($errors->has('role_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('role_id') }}</strong>
                    </span>
                @endif
            </div>
            <p class="signin button">
                <input type="submit" class="btn btn-primary" value="Enregistrer" />
            </p>
        </form>
    </div>
@endsection
@section('footer')
@parent
<!--livicons-->
<script src="{{ asset('assets/js/raphael-min.js') }}"></script>
<script src="{{ asset('assets/js/livicons-1.4.min.js') }}"></script>
<script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/login.js') }}" type="text/javascript"></script>
<!-- end of global js -->
@endsection
