@extends('layouts.master')
@section('title', 'Utilisateurs')
@section('header')
    @parent
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Utilisateurs
            <small>Modification</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active">Utilisateurs</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <section class="col-lg-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Modification Utilisateur</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <form role="form" method="post" action={{URL::to('/'). '/users/'.$user[ 'id']}}>
                            {{method_field('PATCH')}}
                            <div class="box-body">
                                <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                                    <label for="nom">Nom</label>
                                    <input value ="{{ $user['nom'] }}" name="nom" class="form-control" id="" type="text">
                                    @if ($errors->has('nom'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('nom') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                    <label for="username">Username</label>
                                    <input value="{{$user[ 'username']}}" name="username" class="form-control" id="username" type="text">
                                    @if ($errors->has('username'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('username') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                    <label for="telephone">Téléphone</label>
                                    <input value="{{$user['telephone']}}" id="telephone" class="form-control" name="telephone" type="text" />
                                    @if ($errors->has('telephone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('telephone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Email</label>
                                    <input value="{{$user['email']}}" name="email" class="form-control" id="email" type="text">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">Mot de passe</label>
                                    <input name="password" class="form-control" id="" type="password">
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('secteur') ? ' has-error' : '' }}">
                                    <label for="secteur">Secteur</label>
                                    <input value="{{$user['secteur']}}" id="secteur" class="form-control" name="secteur" type="text" />
                                    @if ($errors->has('secteur'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('secteur') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('fonction') ? ' has-error' : '' }}">
                                    <label for="fonction">Fonction</label>
                                    <input value="{{$user['fonction']}}" id="fonction" class="form-control" name="fonction" type="text" />
                                    @if ($errors->has('fonction'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('fonction') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('responsable') ? ' has-error' : '' }}">
                                    <label for="fonction">Responsable</label>
                                    <input value="{{$user['responsable']}}" id="responsable" class="form-control" name="responsable" type="text" />
                                    @if ($errors->has('responsable'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('responsable') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('role_id') ? ' has-error' : '' }}">
                                    <label style="margin-bottom:0px;" for="role_id" class="youmail">
                                        Rôle
                                    </label>
                                    <select name="role_id" class="form-control">
                                        @foreach($roles as $role)
                                            @if($user['role_id'] == $role['id']) <option value="{{$role['id']}}" selected="selected">{{$role['designation']}}</option>
                                            @else <option value="{{$role['id']}}" >{{$role['designation']}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @if ($errors->has('role_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('role_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                {{--<div class="form-group">
                                    <label for="exampleInputFile">Avatar</label>
                                    <input id="exampleInputFile" type="file">
                                </div>--}}
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Enregistrer</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box -->
            </section>
        </div>
    </section>
@stop
@section('footer')
    @parent
@stop

