@extends('layouts.master')

@section('content_header')
<h3>User</h3>
@stop

@section('content')


 <div class="box">

             <!-- /.box-header -->
             <div class="box-body">
               <table class="table table-bordered">
                 <tbody>
                   <tr>
                     <td>Avatar</td>
                     <td>{{$user['avatar']}}</td>
                   </tr>
                 <tr>
                   <td>Nom</td>
                   <td>{{$user['nom']}}</td>
                 </tr>
                 <tr>
                   <td>Username</td>
                   <td>{{$user['username']}}</td>
                 </tr>
                 <tr>
                   <td>Email</td>
                   <td>{{$user['email']}}</td>
                 </tr>
                 <tr>
                   <td>Role</td>
                   <td>{{$user['role']->designation}}</td>
                 </tr>

               </tbody>
             </table>
             </div>
             <!-- /.box-body -->

           </div>


@stop
