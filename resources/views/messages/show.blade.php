@extends('layouts.master')
@section('title', 'Messages')
@section('header')
@parent
@stop
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Messages
        <small>Affichage</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
        <li class="active"><a href="/messages">Messages</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <section class="col-lg-12">
            <div class="box flat">
                <div class="box-header">
                    <div class="col-lg-6 no-padding">
                        <h3 class="box-title">Affichage Message</h3>
                    </div>
                    <div class="col-lg-6 no-padding">
                        <button type="button" id="destroy-pdv" class="btn btn-danger pull-right"><i class="fa fa-close"></i> Supprimer</button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="mailbox-read-info">
                        <h3>{{ $message->objet }}</h3>
                        <h5>De: {{ $message->user_nom }} ({{ $message->user_email }})
                            <span class="mailbox-read-time pull-right">{{ $message->created_at }}</span></h5>
                        <h5>Vers:
                            @foreach($message->users as $key=>$user)
                                    @if($key< count($message->users)-1) {{ $user->nom }},
                                    @else {{ $user->nom }}.
                                    @endif
                            @endforeach
                           </h5>
                    </div>
                    <!-- /.mailbox-controls -->
                    <div class="mailbox-read-message">
                        <p class="text-justify">{{ $message->contenu }}</p>
                    </div>
                    <!-- /.mailbox-read-message -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
    </div>
</section>
@stop
@section('footer')
@parent
        <!-- Bootbox -->
{!! HTML::script('plugins/bootbox/bootbox.min.js') !!}

<script>
    $('#destroy-pdv').click(function(){
        bootbox.dialog({
            closeButton: false,
            message: "Voulez-vous supprimer ce message ?",
            title: "Faderco Information",
            buttons: {
                annuler: {
                    label: "Non !",
                    className: "btn-default",
                    callback: function() {

                    }
                },
                destroy: {
                    label: "Oui, je confirme la suppression.",
                    className: "btn-danger",
                    callback: function() {
                        $.ajax({
                            url: '/messages/{{ $message['id'] }}',
                            type: 'DELETE',
                            success: function(result) {
                                bootbox.dialog({
                                    closeButton: false,
                                    message: "Message supprimé avec succès",
                                    title: "Faderco Information",
                                    buttons: {
                                        success: {
                                            label: "Fermer!",
                                            className: "btn-success",
                                            callback: function() {
                                                location.href ='/messages';
                                            }
                                        }
                                    }
                                });
                            },
                            error : function(result) {
                                bootbox.dialog({
                                    closeButton: false,
                                    message: "Une erreur s'est produite, veuillez reessayer",
                                    title: "Faderco Information",
                                    buttons: {
                                        success: {
                                            label: "Fermer!",
                                            className: "btn-danger",
                                            callback: function() {
                                                location.href ='/messages';
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
    });
</script>
@stop