@extends('layouts.master')
@section('title', 'Messages')
@section('header')
@parent
    <!-- Select2 -->
{!! HTML::style('plugins/select2/select2.min.css') !!}
    <!-- bootstrap wysihtml5 - text editor -->
{{--{!! HTML::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}--}}
<style>
    [class^='select2'] {
        border-radius: 0px !important;
    }
    /*.select2-container .select2-selection {
        height : 35px;
        overflow-y: scroll;
    }*/
</style>
@stop
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Messages
        <small>Création</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
        <li class="active"><a href="/messages">Messages</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <section class="col-lg-12">
            <div class="box flat">
                <div class="box-header">
                    <h3 class="box-title">Créer un nouveau message</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{ url('/messages') }}" autocomplete="off" method="post" role="form">
                        <div class="col-lg-10 no-padding">
                            <div class="form-group {{ $errors->has('destinataires') ? ' has-error' : '' }}">
                                <select name="destinataires[]" id="users-select" class="form-control select2" multiple="multiple" style="width: 100%;">
                                    @foreach($users as $user)
                                        <option value="{{$user['id']}}">{{$user['nom']}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('destinataires'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('destinataires') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-2 no-padding">
                            <div class="form-group">
                                <button type="button" id="select-all" class="btn btn-primary pull-right flat"><i class="fa fa-bars"></i> Tout sélectionner</button>
                            </div>
                        </div>
                        <div class="col-lg-12 no-padding">
                            <div class="form-group {{ $errors->has('objet') ? ' has-error' : '' }}">
                                <input name="objet" class="form-control" placeholder="Sujet :">
                                @if ($errors->has('objet'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('objet') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-12 no-padding">
                            <div class="form-group {{ $errors->has('contenu') ? ' has-error' : '' }}">
                                <textarea name="contenu" class="form-control" placeholder="Votre message ici" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                @if ($errors->has('contenu'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contenu') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-lg-12 no-padding">
                            <div class="form-group">
                                <button type="submit" id="send-all" class="btn btn-primary pull-right flat"><i class="fa fa-send"></i> Envoyer le message</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
    </div>
</section>
@stop
@section('footer')
@parent
        <!-- Select2 -->
{!! HTML::script('plugins/select2/select2.full.min.js') !!}
        <!-- Bootbox -->
{!! HTML::script('plugins/bootbox/bootbox.min.js') !!}
        <!-- Bootstrap WYSIHTML5 -->
{{--{!! HTML::script('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}--}}
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2({
            placeholder: "Selectionner un/plusieurs destinataires",
            allowClear: true
        });
        $("#select-all").click(function(){
            $("#users-select > option").prop("selected","selected");
            $("#users-select").trigger("change");
        });

        //bootstrap WYSIHTML5 - text editor
        /*$(".textarea").wysihtml5();*/
    });
</script>
@stop