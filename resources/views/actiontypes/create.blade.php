@extends('layouts.master')
@section('title', 'Actions types')

@section('content_header')
<h3>Ajouter un type d'action</h3>
@stop

@section('content')
  <section class="content-header">
      <h1>
          Actions types
          <small>Create</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/marques">Actions types</a></li>
          <li class="active">Create</li>
      </ol>
  </section>
  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Action</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <!--<form role="form" method="post" action={{URL::to('/').'/marques'}} >-->
              {!! Form::open(array('url'=>'/actiontypes','method'=>'POST')) !!}
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input name="nom" class="form-control" id="" placeholder="nom de l'action" type="text">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>



                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
  </section>

@stop
