@extends('layouts.master')

@section('content_header')
<h3>Action</h3>
@stop

@section('content')


 <div class="box">

             <!-- /.box-header -->
             <div class="box-body">
               <table class="table table-bordered">
                 <tbody>
                 <tr>
                   <td>Nom</td>
                   <td>{{$actiontype['nom']}}</td>
                 </tr>
                 <tr>
                   <td>Mécanismes</td>
                   <td>@foreach($mechanisms as $mechanism)
                     {{$mechanism}}
                     <br>
                   @endforeach</td>
                 </tr>
               </tbody>
             </table>
             </div>
             <!-- /.box-body -->

           </div>


@stop
