@extends('layouts.master')
@section('title', 'Communes')

@section('content_header')
<h3>Ajouter une Communes</h3>
@stop

@section('content')

  <section class="content-header">
      <h1>
          Communes
          <small>Create</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/communes">Communes</a></li>
          <li class="active">Create</li>
      </ol>
  </section>
  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Communes</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action={{URL::to('/').'/communes'}}>
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input name="nom" class="form-control" id="" placeholder="nom de la commune" type="text">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Code</label>
                    <input name="code" class="form-control" id="" placeholder="code de la commune" type="text">
                    @if ($errors->has('code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('code') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('wilaya_id') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Wilaya</label><br>
                    <select name="wilaya_id">
                    @foreach($wilayas as $key => $value)
                      <option value="{{$value['id']}}">{{$value['nom']}}</option>
                    @endforeach
                    </select>
                    @if ($errors->has('wilaya_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('wilaya_id') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
  </section>          

@stop
