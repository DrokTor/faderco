@extends('layouts.master')

@section('content_header')
<h3>Produit</h3>
@stop

@section('content')


 <div class="box">

             <!-- /.box-header -->
             <div class="box-body">
               <table class="table table-bordered">
                 <tbody>
                   <tr>
                     <td>Photo</td>
                     <td>{{$product['logo']}}</td>
                   </tr>
                 <tr>
                   <td>Nom</td>
                   <td>{{$product['nom']}}</td>
                 </tr>
                 <tr>
                   <td>Marque</td>
                   <td>{{$product->famille_marque->marque_nom}}</td>
                 </tr>
                 <tr>
                   <td>Famille</td>
                   <td>{{$product->famille_marque->famille_nom}}</td>
                 </tr>
               </tbody>
             </table>
             </div>
             <!-- /.box-body -->

           </div>


@stop
