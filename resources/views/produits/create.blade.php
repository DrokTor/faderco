@extends('layouts.master')
@section('title', 'Produits')

@section('content_header')
<h3>Ajouter un produit</h3>
@stop

@section('content')

  <section class="content-header">
      <h1>
          Produits
          <small>Create</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/produits">Produits</a></li>
          <li class="active">Create</li>
      </ol>
  </section>
  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Produit</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action={{URL::to('/').'/produits'}}>
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input name="nom" class="form-control" id="" placeholder="nom du produit" type="text">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('up') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">UP</label>
                    <input name="up" class="form-control" id="" placeholder="up du produit" type="text">
                    @if ($errors->has('up'))
                        <span class="help-block">
                            <strong>{{ $errors->first('up') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('pc') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">PC</label>
                    <input name="pc" class="form-control" id="" placeholder="pc du produit" type="text">
                    @if ($errors->has('pc'))
                        <span class="help-block">
                            <strong>{{ $errors->first('pc') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('largeur') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Largeur</label>
                    <input name="largeur" class="form-control" id="" placeholder="Largeur du produit" type="text">
                    @if ($errors->has('largeur'))
                        <span class="help-block">
                            <strong>{{ $errors->first('largeur') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('longueur') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Longueur</label>
                    <input name="longueur" class="form-control" id="" placeholder="Longueur du produit" type="text">
                    @if ($errors->has('longueur'))
                        <span class="help-block">
                            <strong>{{ $errors->first('longueur') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('hauteur') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Hauteur</label>
                    <input name="hauteur" class="form-control" id="" placeholder="Hauteur du produit" type="text">
                    @if ($errors->has('hauteur'))
                        <span class="help-block">
                            <strong>{{ $errors->first('hauteur') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('marque') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Marque</label>
                    <select id="marques_select" name="marque">
                        <option></option>
                      @foreach($marques as $key => $value)
                        <option value="{{$value['id']}}">{{$value['nom']}}</option>
                      @endforeach

                    </select>
                    @if ($errors->has('marque'))
                        <span class="help-block">
                            <strong>{{ $errors->first('marque') }}</strong>
                        </span>
                    @endif
                   </div>
                  <div class="form-group {{ $errors->has('famille') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Famille</label>
                    <select id="familles_select" name="famille">


                    </select>
                    @if ($errors->has('famille'))
                        <span class="help-block">
                            <strong>{{ $errors->first('famille') }}</strong>
                        </span>
                    @endif
                   </div>


                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
</section>

@stop

@section('scripts')
@parent
<script>
$('#marques_select').on('change',function(event){
  var id= $(event.target).val(),path=window.location.pathname.split('create')[0]+"familles/"+id;
   $.ajax({
    url: path,
    type: 'GET',
    success: function(result) {
        // Do something with the result
        $('#familles_select').html(result);
    }
});
});


</script>
@stop
