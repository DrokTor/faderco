<!-- Logo -->
<a href="/" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>M</b>APP</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Merch</b>APP</span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            {{--<li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope-o"></i>
                    <span class="label label-success">2</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Vous avez 2 messages</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li><!-- start message -->
                                <a href="#">
                                    <div class="pull-left">
                                        <img src="/assets/img/avatar_default.png" class="img-circle" alt="User Image">
                                    </div>
                                    <h4>
                                        Message 1
                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                    </h4>
                                    <p>Le Lorem Ipsum est simplement du faux texte</p>
                                </a>
                            </li>
                            <!-- end message -->
                            <li>
                                <a href="#">
                                    <div class="pull-left">
                                        <img src="/assets/img/avatar_default.png" class="img-circle" alt="User Image">
                                    </div>
                                    <h4>
                                        Message 2
                                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                                    </h4>
                                    <p>Le Lorem Ipsum est simplement du faux texte</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="footer"><a href="#">Voir tous les messages</a></li>
                </ul>
            </li>--}}
            <!-- Notifications: style can be found in dropdown.less -->
            {{--<li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning">4</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Vous avez 4 notifications</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li>
                                <a href="#">
                                    <i class="fa fa-users text-aqua"></i> Notification 1
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-warning text-yellow"></i> Notification 2
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-users text-red"></i> Notification 3
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-shopping-cart text-green"></i> Notification 4
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="footer"><a href="#">Voir tout</a></li>
                </ul>
            </li>--}}
            <!-- Tasks: style can be found in dropdown.less -->
            {{--<li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-flag-o"></i>
                    <span class="label label-danger">1</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Vous avez 1 tache</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li><!-- Task item -->
                                <a href="#">
                                    <h3>
                                        Le Lorem Ipsum est simplement du faux texte
                                        <small class="pull-right">20%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <!-- end task item -->
                        </ul>
                    </li>
                    <li class="footer">
                        <a href="#">Voir toutes les tâches</a>
                    </li>
                </ul>
            </li>--}}
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="/{{ Auth::user()->avatar  }}" class="user-image" alt="User Image">
                    <span class="hidden-xs">{{ Auth::user()->username  }}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="/{{ Auth::user()->avatar  }}" class="img-circle" alt="User Image">

                        <p>
                            {{ Auth::user()->nom  }} - {{ Auth::user()->role->designation  }}
                            <small>Dernière connexion {{ Auth::user()->updated_at }}</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="users/{{ Auth::user()->id  }}/edit" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="/deconnexion" class="btn btn-default btn-flat">Déconnexion</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- Control Sidebar Toggle Button
            <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
            </li>-->
        </ul>
    </div>
</nav>
