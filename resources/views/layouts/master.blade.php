<!DOCTYPE html>
<html lang="fr">
<head>
    <title>@yield('title') - Faderco</title>
@section('header')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Application de Merchandising">
    <meta name="author" content="OverGen">
    <meta name="google-site-verification" content="_d8J9YHsOQZ8pxPUyQlepWOVIj4cazBRnBNzc6AZAnw" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- FAVICON -->
    <link rel="shortcut icon" href="{{ asset('faderco.ico') }}">
    <!-- Bootstrap 3.3.6 -->
    {!! HTML::style('bootstrap/css/bootstrap.min.css') !!}
    <!-- Font Awesome -->
    {!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css') !!}
    <!-- Ionicons -->
    {!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css') !!}
    <!-- Theme style -->
    {!! HTML::style('dist/css/AdminLTE.min.css') !!}
    {{--AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load.--}}
    {!! HTML::style('dist/css/skins/_all-skins.min.css') !!}
    <!-- iCheck -->
    {!! HTML::style('plugins/iCheck/flat/blue.css') !!}
    <!-- jvectormap -->
    {!! HTML::style('plugins/jvectormap/jquery-jvectormap-1.2.2.css') !!}
    <!-- Date Picker -->
    {!! HTML::style('plugins/datepicker/datepicker3.css') !!}
    <!-- Daterange picker -->
    {!! HTML::style('plugins/daterangepicker/daterangepicker-bs3.css') !!}
    <!-- bootstrap wysihtml5 - text editor -->
    {!! HTML::style('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
@show
</head>
<body class="skin-blue sidebar-mini wysihtml5-supported">
@if (!Request::is('connexion') && !Request::is('users/create') && !Request::is('rpms/*/pdf'))
<div class="wrapper">
    <header class="main-header">
    @include('layouts.header')
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
    @include('layouts.menu')
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div id="content-wrapper" class="content-wrapper">
    @include('flash::message')
    @yield('content')
    </div>
    <footer class="main-footer">
    @include('layouts.footer')
    </footer>
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        @include('layouts.sidebar')
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
@else
<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-sm-6 col-sm-offset-3  col-md-5 col-md-offset-4 col-lg-4 col-lg-offset-4">
            <div id="container_demo">
                <div id="wrapper">
                @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@section('footer')
<!-- jQuery 2.2.0 -->
{!! HTML::script('plugins/jQuery/jQuery-2.2.0.min.js') !!}
<!-- jQuery UI 1.11.4 -->
{!! HTML::script('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js') !!}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
{!! HTML::script('bootstrap/js/bootstrap.min.js') !!}
<!-- FastClick -->
{!! HTML::script('plugins/fastclick/fastclick.js') !!}
<!-- AdminLTE App -->
{!! HTML::script('dist/js/app.min.js') !!}
<!-- This is only necessary if you do Flash::overlay('...') -->
<script>
    $('#flash-overlay-modal').modal();
</script>
@show

</body>
</html>
