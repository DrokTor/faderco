<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="/{{ Auth::user()->avatar  }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>{{ Auth::user()->nom  }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> {{ Auth::user()->username  }}</a>
        </div>
    </div>
    <!-- search form -->
    <form action="/pdvs" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="search" class="form-control" placeholder="Recherche PDV...">
              <span class="input-group-btn">
                <button type="submit" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">Menu d'administration</li>
        @if(Request::is('/'))
            <li class="active treeview">
        @else
            <li class="treeview">
                @endif
                <a href="/">
                    <i class="fa fa-dashboard"></i> <span>Tableau de bord</span></i>
                </a>
            </li>
            @if(Request::is('messages') || Request::is('messages/*'))
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-envelope"></i> <span>Messages</span> <i
                                class="fa fa-angle-left pull-right"></i>
                        {{--<small class="label pull-right bg-yellow">12</small>--}}
                    </a>
                    <ul class="treeview-menu">
                        @if(Request::is('messages'))
                            <li class="active"><a href="/messages"><i class="fa fa-circle-o"></i> Liste des messages</a></li>
                            <li><a href="/messages/create"><i class="fa fa-circle-o"></i> Envoyer un message</a></li>
                        @else
                            <li><a href="/messages"><i class="fa fa-circle-o"></i> Liste des messages</a></li>
                            <li class="active"><a href="/messages/create"><i class="fa fa-circle-o"></i> Envoyer un message</a></li>
                        @endif
                    </ul>
                </li>
            @else
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-envelope"></i> <span>Messages</span> <i
                                class="fa fa-angle-left pull-right"></i>
                        {{--<small class="label pull-right bg-yellow">12</small>--}}
                    </a>
                    <ul class="treeview-menu">
                            <li><a href="/messages"><i class="fa fa-circle-o"></i> Liste des messages</a></li>
                            <li><a href="/messages/create"><i class="fa fa-circle-o"></i> Envoyer un message</a> </li>
                    </ul>
                </li>
            @endif
            @if(Request::is('users/*') || Request::is('users'))
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Utilisateurs</span> <i
                                class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(Request::is('users'))
                            <li class="active"><a href="/users"><i class="fa fa-circle-o"></i> Liste des utilisateurs</a></li>
                            <li><a href="/users/create"><i class="fa fa-circle-o"></i> Ajouter un utilisateur</a></li>
                        @else
                            <li><a href="/users"><i class="fa fa-circle-o"></i> Liste des utilisateurs</a></li>
                            <li class="active"><a href="/users/create"><i class="fa fa-circle-o"></i> Ajouter un utilisateur</a></li>
                        @endif
                    </ul>
                </li>
            @else
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Utilisateurs</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/users"><i class="fa fa-circle-o"></i> Liste des utilisateurs</a></li>
                        <li><a href="/users/create"><i class="fa fa-circle-o"></i> Ajouter un utilisateur</a></li>
                    </ul>
                </li>
            @endif

            @if(Request::is('pdvs/*') || Request::is('pdvs'))
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-files-o"></i> <span>Points de vente</span> <i
                                class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(Request::is('pdvs'))
                            <li class="active"><a href="/pdvs"><i class="fa fa-circle-o"></i> Liste des points de vente</a></li>
                        @else
                            <li><a href="/pdvs"><i class="fa fa-circle-o"></i> Liste des points de vente</a></li>
                        @endif
                        @if(Request::is('pdvs/pending'))
                            <li class="active"><a href="/pdvs/pending"><i class="fa fa-circle-o"></i> Points de vente en attente</a></li>
                        @else
                            <li><a href="/pdvs/pending"><i class="fa fa-circle-o"></i> Points de vente en attente</a></li>
                        @endif
                        @if(Request::is('pdvs/advanced'))
                            <li class="active"><a href="/pdvs/advanced"><i class="fa fa-circle-o"></i> Recherche avancée</a></li>
                        @else
                            <li><a href="/pdvs/advanced"><i class="fa fa-circle-o"></i> Recherche avancée</a></li>
                        @endif
                    </ul>
                </li>
            @else
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-files-o"></i> <span>Points de vente</span> <i
                                class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                            <li><a href="/pdvs"><i class="fa fa-circle-o"></i> Liste des points de vente</a></li>
                            <li><a href="/pdvs/pending"><i class="fa fa-circle-o"></i> Points de vente en attente</a></li>
                            <li><a href="/pdvs/advanced"><i class="fa fa-circle-o"></i> Recherche avancée</a></li>
                    </ul>
                </li>
            @endif

            @if(Request::is('rpms') || Request::is('rpms/*') || Request::is('rvcs') || Request::is('rvcs/*') || Request::is('rps') || Request::is('rps/*') || Request::is('rpcs') || Request::is('rpcs/*'))
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i><span>Rapports</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(Request::is('rpms') || Request::is('rpms/*'))
                            <li class="active"><a href="/rpms"><i class="fa fa-circle-o"></i> Rapports Merchandising</a></li>
                        @else
                            <li><a href="/rpms"><i class="fa fa-circle-o"></i> Rapports Merchandising</a></li>
                        @endif
                        @if(Request::is('rvcs') || Request::is('rvcs/*'))
                            <li class="active"><a href="/rvcs"><i class="fa fa-circle-o"></i> Rapports Veille Concurrentielle</a></li>
                        @else
                            <li><a href="/rvcs"><i class="fa fa-circle-o"></i> Rapports Veille Concurrentielle</a></li>
                        @endif
                        @if(Request::is('rps') || Request::is('rps/*'))
                            <li class="active"><a href="/rps"><i class="fa fa-circle-o"></i> Relevés des Prix</a></li>
                        @else
                            <li><a href="/rps"><i class="fa fa-circle-o"></i> Relevés des Prix</a></li>
                        @endif
                        @if(Request::is('rpcs') || Request::is('rpcs/*'))
                            <li class="active"><a href="/rpcs"><i class="fa fa-circle-o"></i> Relevés des Prix Concurrence</a></li>
                        @else
                            <li><a href="/rpcs"><i class="fa fa-circle-o"></i> Relevés des Prix concurrence</a></li>
                        @endif
                    </ul>
                </li>
            @else
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>Rapports</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="/rpms"><i class="fa fa-circle-o"></i> Rapports Merchandising</a></li>
                        <li><a href="/rvcs"><i class="fa fa-circle-o"></i> Rapports Veille Concurrentielle</a></li>
                        <li><a href="/rps"><i class="fa fa-circle-o"></i> Relevés des Prix</a></li>
                        <li><a href="/rpcs"><i class="fa fa-circle-o"></i> Relevés des Prix Concurrence</a></li>
                    </ul>
                </li>
            @endif

                <li class="header">Administration des données</li>
                @if(Request::is('regions') || Request::is('regions/*') || Request::is('wilayas') || Request::is('wilayas/*') || Request::is('communes') || Request::is('communes/*')
                    || Request::is('marques') || Request::is('marques/*') || Request::is('familles') || Request::is('familles/*') || Request::is('produits') || Request::is('produits/*')
                    || Request::is('actiontypes') || Request::is('actiontypes/*') || Request::is('mechanisms') || Request::is('mechanisms/*')
                    || Request::is('cannaux') || Request::is('cannaux/*') || Request::is('typespdvs') || Request::is('typespdvs/*') || Request::is('zones') || Request::is('zones/*')
                    || Request::is('plvs') || Request::is('plvs/*') || Request::is('surfaces') || Request::is('surfaces/*') || Request::is('freins') || Request::is('freins/*')
                    || Request::is('ruptures') || Request::is('ruptures/*'))
                <li class="treeview active">
                @else
                <li class="treeview">
                @endif
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Gestion des données</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        @if(Request::is('regions') || Request::is('regions/*') || Request::is('wilayas') || Request::is('wilayas/*') || Request::is('communes') || Request::is('communes/*'))
                        <li class="active">
                            <a href="#"><i class="fa fa-circle-o text-red"></i> Organisation territoriale <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu menu-open">
                        @else
                        <li >
                            <a href="#"><i class="fa fa-circle-o text-red"></i> Organisation territoriale <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="treeview-menu">
                        @endif
                                @if(Request::is('regions') || Request::is('regions/*'))
                                <li class="active">
                                    <a href="#"><i class="fa fa-circle-o"></i> Régions <i class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu menu-open">
                                        @if(Request::is('regions'))
                                        <li class="active"><a href="/regions/"><i class="fa fa-circle-o"></i> Liste des régions</a></li>
                                        <li><a href="/regions/create"><i class="fa fa-circle-o"></i> Ajouter une région</a></li>
                                        @else
                                        <li><a href="/regions/"><i class="fa fa-circle-o"></i> Liste des régions</a></li>
                                        <li class="active"><a href="/regions/create"><i class="fa fa-circle-o"></i> Ajouter une région</a></li>
                                        @endif
                                    </ul>
                                </li>
                                @else
                                <li>
                                    <a href="#"><i class="fa fa-circle-o"></i> Régions <i class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu">
                                        <li><a href="/regions/"><i class="fa fa-circle-o"></i> Liste des régions</a></li>
                                        <li><a href="/regions/create"><i class="fa fa-circle-o"></i> Ajouter une région</a></li>
                                    </ul>
                                </li>
                                @endif
                                @if(Request::is('wilayas') || Request::is('wilayas/*'))
                                <li class="active">
                                    <a href="#"><i class="fa fa-circle-o"></i> Wilayas <i class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu menu-open">
                                        @if(Request::is('wilayas'))
                                            <li class="active"><a href="/wilayas/"><i class="fa fa-circle-o"></i> Liste des wilayas</a>
                                            </li>
                                            <li><a href="/wilayas/create"><i class="fa fa-circle-o"></i> Ajouter une wilaya</a>
                                            </li>
                                        @else
                                            <li><a href="/wilayas/"><i class="fa fa-circle-o"></i> Liste des wilayas</a>
                                            </li>
                                            <li class="active"><a href="/wilayas/create"><i class="fa fa-circle-o"></i> Ajouter une wilaya</a>
                                            </li>
                                        @endif
                                    </ul>
                                </li>
                                @else
                                <li>
                                    <a href="#"><i class="fa fa-circle-o"></i> Wilayas <i
                                                class="fa fa-angle-left pull-right"></i></a>
                                    <ul class="treeview-menu">
                                        <li><a href="/wilayas/"><i class="fa fa-circle-o"></i> Liste des
                                                wilayas</a>
                                        </li>
                                        <li><a href="/wilayas/create"><i class="fa fa-circle-o"></i> Ajouter
                                                une
                                                wilaya</a>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                                @if(Request::is('communes') || Request::is('communes/*'))
                                    <li class="active">
                                        <a href="#"><i class="fa fa-circle-o"></i> Communes <i
                                                    class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu menu-open">
                                            @if(Request::is('communes'))
                                                <li class="active"><a href="/communes"><i class="fa fa-circle-o"></i> Liste des
                                                        communes</a>
                                                </li>
                                                <li><a href="/communes/create"><i class="fa fa-circle-o"></i>
                                                        Ajouter une
                                                        commune</a></li>
                                            @else
                                                <li><a href="/communes"><i class="fa fa-circle-o"></i> Liste des
                                                        communes</a>
                                                </li>
                                                <li class="active"><a href="/communes/create"><i class="fa fa-circle-o"></i>
                                                        Ajouter une
                                                        commune</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                @else
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Communes <i
                                                    class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/communes"><i class="fa fa-circle-o"></i> Liste des
                                                    communes</a>
                                            </li>
                                            <li><a href="/communes/create"><i class="fa fa-circle-o"></i>
                                                    Ajouter une
                                                    commune</a></li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </li>
                        @if(Request::is('marques') || Request::is('marques/*') || Request::is('familles') || Request::is('familles/*') || Request::is('produits') || Request::is('produits/*'))
                            <li class="active">
                                <a href="#"><i class="fa fa-circle-o text-blue"></i> Gamme de produits <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu menu-open">
                                    @if(Request::is('marques') || Request::is('marques/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> Marques <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu menu-open">
                                                @if(Request::is('marques'))
                                                    <li class="active"><a href="/marques"><i class="fa fa-circle-o"></i> Liste des marques</a>
                                                    </li>
                                                    <li><a href="/marques/create"><i class="fa fa-circle-o"></i> Ajouter une marque</a>
                                                    </li>
                                                @else
                                                    <li><a href="/marques"><i class="fa fa-circle-o"></i> Liste des marques</a>
                                                    </li>
                                                    <li class="active"><a href="/marques/create"><i class="fa fa-circle-o"></i> Ajouter une marque</a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Marques <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/marques"><i class="fa fa-circle-o"></i> Liste des
                                                        marques</a>
                                                </li>
                                                <li><a href="/marques/create"><i class="fa fa-circle-o"></i> Ajouter
                                                        une
                                                        marque</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Request::is('familles') || Request::is('familles/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> Familles <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu menu-open">
                                                @if(Request::is('familles'))
                                                    <li class="active"><a href="/familles"><i class="fa fa-circle-o"></i> Liste des familles</a>
                                                    </li>
                                                    <li ><a href="/familles/create"><i class="fa fa-circle-o"></i> Ajouter une famille</a></li>
                                                @else
                                                    <li ><a href="/familles"><i class="fa fa-circle-o"></i> Liste des familles</a>
                                                    </li>
                                                    <li class="active"><a href="/familles/create"><i class="fa fa-circle-o"></i> Ajouter une famille</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Familles <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/familles"><i class="fa fa-circle-o"></i> Liste des
                                                        familles</a>
                                                </li>
                                                <li><a href="/familles/create"><i class="fa fa-circle-o"></i>
                                                        Ajouter une
                                                        famille</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Request::is('produits') || Request::is('produits/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> Produits <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu menu-open">
                                                @if(Request::is('produits'))
                                                    <li class="active"><a href="/produits"><i class="fa fa-circle-o"></i> Liste des produits</a>
                                                    </li>
                                                    <li><a href="/produits/create"><i class="fa fa-circle-o"></i> Ajouter un produit</a>
                                                    </li>
                                                @else
                                                    <li><a href="/produits"><i class="fa fa-circle-o"></i> Liste des produits</a>
                                                    </li>
                                                    <li class="active"><a href="/produits/create"><i class="fa fa-circle-o"></i> Ajouter un produit</a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Produits <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/produits"><i class="fa fa-circle-o"></i> Liste des
                                                        produits</a>
                                                </li>
                                                <li><a href="/produits/create"><i class="fa fa-circle-o"></i>
                                                        Ajouter un
                                                        produit</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </li>
                        @else
                            <li>
                                <a href="#"><i class="fa fa-circle-o text-blue"></i> Gamme de produits <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Marques <i
                                                    class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/marques"><i class="fa fa-circle-o"></i> Liste des
                                                    marques</a>
                                            </li>
                                            <li><a href="/marques/create"><i class="fa fa-circle-o"></i> Ajouter
                                                    une
                                                    marque</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Familles <i
                                                    class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/familles"><i class="fa fa-circle-o"></i> Liste des
                                                    familles</a>
                                            </li>
                                            <li><a href="/familles/create"><i class="fa fa-circle-o"></i>
                                                    Ajouter une
                                                    famille</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Produits <i
                                                    class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/produits"><i class="fa fa-circle-o"></i> Liste des
                                                    produits</a>
                                            </li>
                                            <li><a href="/produits/create"><i class="fa fa-circle-o"></i>
                                                    Ajouter un
                                                    produit</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        @if(Request::is('cannaux') || Request::is('cannaux/*') || Request::is('typespdvs') || Request::is('typespdvs/*') || Request::is('zones') || Request::is('zones/*')
                            || Request::is('plvs') || Request::is('plvs/*') || Request::is('surfaces') || Request::is('surfaces/*') )
                            <li class="active">
                                <a href="#"><i class="fa fa-circle-o text-green"></i> Options PDV <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu menu-open">
                                    @if(Request::is('cannaux') || Request::is('cannaux/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> Cannaux d'achats <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                @if(Request::is('cannaux'))
                                                    <li class="active"><a href="/cannaux"><i class="fa fa-circle-o"></i> Liste des cannaux</a></li>
                                                    <li><a href="/cannaux/create"><i class="fa fa-circle-o"></i> Ajouter une canal</a></li>
                                                @else
                                                    <li><a href="/cannaux"><i class="fa fa-circle-o"></i> Liste des cannaux</a></li>
                                                    <li class="active"><a href="/cannaux/create"><i class="fa fa-circle-o"></i> Ajouter une canal</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Cannaux d'achats <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/cannaux"><i class="fa fa-circle-o"></i> Liste des cannaux</a></li>
                                                <li><a href="/cannaux/create"><i class="fa fa-circle-o"></i> Ajouter une canal</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Request::is('typespdvs') || Request::is('typespdvs/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> Types PDV <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu menu-open">
                                                @if(Request::is('typespdvs'))
                                                    <li class="active"><a href="/typespdvs"><i class="fa fa-circle-o"></i> Liste des types PDV</a></li>
                                                    <li><a href="/typespdvs/create"><i class="fa fa-circle-o"></i> Ajouter un type PDV</a></li>
                                                @else
                                                    <li><a href="/typespdvs"><i class="fa fa-circle-o"></i> Liste des types PDV</a></li>
                                                    <li class="active"><a href="/typespdvs/create"><i class="fa fa-circle-o"></i> Ajouter un type PDV</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Types PDV <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/typespdvs"><i class="fa fa-circle-o"></i> Liste des types PDV</a></li>
                                                <li><a href="/typespdvs/create"><i class="fa fa-circle-o"></i> Ajouter un type PDV</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Request::is('zones') || Request::is('zones/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> Zones chalandises <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu menu-open">
                                                @if(Request::is('zones'))
                                                    <li class="active"><a href="/zones"><i class="fa fa-circle-o"></i> Liste des zones</a></li>
                                                    <li><a href="/zones/create"><i class="fa fa-circle-o"></i> Ajouter une zone</a></li>
                                                @else
                                                    <li><a href="/zones"><i class="fa fa-circle-o"></i> Liste des zones</a></li>
                                                    <li class="active"><a href="/zones/create"><i class="fa fa-circle-o"></i> Ajouter une zone</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Zones chalandises <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/zones"><i class="fa fa-circle-o"></i> Liste des zones</a></li>
                                                <li><a href="/zones/create"><i class="fa fa-circle-o"></i> Ajouter une zone</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Request::is('plvs') || Request::is('plvs/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> PLVs stores <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu menu-open">
                                                @if(Request::is('plvs'))
                                                    <li class="active"><a href="/plvs"><i class="fa fa-circle-o"></i> Liste des zones</a></li>
                                                    <li><a href="/plvs/create"><i class="fa fa-circle-o"></i> Ajouter un PLV</a></li>
                                                @else
                                                    <li><a href="/plvs"><i class="fa fa-circle-o"></i> Liste des zones</a></li>
                                                    <li class="active"><a href="/plvs/create"><i class="fa fa-circle-o"></i> Ajouter un PLV</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> PLVs stores <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/plvs"><i class="fa fa-circle-o"></i> Liste des zones</a></li>
                                                <li><a href="/plvs/create"><i class="fa fa-circle-o"></i> Ajouter un PLV</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Request::is('surfaces') || Request::is('surfaces/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> Surfaces PDV <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu menu-open">
                                                @if(Request::is('surfaces'))
                                                    <li class="active"><a href="/surfaces"><i class="fa fa-circle-o"></i> Liste des surfaces</a></li>
                                                    <li><a href="/surfaces/create"><i class="fa fa-circle-o"></i> Ajouter une surface</a></li>
                                                @else
                                                    <li><a href="/surfaces"><i class="fa fa-circle-o"></i> Liste des surfaces</a></li>
                                                    <li class="active"><a href="/surfaces/create"><i class="fa fa-circle-o"></i> Ajouter une surface</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Surfaces PDV <i class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/surfaces"><i class="fa fa-circle-o"></i> Liste des surfaces</a></li>
                                                <li><a href="/surfaces/create"><i class="fa fa-circle-o"></i> Ajouter une surface</a></li>
                                            </ul>
                                        </li>
                                    @endif

                                </ul>
                            </li>
                        @else
                            <li>
                                <a href="#"><i class="fa fa-circle-o text-green"></i> Options PDV <i class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Cannaux d'achats <i class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/cannaux"><i class="fa fa-circle-o"></i> Liste des cannaux</a></li>
                                            <li><a href="/cannaux/create"><i class="fa fa-circle-o"></i> Ajouter une canal</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Types PDV <i class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/typespdvs"><i class="fa fa-circle-o"></i> Liste des types PDV</a></li>
                                            <li><a href="/typespdvs/create"><i class="fa fa-circle-o"></i> Ajouter un type PDV</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Zones chalandises <i class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/zones"><i class="fa fa-circle-o"></i> Liste des zones</a></li>
                                            <li><a href="/zones/create"><i class="fa fa-circle-o"></i> Ajouter une zone</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> PLVs stores <i class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/plvs"><i class="fa fa-circle-o"></i> Liste des zones</a></li>
                                            <li><a href="/plvs/create"><i class="fa fa-circle-o"></i> Ajouter un PLV</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Surfaces PDV <i class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/surfaces"><i class="fa fa-circle-o"></i> Liste des surfaces</a></li>
                                            <li><a href="/surfaces/create"><i class="fa fa-circle-o"></i> Ajouter une surface</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>
                        @endif

                        @if(Request::is('freins') || Request::is('freins/*') || Request::is('ruptures') || Request::is('ruptures/*') )
                            <li class="active">
                                <a href="#"><i class="fa fa-circle-o text-orange"></i> Options RPM <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu menu-open">
                                  @if(Request::is('freins') || Request::is('freins/*'))
                                          <li class="active">
                                              <a href="#"><i class="fa fa-circle-o"></i> Freins d'éxecution <i class="fa fa-angle-left pull-right"></i></a>
                                              <ul class="treeview-menu menu-open">
                                                  @if(Request::is('freins'))
                                                      <li class="active"><a href="/freins"><i class="fa fa-circle-o"></i> Liste des freins</a></li>
                                                      <li><a href="/freins/create"><i class="fa fa-circle-o"></i> Ajouter un frein</a></li>
                                                  @else
                                                      <li><a href="/freins"><i class="fa fa-circle-o"></i> Liste des freins</a></li>
                                                      <li class="active"><a href="/freins/create"><i class="fa fa-circle-o"></i> Ajouter un frein</a></li>
                                                  @endif
                                              </ul>
                                          </li>
                                  @else
                                          <li>
                                              <a href="#"><i class="fa fa-circle-o"></i> Freins d'éxecution <i class="fa fa-angle-left pull-right"></i></a>
                                              <ul class="treeview-menu">
                                                  <li><a href="/freins"><i class="fa fa-circle-o"></i> Liste des freins</a></li>
                                                  <li><a href="/freins/create"><i class="fa fa-circle-o"></i> Ajouter un frein</a></li>
                                              </ul>
                                          </li>
                                  @endif
                                  @if(Request::is('ruptures') || Request::is('ruptures/*'))
                                          <li class="active">
                                              <a href="#"><i class="fa fa-circle-o"></i> Raisons de rupture <i class="fa fa-angle-left pull-right"></i></a>
                                              <ul class="treeview-menu menu-open">
                                                  @if(Request::is('ruptures'))
                                                      <li class="active"><a href="/ruptures"><i class="fa fa-circle-o"></i> Liste des raisons</a></li>
                                                      <li><a href="/ruptures/create"><i class="fa fa-circle-o"></i> Ajouter une raison</a></li>
                                                  @else
                                                      <li><a href="/ruptures"><i class="fa fa-circle-o"></i> Liste des raisons</a></li>
                                                      <li class="active"><a href="/ruptures/create"><i class="fa fa-circle-o"></i> Ajouter une raison</a></li>
                                                  @endif
                                              </ul>
                                          </li>
                                  @else
                                          <li>
                                              <a href="#"><i class="fa fa-circle-o"></i> Raisons de rupture <i
                                                          class="fa fa-angle-left pull-right"></i></a>
                                              <ul class="treeview-menu">
                                                  <li><a href="/ruptures"><i class="fa fa-circle-o"></i> Liste des raisons</a></li>
                                                  <li><a href="/ruptures/create"><i class="fa fa-circle-o"></i> Ajouter une raison</a></li>
                                              </ul>
                                          </li>
                                  @endif


                                </ul>
                            </li>
                        @else
                            <li>
                                <a href="#"><i class="fa fa-circle-o text-orange"></i> Options RPM <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">
                                  <li>
                                      <a href="#"><i class="fa fa-circle-o"></i> Freins d'éxecution <i class="fa fa-angle-left pull-right"></i></a>
                                      <ul class="treeview-menu">
                                          <li><a href="/freins"><i class="fa fa-circle-o"></i> Liste des freins</a></li>
                                          <li><a href="/freins/create"><i class="fa fa-circle-o"></i> Ajouter un frein</a></li>
                                      </ul>
                                  </li>
                                  <li>
                                      <a href="#"><i class="fa fa-circle-o"></i> Raisons de rupture <i class="fa fa-angle-left pull-right"></i></a>
                                      <ul class="treeview-menu">
                                          <li><a href="/ruptures"><i class="fa fa-circle-o"></i> Liste des raisons</a></li>
                                          <li><a href="/ruptures/create"><i class="fa fa-circle-o"></i> Ajouter une raison</a></li>
                                      </ul>
                                  </li>



                                </ul>
                            </li>
                        @endif

                        @if(Request::is('mechanisms') || Request::is('mechanisms/*') || Request::is('actiontypes') || Request::is('actiontypes/*') )
                            <li class="active">
                                <a href="#"><i class="fa fa-circle-o text-purple"></i> Options RVC <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu menu-open">

                                    @if(Request::is('actiontypes') || Request::is('actiontypes/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> Actions types <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu menu-open">
                                                @if(Request::is('actiontypes'))
                                                    <li class="active"><a href="/familles"><i class="fa fa-circle-o"></i> Liste des actions</a>
                                                    </li>
                                                    <li ><a href="/actiontypes/create"><i class="fa fa-circle-o"></i> Ajouter une action</a></li>
                                                @else
                                                    <li ><a href="/actiontypes"><i class="fa fa-circle-o"></i> Liste des actions</a>
                                                    </li>
                                                    <li class="active"><a href="/actiontypes/create"><i class="fa fa-circle-o"></i> Ajouter une action</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Actions types <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/actiontypes"><i class="fa fa-circle-o"></i> Liste des
                                                        actions</a>
                                                </li>
                                                <li><a href="/actiontypes/create"><i class="fa fa-circle-o"></i>
                                                        Ajouter une
                                                        action</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                    @if(Request::is('mechanisms') || Request::is('mechanisms/*'))
                                        <li class="active">
                                            <a href="#"><i class="fa fa-circle-o"></i> Mécanismes <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu menu-open">
                                                @if(Request::is('mechanisms'))
                                                    <li class="active"><a href="/mechanisms"><i class="fa fa-circle-o"></i> Liste des mécanismes</a>
                                                    </li>
                                                    <li><a href="/mechanisms/create"><i class="fa fa-circle-o"></i> Ajouter un mécanisme</a>
                                                    </li>
                                                @else
                                                    <li><a href="/mechanisms"><i class="fa fa-circle-o"></i> Liste des mécanismes</a>
                                                    </li>
                                                    <li class="active"><a href="/mechanisms/create"><i class="fa fa-circle-o"></i> Ajouter un mécanisme</a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="#"><i class="fa fa-circle-o"></i> Mécanismes <i
                                                        class="fa fa-angle-left pull-right"></i></a>
                                            <ul class="treeview-menu">
                                                <li><a href="/mechanisms"><i class="fa fa-circle-o"></i> Liste des
                                                        Mécanismes</a>
                                                </li>
                                                <li><a href="/mechanisms/create"><i class="fa fa-circle-o"></i> Ajouter
                                                        un
                                                        Mécanisme</a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif

                                </ul>
                            </li>
                        @else
                            <li>
                                <a href="#"><i class="fa fa-circle-o text-purple"></i> Options RVC <i
                                            class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">

                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Actions types <i
                                                    class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/actiontypes"><i class="fa fa-circle-o"></i> Liste des
                                                    actions</a>
                                            </li>
                                            <li><a href="/actiontypes/create"><i class="fa fa-circle-o"></i>
                                                    Ajouter une
                                                    action</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fa fa-circle-o"></i> Mécanismes <i
                                                    class="fa fa-angle-left pull-right"></i></a>
                                        <ul class="treeview-menu">
                                            <li><a href="/mechanisms"><i class="fa fa-circle-o"></i> Liste des
                                                    mécanismes</a>
                                            </li>
                                            <li><a href="/mechanisms/create"><i class="fa fa-circle-o"></i> Ajouter
                                                    un
                                                    mécanisme</a>
                                            </li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>
                        @endif
                    </ul>
                </ul>
    </ul>
</section>
<!-- /.sidebar -->
