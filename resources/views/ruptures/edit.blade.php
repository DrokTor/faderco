@extends('layouts.master')
@section('title', 'Rupture')

@section('content_header')
<h3>Modifier une Rupture</h3>
@stop
@section('content')

  <section class="content-header">
      <h1>
          Raison de rupture
          <small>Edit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/ruptures">Raison de rupture</a></li>
          <li class="active">Edit</li>
      </ol>
  </section>
  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 id="productID" class="box-title" data-id="{{$rupture['id']}}">Rupture</h3>
               </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action={{URL::to('/').'/ruptures/'.$rupture['id']}}>
                  {{method_field('PATCH')}}
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input value="{{$rupture['nom']}}" name="nom" class="form-control" id="" placeholder="nom de la rupture" type="text">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
</section>

@stop
