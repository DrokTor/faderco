@extends('layouts.master')
@section('title', 'Points de vente')
@section('header')
@parent
        <!-- DataTables -->
{!! HTML::style('plugins/datatables/dataTables.bootstrap.css') !!}
        <!-- Select2 -->
{!! HTML::style('plugins/select2/select2.min.css') !!}
<style>
    [class^='select2'] {
        border-radius: 0px !important;
    }
    #toolbar {
        float:right;
    }
</style>
@stop
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Points de vente
        <small>Liste</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
        <li class="active"><a href="/pdvs">Points de vente</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <section class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Liste des points de vente</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form id="advanced-form">
                        <div class="col-lg-12 no-padding">
                            <div class="col-lg-4 no-padding">
                                <div class="form-group">
                                    <label for="nom-pdv">Nom Point de vente</label>
                                    <input class="form-control" id="nom-pdv" placeholder="Entrer le nom du point de vente"
                                           type="text" style="height: 28px;">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Types Point de vente</label>
                                    <select id="type-pdv" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
                                        <option value="0" selected>Selectionner une Type</option>
                                        @foreach($types as $type)
                                            <option value="{{$type['id']}}">{{$type['nom']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 no-padding">
                                <label>Canaux d'achats</label>
                                <select id="canal-pdv" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
                                    <option value="0" selected>Selectionner une Canal</option>
                                    @foreach($canaux as $canal)
                                        <option value="{{$canal['id']}}">{{$canal['nom']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 no-padding">
                            <div class="col-lg-4 no-padding">
                                <div class="form-group">
                                    <label>Régions</label>
                                    <select id="region-pdv" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
                                        <option value="0" selected>Selectionner une Région</option>
                                        @foreach($regions as $region)
                                            <option value="{{$region['id']}}">{{$region['nom']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Wilayas</label>
                                    <select disabled id="wilaya-pdv" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
                                        <option value="0" selected>Selectionner une Wilaya</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 no-padding">
                                <div class="form-group">
                                    <label>Communes</label>
                                    <select disabled id="commune-pdv" class="form-control select2 select2-hidden-accessible" style="width: 100%;">
                                        <option value="0" selected>Selectionner une Commune</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="div-dataTables" class="box-body">
                    {{--generated by jquery--}}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
    </div>
</section>
@stop
@section('footer')
@parent
        <!-- DataTables -->
{!! HTML::script('plugins/datatables/jquery.dataTables.min.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.min.js') !!}
        <!-- Select2 -->
{!! HTML::script('plugins/select2/select2.full.min.js') !!}
        <!-- Bootbox -->
{!! HTML::script('plugins/bootbox/bootbox.min.js') !!}

<script>
    $(function () {
        var selected = -1;

        //Initialize Select2 Elements
        $(".select2").select2();

        //datatables
        $('#div-dataTables').html('<table id="dataTables" class="table table-bordered table-hover table-striped" style="width:100%"></table>');
        $('#dataTables').dataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.11/i18n/French.json"
            },
            "bProcessing": true,
            "bServerSide": true,
            "stateSave": true,
            "sDom": '<"row"<"col-sm-6"l><"col-sm-6"<"#toolbar.dataTables_filter">>><"row"<"col-sm-12"t>><"row"<"col-sm-5"i><"col-sm-7"p>>r',
            "aoColumns": [
                {"sTitle": "Id"},
                {"sTitle": "Nom"},
                {"sTitle": "Utilisateur"},
                {"sTitle": "Région"},
                {"sTitle": "Wilaya"},
                {"sTitle": "Commune"},
                {"sTitle": "Canal"},
                {"sTitle": "Type"},
                {"sTitle": "Date de création"},
                {"sTitle": "Etat"}
            ],
            "columnDefs": [
                {visible: false, targets: [0]},
                {
                    "render": function (data, type, row) {
                        if (data == '1')
                            return '<span class="label label-success center-block"><i class="fa fa-check"></i> Validé</span>';

                        return '<span class="label label-warning center-block"><i class="fa fa-clock-o"></i> En attente</span>';
                    },
                    "targets": 9
                }
            ],
            "order": [[8, "desc"]],
            "iDisplayLength": 25,
            "sAjaxSource": "/pdvs/advanced",
            "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
                oSettings.jqXHR = $.ajax({
                    "dataType": 'json',
                    "type": "GET",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback,
                    "error": handleAjaxError
                });
            },
            "rowCallback": function (row, data) {
                if (data.DT_RowId == selected) {
                    $(row).addClass('selected');
                }
            }
            /*"initComplete": function(settings, json) {
                $('#toolbar').html('<button id="search-btn" type="button" class="btn btn-primary" onclick="advancedSearch();"><i class="fa fa-search"></i> Rechercher</button>');
            }*/
        });

        //click on datatables
        $('#dataTables tbody').on('click', 'tr', function () {
            if(this.id != "") {
                var id = this.id;
                var pid = id.substring(4);

                $('#' + selected).toggleClass('selected');

                if (selected != id) {

                    selected = id;
                    $(this).toggleClass('selected');
                }
                else selected = -1;

                var selected_tr = $(this);
                bootbox.dialog({
                    closeButton: false,
                    message: "Veuillez choisir une action",
                    title: "Faderco Information",
                    buttons: {
                        annuler: {
                            label: "Retour",
                            className: "btn-default",
                            callback: function () {
                                selected_tr.toggleClass('selected');
                                selected = -1;
                            }
                        },
                        destroy: {
                            label: "Supprimer le point de vente",
                            className: "btn-danger",
                            callback: function () {
                                $.ajax({
                                    url: '/pdvs/' + pid,
                                    type: 'DELETE',
                                    success: function (result) {
                                        bootbox.dialog({
                                            closeButton: false,
                                            message: "Point de vente supprimé avec succès",
                                            title: "Faderco Information",
                                            buttons: {
                                                success: {
                                                    label: "Fermer!",
                                                    className: "btn-success",
                                                    callback: function () {
                                                        location.href = '/pdvs';
                                                    }
                                                }
                                            }
                                        });
                                    },
                                    error: function (result) {
                                        bootbox.dialog({
                                            closeButton: false,
                                            message: "Une erreur s'est produite, veuillez reessayer",
                                            title: "Faderco Information",
                                            buttons: {
                                                success: {
                                                    label: "Fermer!",
                                                    className: "btn-danger",
                                                    callback: function () {
                                                        location.href = '/pdvs';
                                                    }
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        },
                        show: {
                            label: "Afficher le point de vente",
                            className: "btn-primary",
                            callback: function () {
                                location.href = '/pdvs/' + pid;
                            }
                        }
                    }
                });
            }
        });

        //stateSave
        stateTable = $('#dataTables').DataTable();

        if(stateTable.column(1).search() !="") {
            $('#nom-pdv').val(stateTable.column(1).search());
        }

        if(stateTable.column(3).search() !="") {
            $('#region-pdv').val(stateTable.column(3).search()).attr('selected','selected');
            $('#select2-region-pdv-container').attr('title',$('#region-pdv').find(":selected").text()).text($('#region-pdv').find(":selected").text());

            if(stateTable.column(4).search() == "") getWilayas(stateTable.column(3).search());
        }

        if(stateTable.column(4).search() !="") {
            $.when(getWilayas(stateTable.column(3).search()).done(function() {
                $('#wilaya-pdv').val(stateTable.column(4).search()).attr('selected','selected');
                $('#select2-wilaya-pdv-container').attr('title',$('#wilaya-pdv').find(":selected").text()).text($('#wilaya-pdv').find(":selected").text());

                if(stateTable.column(5).search() == "") getCommunes(stateTable.column(4).search());
            }));
        }

        if(stateTable.column(5).search() !="") {
            $.when(getCommunes(stateTable.column(4).search()).done(function() {
                $('#commune-pdv').val(stateTable.column(5).search()).attr('selected','selected');
                $('#select2-commune-pdv-container').attr('title',$('#commune-pdv').find(":selected").text()).text($('#commune-pdv').find(":selected").text());
            }));
        }

        if(stateTable.column(6).search() !="") {
            $('#canal-pdv').val(stateTable.column(6).search()).attr('selected','selected');
            $('#select2-canal-pdv-container').attr('title',$('#canal-pdv').find(":selected").text()).text($('#canal-pdv').find(":selected").text());
        }

        if(stateTable.column(7).search() !="") {
            $('#type-pdv').val(stateTable.column(7).search()).attr('selected','selected');
            $('#select2-type-pdv-container').attr('title',$('#type-pdv').find(":selected").text()).text($('#type-pdv').find(":selected").text());
        }

        //advanced filter
        $('#nom-pdv').on('change paste keyup', function() {
            inputvalue = $('#nom-pdv').val();
            if(inputvalue.length > 2) {
                $('#dataTables').dataTable().fnFilter(inputvalue, 1);
            }
            else $('#dataTables').dataTable().fnFilter("", 1);
        });

        $('#region-pdv').on('change keyup', function() {
            inputvalue = $('#region-pdv').val();
            if(inputvalue != 0) {
                $('#dataTables').dataTable().fnFilter(inputvalue, 3);

                getWilayas(inputvalue);
            }
            else {
                $('#dataTables').dataTable().fnFilter("", 3);
                $('#wilaya-pdv').prop('disabled', 'disabled');
                $('#wilaya-pdv').empty();
                $('#wilaya-pdv').append($('<option>').text('Selectionner une Wilaya').val(0));
                $('#select2-wilaya-pdv-container').attr('title','Selectionner une Wilaya').text('Selectionner une Wilaya');
                $('#dataTables').dataTable().fnFilter("", 4);
                $('#commune-pdv').prop('disabled', 'disabled');
                $('#commune-pdv').empty();
                $('#commune-pdv').append($('<option>').text('Selectionner une Commune').val(0));
                $('#select2-commune-pdv-container').attr('title','Selectionner une Commune').text('Selectionner une Commune');
                $('#dataTables').dataTable().fnFilter("", 5);
            }
        });

        $('#wilaya-pdv').on('change keyup', function() {
            inputvalue = $('#wilaya-pdv').val();
            if(inputvalue != 0) {
                $('#dataTables').dataTable().fnFilter(inputvalue, 4);

                getCommunes(inputvalue);
            }
            else {
                $('#dataTables').dataTable().fnFilter("", 4);
                $('#commune-pdv').prop('disabled', 'disabled');
                $('#commune-pdv').empty();
                $('#commune-pdv').append($('<option>').text('Selectionner une Commune').val(0));
                $('#select2-commune-pdv-container').attr('title','Selectionner une Commune').text('Selectionner une Commune');
                $('#dataTables').dataTable().fnFilter("", 5);
            }
        });

        $('#commune-pdv').on('change keyup', function() {
            inputvalue = $('#commune-pdv').val();
            if(inputvalue != 0) {
                $('#dataTables').dataTable().fnFilter(inputvalue, 5);
            }
            else $('#dataTables').dataTable().fnFilter("", 5);
        });

        $('#canal-pdv').on('change keyup', function() {
            inputvalue = $('#canal-pdv').val();
            if(inputvalue != 0) {
                $('#dataTables').dataTable().fnFilter(inputvalue, 6);
            }
            else $('#dataTables').dataTable().fnFilter("", 6);
        });

        $('#type-pdv').on('change keyup', function() {
            inputvalue = $('#type-pdv').val();
            if(inputvalue != 0) {
                $('#dataTables').dataTable().fnFilter(inputvalue, 7);
            }
            else $('#dataTables').dataTable().fnFilter("", 7);
        });
    });

    $(document).ajaxComplete(function() {

    });

    function handleAjaxError(xhr) {
        bootbox.alert({
            message: xhr.responseText,
            title: 'Faderco Information',
            callback: function () {
                location.href = '/';
            }
        });
    }

    function getWilayas(region_id) {
        $('#wilaya-pdv').prop('disabled', false);
        $('#wilaya-pdv').empty();
        $('#wilaya-pdv').append($('<option>').text('Selectionner une Wilaya').attr('value', '0'));

        return $.getJSON('/ajax/regions/'+region_id, function(json){
            $.each(json, function(i, obj){
                $('#wilaya-pdv').append($('<option>').text(obj.text).attr('value', obj.value));
            });
        });
    }

    function getCommunes(wilaya_id) {
        $('#commune-pdv').prop('disabled', false);
        $('#commune-pdv').empty();
        $('#commune-pdv').append($('<option>').text('Selectionner une Commune').attr('value', '0'));

        return $.getJSON('/ajax/wilayas/'+wilaya_id, function(json){
            $.each(json, function(i, obj){
                $('#commune-pdv').append($('<option>').text(obj.text).attr('value', obj.value));
            });
        });
    }
</script>
@stop