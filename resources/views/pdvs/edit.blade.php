@extends('layouts.master')
@section('title', 'Points de vente')
@section('header')
    @parent
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Point de vente
            <small>Modifier</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active"><a href="/pdvs">Points de vente</a></li>
        </ol>
    </section>

  <form role="form" method="post" action={{URL::to('/').'/pdvs/'.$data['id']}} >
      {{method_field('PATCH')}}
      <input name="webupdate" value="true" hidden/>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <section class="col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <div class="col-lg-6 no-padding">
                            <h3 class="box-title">Fiche Point de vente</h3>
                        </div>
                        <div class="col-lg-6 no-padding">
                            <button type="submit" id="validate-pdv" class="btn btn-success pull-right" style="margin-right: 5px;" ><i class="fa fa-check"></i> Valider</button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- START ACCORDION -->
                        <div class="box-group" id="accordion">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Information Point de vente
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-4">
                                            <ul class="list-group list-group-unbordered">
                                                <li class="list-group-item">
                                                    <b>Nom</b> <input name="nom" type="text" class="pull-right" value="{{ $data['nom'] }}" />
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Région</b>
                                                    <select id="regionsSelect" name="region_id" class="pull-right" >
                                                      @foreach( $regions as $opt)
                                                        <option value="{{ $opt['id'] }}" @if( $opt['id']==$data['region']['id']) selected='selected' @endif >{{$opt['nom']}}</option>
                                                      @endforeach
                                                    </select>
                                                  </li>
                                                <li class="list-group-item">
                                                    <b>Wilaya</b>
                                                    <select id="wilayasSelect" class="pull-right" name="wilaya_id">
                                                      @foreach( $wilayas as $opt)
                                                        <option value="{{ $opt['id'] }}" @if( $opt['id']==$data['wilaya']['id']) selected='selected' @endif >{{$opt['nom']}}</option>
                                                      @endforeach
                                                    </select>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Commune</b>
                                                    <select id="communesSelect" class="pull-right" name="commune_id">
                                                      @foreach( $communes as $opt)
                                                        <option value="{{ $opt['id'] }}" @if( $opt['id']==$data['commune']['id']) selected='selected' @endif >{{$opt['nom']}}</option>
                                                      @endforeach
                                                    </select>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Canal d'achat</b>
                                                    <select name="canal_achat_id" class="pull-right" >
                                                      @foreach( $cannaux as $opt)
                                                        <option value="{{ $opt['id'] }}" @if( $opt['id']==$data['canal_achat']['id']) selected='selected' @endif >{{$opt['nom']}}</option>
                                                      @endforeach
                                                    </select>
                                                  </li>
                                                <li class="list-group-item">
                                                    <b>Type</b>
                                                    <select name="type_pdv_id" class="pull-right" >
                                                      @foreach( $PdvType as $opt)
                                                        <option value="{{ $opt['id'] }}" @if( $opt['id']==$data['type_pdv']['id']) selected='selected' @endif >{{$opt['nom']}}</option>
                                                      @endforeach
                                                    </select>
                                                  </li>
                                                <li class="list-group-item">
                                                    <b>Surface</b>
                                                    <select name="surface_pdv_id" class="pull-right" >
                                                      @foreach( $PdvSurface as $opt)
                                                        <option value="{{ $opt['id'] }}" @if( $opt['id']==$data['surface_pdv']['id']) selected='selected' @endif >{{$opt['nom']}}</option>
                                                      @endforeach
                                                    </select>
                                                  </li>
                                                <li class="list-group-item">
                                                    <b>Zone chalandise</b>
                                                    <select name="zone_chalandise_id" class="pull-right" >
                                                      @foreach( $ZoneChalandises as $opt)
                                                        <option value="{{ $opt['id'] }}" @if( $opt['id']==$data['zone_chalandise']['id']) selected='selected' @endif >{{$opt['nom']}}</option>
                                                      @endforeach
                                                    </select>
                                                  </li>
                                                <li class="list-group-item">
                                                    <b>Sortie de caisse</b> <input name="nb_sortie_caisse" type="text" class="pull-right" value="{{ $data['nb_sortie_caisse'] }}" />
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-5">
                                            {!! $map !!}
                                        </div>
                                        <div class="col-lg-3">
                                            <img class="profile-user-img img-responsive img-circle" src="/{{ $data['user']['avatar'] }}" alt="User profile picture">
                                            <h3 class="profile-username text-center">{{ $data['user']['nom'] }}</h3>
                                            <p class="text-muted text-center">Nom d'utilisateur : {{ $data['user']['username'] }}</p>
                                            <p class="text-muted text-center">Téléphone : {{ $data['user']['telephone'] }}</p>
                                            <p class="text-muted text-center" style="margin-bottom: 28px;"></p>
                                            <a href="/users/{{ $data['user']['id'] }}/edit" class="btn btn-primary btn-block"><b>Modifier l'utilisateur</b></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            Information Linéaire Produit
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-6">
                                            <ul id="lindevBlock" class="list-group list-group-unbordered">
                                                @foreach($data['lin_devs'] as $lin_dev)
                                                    <li id="famille_{{$lin_dev['famille']['id']}}" class="list-group-item">
                                                        <b>{{$lin_dev['famille']['nom']}}</b> <span class="pull-right"><input  name="lindev[{{$lin_dev['famille']['id']}}]" type="text" value="{{ $lin_dev['lineaire_developpe'] }}" /> CM <label data-item-id="famille_{{$lin_dev['famille']['id']}}" class="removeLinDev label label-danger" >-</label></span>
                                                    </li>

                                                @endforeach
                                            </ul>
                                            <label class="col-lg-2 addLinDev pull-right label label-success">+</label>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            Information Location Linéaire
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-6">
                                            <ul id="locLinPrice" class="list-group list-group-unbordered">
                                                @foreach($data['loc_lins'] as $loc_lin)
                                                    <li id="marque_price_{{$loc_lin['marque']['id']}}" class="list-group-item">
                                                        <b>{{$loc_lin['marque']['nom'] }}</b> <span class="pull-right"><input name="loclin[{{ $loc_lin['marque']['id'] }}][prix]" value="{{ $loc_lin['prix'] }}"/> DZD</span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-lg-6">
                                            <ul id="locLinDuration" class="list-group list-group-unbordered">
                                                @foreach($data['loc_lins'] as $loc_lin)
                                                    <li id="marque_duration_{{$loc_lin['marque']['id']}}" class="list-group-item">
                                                        <b>Durée</b> <span class="pull-right"><input  name="loclin[{{ $loc_lin['marque']['id'] }}][duree]" value="{{ $loc_lin['duree'] }}"/> Mois <label data-item-id="{{$loc_lin['marque']['id']}}" class="removeLocLin label label-danger" >-</label></span>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <label class="col-lg-2 addLocLin pull-right label label-success">+</label>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                            Information Plv Store
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse in">
                                    <div id="plvs_container" class="box-body">

                                        @foreach($loclinDataUpdate as $marques_plvs)
                                        <div id="marque_plv_{{$marques_plvs['marque']['id']}}" class="row" >
                                          <h2 class="text-center"><a>{{$marques_plvs['marque']['nom']}}</a></h2>
                                          <div class="col-lg-6">
                                            <div class="row plv_title">
                                            <h4 class="col-lg-6" ><a>PLV IN-Store</a></h4>
                                            <h4 class="col-lg-3"><a>Quantité</a></h4>
                                            <h4 class="col-lg-3"><a>Date</a></h4>
                                          </div>
                                          <ul class="list-unstyled list-group list-group-unbordered">
                                            @foreach($marques_plvs['plvsIn'] as $plv_in)
                                                    <li class="row list-group-item"><b class="col-lg-6">{{$plv_in['nom']}}</b><input name="plv[{{$marques_plvs['marque']['id']}}][{{$plv_in['id']}}][quantite]" class="col-lg-2" type="text" value="{{ $plv_in['quantite'] }}" /><input name="plv[{{$marques_plvs['marque']['id']}}][{{$plv_in['id']}}][date]" class="col-lg-3 col-lg-offset-1" type="date" value="{{ $plv_in['date'] }}" /></li>
                                            @endforeach
                                           </ul>
                                          </div>
                                          <div class="col-lg-6">
                                            <div class="row plv_title">
                                            <h4 class="col-lg-6" ><a>PLV OUT-Store</a></h4>
                                            <h4 class="col-lg-3"><a>Quantité</a></h4>
                                            <h4 class="col-lg-3"><a>Date</a></h4>
                                          </div>
                                          <ul class="list-unstyled list-group list-group-unbordered">
                                            @foreach($marques_plvs['plvsOut'] as $plv_out)
                                                    <li class="row list-group-item"><b class="col-lg-6">{{$plv_out['nom']}}</b><input name="plv[{{$marques_plvs['marque']['id']}}][{{$plv_out['id']}}][quantite]" class="col-lg-2" type="text" value="{{ $plv_out['quantite'] }}"   /><input name="plv[{{$marques_plvs['marque']['id']}}][{{$plv_out['id']}}][date]" class="col-lg-3 col-lg-offset-1" type="date" value="{{ $plv_out['date'] }}" /></li>
                                            @endforeach
                                          </ul>
                                          </div>

                                        </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <!-- /.row -->
                </div>
                <!-- /.box -->
            </section>
        </div>
    </section>
  </form>
@stop
@section('footer')
    @parent
    <!-- Bootbox -->
    {!! HTML::script('plugins/bootbox/bootbox.min.js') !!}

    <script>

      $('#regionsSelect').change(function( event ){

        var region=$(event.target).val();
        //alert( region );

        $.ajax({
            url:'/wilayas/region/'+region,//base_url.region, //'/wilayas/region/'.region ,
            type: 'GET',
            success: function(result) {

              //alert( result );
              $("#wilayasSelect").html(result);


            },
            error : function(result) {
                bootbox.dialog({
                    closeButton: false,
                    message: "Une erreur s'est produite, veuillez reessayer",
                    title: "Faderco Information",
                    buttons: {
                        success: {
                            label: "Fermer!",
                            className: "btn-danger",
                            callback: function() {
                                location.href ='/pdvs';
                            }
                        }
                    }
                });
            }
        });

      });

      $('#wilayasSelect').change(function( event ){

        var wilaya=$(event.target).val();
        //alert( wilaya );

        $.ajax({
            url:'/communes/wilaya/'+wilaya,//base_url.region, //'/wilayas/region/'.region ,
            type: 'GET',
            success: function(result) {

              //alert( result );
              $("#communesSelect").html(result);


            },
            error : function(result) {
                //alert(result);
                bootbox.dialog({
                    closeButton: false,
                    message: "Une erreur s'est produite, veuillez reessayer",
                    title: "Faderco Information",
                    buttons: {
                        success: {
                            label: "Fermer!",
                            className: "btn-danger",
                            callback: function() {
                                location.href ='/pdvs';
                            }
                        }
                    }
                });
            }
        });

      });


      $('#lindevBlock').on('click','.removeLinDev',function(event){

        bootbox.dialog({
            closeButton: false,
            message: 'Voulez-vous supprimer cette famille ?',
            title: "Faderco Information",
            buttons: {
                oui: {
                    label: "Oui",
                    className: "btn-success",
                    callback: function() {
                      famille_id=$(event.target).data('item-id');
                      $('#'+famille_id).remove();
                    }
                },
                cancel: {
                    label: "Annuler",
                    className: "btn-danger",
                    callback: function() {

                    }
                }
            }
        });



      });

      $('.addLinDev').click(function(event){



        $.ajax({
            url:'/produits/familles/all',
            type: 'GET',
            success: function(result) {

              //alert( result );
              //$("#selectLinDevBlock").html(result);
              bootbox.dialog({
                  closeButton: false,
                  message: '<select id="lindevFamilles" >'+result+'</select>',
                  title: "Faderco Information",
                  buttons: {
                      success: {
                          label: "Valider",
                          className: "btn-success",
                          callback: function() {
                              //alert($("#lindevFamilles option:selected").text());
                              if($('#famille_'+$("#lindevFamilles").val()).length)
                              {
                                alert('cette famille existe déja !');
                              }
                              else {
                                new_lindev= '<li id="famille_'+$("#lindevFamilles").val()+'" class="list-group-item"><b>'+$("#lindevFamilles option:selected").text()+'</b><span class="pull-right"><input  name="lindev['+$("#lindevFamilles").val()+']" type="text" value="" /> CM <label data-item-id="famille_'+$("#lindevFamilles").val()+'" class="removeLinDev label label-danger" >-</label></span></li>';
                                $("#lindevBlock").append(new_lindev);
                              }

                          }
                      },
                      cancel: {
                          label: "Annuler",
                          className: "btn-danger",
                          callback: function() {

                          }
                      }
                  }
              });


            },
            error : function(result) {
                bootbox.dialog({
                    closeButton: false,
                    message: "Une erreur s'est produite, veuillez reessayer",
                    title: "Faderco Information",
                    buttons: {
                        success: {
                            label: "Fermer!",
                            className: "btn-danger",
                            callback: function() {
                                location.href ='/pdvs';
                            }
                        }
                    }
                });
            }
        });

      });



      $('#locLinDuration').on('click','.removeLocLin',function(event){

        bootbox.dialog({
            closeButton: false,
            message: 'Voulez-vous supprimer cette marque ?',
            title: "Faderco Information",
            buttons: {
                oui: {
                    label: "Oui",
                    className: "btn-success",
                    callback: function() {
                      marque_id=$(event.target).data('item-id');
                      $('#marque_price_'+marque_id).remove();
                      $('#marque_duration_'+marque_id).remove();
                      $('#marque_plv_'+marque_id).remove();
                    }
                },
                cancel: {
                    label: "Annuler",
                    className: "btn-danger",
                    callback: function() {

                    }
                }
            }
        });



      });

      $('.addLocLin').click(function(event){



        $.ajax({
            url:'/marques/marques/all',
            type: 'GET',
            success: function(result) {

              //alert( result );
              //$("#selectLinDevBlock").html(result);
              bootbox.dialog({
                  closeButton: false,
                  message: '<select id="locLinMarques" >'+result+'</select>',
                  title: "Faderco Information",
                  buttons: {
                      success: {
                          label: "Valider",
                          className: "btn-success",
                          callback: function() {
                              //alert($("#locLinMarques option:selected").text());

                              if($('#marque_price_'+$("#locLinMarques").val()).length)
                              {
                                alert('cette marque existe déja !');
                              }
                              else {
                                new_loclinPrice= '<li id="marque_price_'+$("#locLinMarques").val()+'" class="list-group-item"><b>'+$("#locLinMarques option:selected").text()+'</b><span class="pull-right"><input  name="loclin['+$("#locLinMarques").val()+'][prix]" type="text" value="" /> DZD </span></li>';
                                new_loclinDuration= '<li id="marque_duration_'+$("#locLinMarques").val()+'" class="list-group-item"><b>Durée</b><span class="pull-right"><input  name="loclin['+$("#locLinMarques").val()+'][duree]" type="text" value="" /> Mois <label data-item-id="'+$("#locLinMarques").val()+'" class="removeLocLin label label-danger" >-</label></span></li>';

                                $("#locLinPrice").append(new_loclinPrice);
                                $("#locLinDuration").append(new_loclinDuration);

                                addPlvs($("#locLinMarques option:selected").text(),$("#locLinMarques").val());

                              }
                              }
                      },
                      cancel: {
                          label: "Annuler",
                          className: "btn-danger",
                          callback: function() {

                          }
                      }
                  }
              });


            },
            error : function(result) {
                bootbox.dialog({
                    closeButton: false,
                    message: "Une erreur s'est produite, veuillez reessayer",
                    title: "Faderco Information",
                    buttons: {
                        success: {
                            label: "Fermer!",
                            className: "btn-danger",
                            callback: function() {
                                location.href ='/pdvs';
                            }
                        }
                    }
                });
            }
        });

      });

      function addPlvs(marque,marque_id)
      {

        var plvin,plvout;
        $.ajax({
            url:'/pdvs/pdvs/plvs',
            type: 'GET',
            success: function(result) {

              //alert( result );
              //$("#selectLinDevBlock").html(result);
              //console.log(result.out);
              plvout=result.out;
              plvin=result.in;

              instore='';
              $.each(plvin ,function(index,plv)
              {
                  instore+='<li class="row list-group-item"><b class="col-lg-6">'+plv.nom+'</b><input name="plv['+marque_id+']['+plv.id+'][quantite]" class="col-lg-2" type="text" value="" /><input name="plv['+marque_id+']['+plv.id+'][date]" class="col-lg-3 col-lg-offset-1" type="date" value="" /></li>';
              });
              //instore="test";
              outstore="";
              $.each(plvout,function(index,plv)
              {
                  outstore+='<li class="row list-group-item"><b class="col-lg-6">'+plv.nom+'</b><input name="plv['+marque_id+']['+plv.id+'][quantite]" class="col-lg-2" type="text" value="" /><input name="plv['+marque_id+']['+plv.id+'][date]" class="col-lg-3 col-lg-offset-1" type="date" value="" /></li>';
              });
              data= "<div id='marque_plv_"+marque_id+"' class='row'><h2 class='text-center'><a>"+marque+"</a></h2>";
              data+='<div class="col-lg-6"><div class="row plv_title"><h4 class="col-lg-6" ><a>PLV IN-Store</a></h4><h4 class="col-lg-3"><a>Quantité</a></h4><h4 class="col-lg-3"><a>Date</a></h4></div><ul class="list-unstyled list-group list-group-unbordered">'+instore+'</ul></div>';
              data+='<div class="col-lg-6"><div class="row plv_title"><h4 class="col-lg-6" ><a>PLV OUT-Store</a></h4><h4 class="col-lg-3"><a>Quantité</a></h4><h4 class="col-lg-3"><a>Date</a></h4></div><ul class="list-unstyled list-group list-group-unbordered">'+outstore+'</ul></div></div>';

              $('#plvs_container').append(data);


            },
            error : function(result) {
                bootbox.dialog({
                    closeButton: false,
                    message: "Une erreur s'est produite, veuillez reessayer",
                    title: "Faderco Information",
                    buttons: {
                        success: {
                            label: "Fermer!",
                            className: "btn-danger",
                            callback: function() {
                                location.href ='/pdvs';
                            }
                        }
                    }
                });
            }
        });

      }
      /*  $('#validate-pdv').click(function(){
            bootbox.dialog({
                closeButton: false,
                message: "Voulez-vous valider la mis à jour de ce point de vente ?",
                title: "Faderco Information",
                buttons: {
                    annuler: {
                        label: "Non !",
                        className: "btn-default",
                        callback: function() {

                        }
                    },
                    validate: {
                        label: "Oui, je confirme la validation.",
                        className: "btn-success",
                        callback: function() {
                            $.ajax({
                                url: '/pdvs/{{ $data['id'] }}',
                                type: 'GET',
                                success: function(result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Point de vente mis à jour avec succès",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-success",
                                                callback: function() {
                                                    location.href ='/pdvs/{{ $data['id'] }}';
                                                }
                                            }
                                        }
                                    });
                                },
                                error : function(result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Une erreur s'est produite, veuillez reessayer",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-danger",
                                                callback: function() {
                                                    location.href ='/pdvs';
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            });
        });*/

    </script>
@stop
