@extends('layouts.master')
@section('title', 'Points de vente')
@section('header')
    @parent
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Point de vente
            <small>Afficher</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active"><a href="/pdvs">Points de vente</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <section class="col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <div class="col-lg-6 no-padding">
                            <h3 class="box-title">Fiche Point de vente</h3>
                        </div>
                        <div class="col-lg-6 no-padding">
                            <a href="/pdvs/{{ $data['id'] }}/edit" type="button" id="edit-pdv" class="btn btn-primary pull-right "><i class="fa fa-edit"></i> Modifier</a>
                            @if($data['etat'])
                                <button type="button" id="disable-pdv" class="btn btn-warning pull-right" style="margin-right: 5px;" ><i class="fa fa-clock-o"></i> Désapprouver</button>
                            @else
                                <button type="button" id="enable-pdv" class="btn btn-success pull-right" style="margin-right: 5px;" ><i class="fa fa-check"></i> Approuver</button>
                            @endif
                            <button type="button" id="destroy-pdv" class="btn btn-danger pull-right" style="margin-right: 5px;" ><i class="fa fa-close"></i> Supprimer</button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- START ACCORDION -->
                        <div class="box-group" id="accordion">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Information Point de vente
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-4">
                                            <ul class="list-group list-group-unbordered">
                                                <li class="list-group-item">
                                                    <b>Nom</b> <a class="pull-right">{{ $data['nom'] }}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Région</b> <a class="pull-right">{{ $data['region']['nom'] }} - {{ $data['wilaya']['nom'] }} - {{ $data['commune']['nom'] }}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Canal d'achat</b> <a class="pull-right">{{ $data['canal_achat']['nom'] }} </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Type</b> <a class="pull-right">{{ $data['type_pdv']['nom'] }} </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Surface</b> <a class="pull-right">{{ $data['surface_pdv']['nom'] }} </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Zone chalandise</b> <a class="pull-right">{{ $data['zone_chalandise']['nom'] }} </a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Sortie de caisse</b> <a class="pull-right">{{ $data['nb_sortie_caisse'] }} </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-5">
                                            {!! $map !!}
                                        </div>
                                        <div class="col-lg-3">
                                            <img class="profile-user-img img-responsive img-circle" src="/{{ $data['user']['avatar'] }}" alt="User profile picture">
                                            <h3 class="profile-username text-center">{{ $data['user']['nom'] }}</h3>
                                            <p class="text-muted text-center">Nom d'utilisateur : {{ $data['user']['username'] }}</p>
                                            <p class="text-muted text-center">Téléphone : {{ $data['user']['telephone'] }}</p>
                                            <p class="text-muted text-center" style="margin-bottom: 28px;"></p>
                                            <a href="/users/{{ $data['user']['id'] }}/edit" class="btn btn-primary btn-block"><b>Modifier l'utilisateur</b></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            Information Linéaire Produit
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-6">
                                            <ul class="list-group list-group-unbordered">
                                                @foreach($data['lin_devs'] as $lin_dev)
                                                    <li class="list-group-item">
                                                        <b>{{$lin_dev['famille']['nom']}}</b> <a class="pull-right">{{ $lin_dev['lineaire_developpe'] }} CM</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            Information Location Linéaire
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-6">
                                            <ul class="list-group list-group-unbordered">
                                                @foreach($data['loc_lins'] as $loc_lin)
                                                    <li class="list-group-item">
                                                        <b>{{$loc_lin['marque']['nom'] }}</b> <a class="pull-right">{{ $loc_lin['prix'] }} DZD</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-lg-6">
                                            <ul class="list-group list-group-unbordered">
                                                @foreach($data['loc_lins'] as $loc_lin)
                                                    <li class="list-group-item">
                                                        <b>Durée</b> <a class="pull-right">{{ $loc_lin['duree'] }} Mois</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                            Information Plv Store
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-6">
                                            <ul class="list-unstyled list-group list-group-unbordered">
                                                <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><b class="pull-left">Plv-InStore</b></li>
                                                @foreach($data_marques_plvs as $marques_plvs)
                                                    <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><a class="pull-right">{{ $marques_plvs['marque'] }}</a></li>
                                                @endforeach
                                            </ul>
                                            @foreach($plvs_in as $plv_in)
                                                <ul class="list-unstyled list-group list-group-unbordered">
                                                    <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><b class="pull-left">{{$plv_in['nom']}}</b></li>
                                                    @foreach($data_marques_plvs as $marques_plvs)
                                                        @if($marques_plvs['plv_in'][$plv_in['nom']] == "oui")
                                                            <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><a class="pull-right"><span class="label label-success"><i class="fa fa-check"></i> Oui</span></a></li>
                                                        @else
                                                            <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><a class="pull-right"><span class="label label-danger"><i class="fa fa-close"></i> Non</span></a></li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endforeach
                                        </div>
                                        <div class="col-lg-6">
                                            <ul class="list-unstyled list-group list-group-unbordered">
                                                <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><b class="pull-left">Plv-OutStore</b></li>
                                                @foreach($data_marques_plvs as $marques_plvs)
                                                    <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><a class="pull-right">{{ $marques_plvs['marque'] }}</a></li>
                                                @endforeach
                                            </ul>
                                            @foreach($plvs_out as $plv_out)
                                                <ul class="list-unstyled list-group list-group-unbordered">
                                                    <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><b class="pull-left">{{$plv_out['nom']}}</b></li>
                                                    @foreach($data_marques_plvs as $marques_plvs)
                                                        @if($marques_plvs['plv_out'][$plv_out['nom']] == "oui")
                                                            <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><a class="pull-right"><span class="label label-success"><i class="fa fa-check"></i> Oui</span></a></li>
                                                        @else
                                                            <li class="list-group-item col-md-{{ str_replace('.', '-', (12/(count($data['loc_lins'])+1))) }}"><a class="pull-right"><span class="label label-danger"><i class="fa fa-close"></i> Non</span></a></li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <!-- /.row -->
                </div>
                <!-- /.box -->
            </section>
        </div>
    </section>
@stop
@section('footer')
    @parent
    <!-- Bootbox -->
    {!! HTML::script('plugins/bootbox/bootbox.min.js') !!}

    <script>
        $('#destroy-pdv').click(function(){
            bootbox.dialog({
                closeButton: false,
                message: "Voulez-vous supprimer ce point de vente ?",
                title: "Faderco Information",
                buttons: {
                    annuler: {
                        label: "Non !",
                        className: "btn-default",
                        callback: function() {

                        }
                    },
                    destroy: {
                        label: "Oui, je confirme la suppression.",
                        className: "btn-danger",
                        callback: function() {
                            $.ajax({
                                url: '/pdvs/{{ $data['id'] }}',
                                type: 'DELETE',
                                success: function(result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Point de vente supprimé avec succès",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-success",
                                                callback: function() {
                                                    location.href ='/pdvs';
                                                }
                                            }
                                        }
                                    });
                                },
                                error : function(result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Une erreur s'est produite, veuillez reessayer",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-danger",
                                                callback: function() {
                                                    location.href ='/pdvs';
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            });
        });
        @if($data['etat'])
            $('#disable-pdv').click(function(){
            bootbox.dialog({
                closeButton: false,
                message: "Voulez-vous désapprouver ce point de vente ?",
                title: "Faderco Information",
                buttons: {
                    annuler: {
                        label: "Non !",
                        className: "btn-default",
                        callback: function() {

                        }
                    },
                    destroy: {
                        label: "Oui, je confirme la désactivation.",
                        className: "btn-warning",
                        callback: function() {
                            $.ajax({
                                url: '/pdvs/{{ $data['id'] }}?type=disable',
                                type: 'PATCH',
                                success: function(result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Point de vente désapprouvé avec succès",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-success",
                                                callback: function() {
                                                    location.reload();
                                                }
                                            }
                                        }
                                    });
                                },
                                error : function(result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Une erreur s'est produite, veuillez reessayer",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-danger",
                                                callback: function() {
                                                    location.href ='/pdvs';
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            });
        });
        @else
            $('#enable-pdv').click(function(){
            bootbox.dialog({
                closeButton: false,
                message: "Voulez-vous approuver ce point de vente ?",
                title: "Faderco Information",
                buttons: {
                    annuler: {
                        label: "Non !",
                        className: "btn-default",
                        callback: function() {

                        }
                    },
                    destroy: {
                        label: "Oui, je confirme l'activation.",
                        className: "btn-success",
                        callback: function() {
                            $.ajax({
                                url: '/pdvs/{{ $data['id'] }}?type=enable',
                                type: 'PATCH',
                                success: function(result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Point de vente approuvé avec succès",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-success",
                                                callback: function() {
                                                    location.reload();
                                                }
                                            }
                                        }
                                    });
                                },
                                error : function(result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Une erreur s'est produite, veuillez reessayer",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-danger",
                                                callback: function() {
                                                    location.href ='/pdvs';
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            });
        });
        @endif
    </script>
@stop
