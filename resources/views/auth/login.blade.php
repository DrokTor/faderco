@extends('layouts.master')
@section('title', 'Connexion')
@section('header')
    @parent
    <!-- page level css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/pages/login.css') }}" />
    <!-- end of page level css -->
@endsection
@section('content')
    <div id="login" class="animate form">
        <form action="{{ url('/connexion') }}" autocomplete="on" method="post" role="form">
            <h3 class="">
                <img src="{{ asset('assets/img/logo.png') }}" alt="faderco logo">
            </h3>
            <!-- CSRF Token -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />

            <div class="form-group {{ $errors->first('username', 'has-error') }}">
                <label style="margin-bottom:0px;" for="email" class="uname control-label"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    Nom d'utilisateur
                </label>
                <input id="username" name="username" required type="username" placeholder="Votre nom d'utilisateur"
                       value="{!! old('username') !!}"/>
                <div class="col-sm-12">
                    {!! $errors->first('username', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->first('password', 'has-error') }}">
                <label style="margin-bottom:0px;" for="password" class="youpasswd"> <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#3c8dbc" data-hc="#3c8dbc"></i>
                    Mot de passe
                </label>
                <input id="password" name="password" required type="password" placeholder="Votre mot de passe" />
                <div class="col-sm-12">
                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="form-group">
                <input type="checkbox" name="remember-me" id="remember-me" value="remember-me" class="icheckbox_square-blue"/>
                <label>Se souvenir de moi</label>
            </div>
            <p class="login button">
                <input type="submit" value="Connexion" class="btn btn-primary" />
            </p>
        </form>
    </div>
@endsection

@section('footer')
    @parent
    <!--livicons-->
    <script src="{{ asset('assets/js/raphael-min.js') }}"></script>
    <script src="{{ asset('assets/js/livicons-1.4.min.js') }}"></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/login.js') }}" type="text/javascript"></script>

    <!-- end of global js -->
@endsection
