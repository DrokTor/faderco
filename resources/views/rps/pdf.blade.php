<!DOCTYPE html>
<html>
<head>
  <link media="all" type="text/css" rel="stylesheet" href="{{URL::to('/')}}/bootstrap/css/bootstrap.min.css">

  <style>
.bluelabel
{
  color:#3c8dbc;
}
.thickline
{
  border: 1px solid #3c8dbc;
}
.line
{
  border: 1px solid #c9c9c9;
  margin:5px;
}
.float-left
{
  width:32%;
  display:inline-block;

 }
.img-circle
{
  width:100px;
  padding:3px;

}
.table{
  margin-bottom :5px;
}
.table-line {
  width:100%;
  height: 20px;
 }
.table-cell{
  width:28%;
  display:inline-block;
  padding: 0px 11px;
  text-align: center;
}
.th{
  color: #3c8dbc;
  font-weight: bold;
 }
body
{
  font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
}
h1,h2,h3,h4
{
  font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
}
.section
{
  padding: 0px 20px;
}
  </style>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="text-center">Relevé des Prix</h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <section class="col-xs-12">
            <div class="box">

                 <!-- /.box-header -->
                <div class="box-body">
                    <!-- START ACCORDION -->
                    <div class="box-group" id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        {{-- general--}}
                        <div class="panel box box-primary">
                                  <hr class="bluelabel thickline">
                                 <h4 class="bluelabel box-title">
                                         Information Relevé des Prix Conccurence
                                 </h4>

                                 <div class="section">
                                  <div class="float-left ">
                                              <hr class="line"><b>Point de vente</b> <span class="bluelabel pull-right">{{ $rp['pdv_nom'] }}</span>
                                              <hr class="line"><b>Utilisateur</b> <span class="bluelabel pull-right">{{ $rp['user_nom'] }}</span>
                                              <hr class="line"><b>Marque</b> <span class="bluelabel pull-right">{{ $rp['marque_nom'] }}</span>
                                              <hr class="line"><b>Crée</b> <span class="bluelabel pull-right">{{ $rp['created_at'] }}</span><hr class="line">
                                  </div>
                                  <!--<div class="col-xs-3">
                                        {!! $map !!}
                                    </div>--->
                                    <div class="float-left text-center">
                                         <img class="img-circle"
                                             src="{{URL::to('/')}}/images/marques/{{ $rp['marque_id'] }}_square.png"
                                             >
                                        <h3 class="profile-username text-center">{{ $rp['marque_nom'] }}</h3>
                                    </div>
                                    <div class="float-left text-center">
                                       <img class="profile-user-img img-responsive img-circle"
                                             src="/{{ $rp['user_avatar'] }}"  />
                                        <h3 class="profile-username text-center">{{ $rp['user_nom'] }}</h3>
                                        <p class="text-muted text-center">Nom d'utilisateur
                                            : {{ $rp['user_username'] }}</p>
                                        <p class="text-muted text-center">Téléphone
                                            : {{ $rp['user_telephone'] }}</p>
                                    </div>
                                </div>
                         </div>
                        {{-- details --}}

                        @if(count($data_collection)>0)
                            <div class="panel box box-primary">
                                <hr class="thickline">
                                    <h4 class="bluelabel box-title">Details Relevé des Prix Concurrence</h4>

                                <div  class="section">

                                  @foreach($data_collection as $key=>$collection)

                                        <div class="table "><hr class="line">
                                                  <div class="table-line">
                                                          <span class="table-cell th"> {{ $key  }}</span>
                                                          <span class="table-cell th"> Pad</span>
                                                          <span class="table-cell th"> Pvc</span>
                                                  </div>
                                          <hr class="line">
                                                  @foreach($collection as $produit)
                                                    <div class="table-line">
                                                      <span class="table-cell">{{ $produit['produit']['produit_nom'] }}</span>
                                                       @foreach($produit['produit'] as $key=>$row)
                                                          @if($key != 'produit_nom')
                                                              <span class="table-cell">
                                                                      {{$row }}
                                                              </span>
                                                          @endif
                                                      @endforeach</div><hr class="line">
                                                  @endforeach
                                              </div><div class="text-center">###</div>
                                  @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- /.box-body -->
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
    </div>
</section>
</body>
</html>
