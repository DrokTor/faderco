@extends('layouts.master')
@section('title', 'Relevés des Prix')
@section('header')
    @parent
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Relevé des Prix
            <small>Afficher</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active"><a href="/rps">Relevés des Prix</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <section class="col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <div class="col-lg-6 no-padding">
                            <h3 class="box-title">Relevé des Prix</h3>
                        </div>
                        <div class="col-lg-6 no-padding">
                            <a type="button" id="edit-pdv" class="btn btn-primary pull-right" href="{{$rp['id']}}/pdf"><i
                                        class="fa fa-download"></i> Exporter en PDF
                            </a>
                            <button type="button" id="destroy-pdv" class="btn btn-danger pull-right"
                                    style="margin-right: 5px;"><i class="fa fa-close"></i> Supprimer
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- START ACCORDION -->
                        <div class="box-group" id="accordion">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            {{-- general--}}
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Information Relevé des Prix
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-3">
                                            <ul class="list-group list-group-unbordered">
                                                <li class="list-group-item">
                                                    <b>Point de vente</b> <a
                                                            class="pull-right">{{ $rp['pdv_nom'] }}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Utilisateur</b> <a
                                                            class="pull-right">{{ $rp['user_nom'] }}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Marque</b> <a
                                                            class="pull-right">{{ $rp['marque_nom'] }}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Crée</b> <a
                                                            class="pull-right">{{ $rp['created_at'] }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3">
                                            {!! $map !!}
                                        </div>
                                        <div class="col-lg-3">
                                            <img class="profile-user-img img-responsive img-circle"
                                                 src="/images/marques/{{ $rp['marque_id'] }}_square.png"
                                                 alt="User profile picture">
                                            <h3 class="profile-username text-center">{{ $rp['marque_nom'] }}</h3>
                                        </div>
                                        <div class="col-lg-3">
                                            <img class="profile-user-img img-responsive img-circle"
                                                 src="/{{ $rp['user_avatar'] }}" alt="User profile picture"/>
                                            <h3 class="profile-username text-center">{{ $rp['user_nom'] }}</h3>
                                            <p class="text-muted text-center">Nom d'utilisateur
                                                : {{ $rp['user_username'] }}</p>
                                            <p class="text-muted text-center">Téléphone
                                                : {{ $rp['user_telephone'] }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- details --}}
                            @if(count($data_collection)>0)
                                <div class="panel box box-primary">
                                    <div class="box-header">
                                        <h4 class="box-title"><a data-toggle="collapse" data-parent="#accordion"
                                                                 href="#collapseTwo">Details Relevé des Prix</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in">
                                        @foreach($data_collection as $key=>$collection)
                                            <div class="box-header">
                                                <ul class="list-group list-group-unbordered">
                                                    <li class="list-group-item col-md-4"><h4
                                                                class="text-success text-capitalize"><b>{{ $key  }}</b>
                                                        </h4></li>
                                                    <li class="list-group-item col-md-4"><h4
                                                                class="text-center text-info text-capitalize">pad</h4>
                                                    </li>
                                                    <li class="list-group-item col-md-4"><h4
                                                                class="text-center text-info text-capitalize">pvc</h4>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-lg-12 no-padding">
                                                    <ul class="list-group list-group-unbordered">
                                                        @foreach($collection as $produit)
                                                            <li class="list-group-item col-md-4">
                                                                <b class="text-muted">{{ $produit['produit']['produit_nom'] }}</b>
                                                            </li>
                                                            @foreach($produit['produit'] as $key=>$row)
                                                                @if($key != 'produit_nom')
                                                                    <li class="list-group-item col-md-4 text-center">
                                                                            {{$row }}
                                                                    </li>
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                        <!-- /.box-body -->
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
        </div>
    </section>
@stop
@section('footer')
    @parent
            <!-- Bootbox -->
    {!! HTML::script('plugins/bootbox/bootbox.min.js') !!}
    <script>
        $('#destroy-pdv').click(function () {
            bootbox.dialog({
                closeButton: false,
                message: "Voulez-vous supprimer ce Relevé ?",
                title: "Faderco Information",
                buttons: {
                    annuler: {
                        label: "Non !",
                        className: "btn-default",
                        callback: function () {

                        }
                    },
                    destroy: {
                        label: "Oui, je confirme la suppression.",
                        className: "btn-danger",
                        callback: function () {
                            $.ajax({
                                url: '/rps/{{ $rp['id'] }}',
                                type: 'DELETE',
                                success: function (result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Relevé supprimé avec succès",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-success",
                                                callback: function () {
                                                    location.href = '/rps';
                                                }
                                            }
                                        }
                                    });
                                },
                                error: function (result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Une erreur s'est produite, veuillez reessayer",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-danger",
                                                callback: function () {
                                                    location.href = '/rps';
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            });
        });
    </script>
@stop
