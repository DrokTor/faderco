@extends('layouts.master')
@section('title', 'PLV')

@section('content_header')
<h3>Modifier un PLV</h3>
@stop
@section('content')

  <section class="content-header">
      <h1>
          PLV
          <small>Edit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/plvs">PLV</a></li>
          <li class="active">Edit</li>
      </ol>
  </section>
  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 id="productID" class="box-title" data-id="{{$plv['id']}}">plv</h3>
               </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action={{URL::to('/').'/plvs/'.$plv['id']}}>
                  {{method_field('PATCH')}}
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input value="{{$plv['nom']}}" name="nom" class="form-control" id="" placeholder="nom du plv" type="text">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                    <label>Type</label><br>
                    <select name="type">
                      <option value="in" @if($plv['type']=='in') {{'selected=selected'}} @endif >in</option>
                      <option value="out" @if($plv['type']=='out') {{'selected=selected'}} @endif >out</option>
                    </select>
                    @if ($errors->has('type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                    @endif
                   </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
</section>

@stop
