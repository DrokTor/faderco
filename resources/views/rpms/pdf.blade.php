<!DOCTYPE html>
<html>
<head>
  <link media="all" type="text/css" rel="stylesheet" href="{{URL::to('/')}}/bootstrap/css/bootstrap.min.css">

  <style>
.bluelabel
{
  color:#3c8dbc;
}
.thickline
{
  border: 1px solid #3c8dbc;
}
.line
{
  border: 1px solid #c9c9c9;
  margin:5px;

}
.float-left
{
  width:32%;
  display:inline-block;



 }
.img-circle
{
  width:100px;
  padding:3px;

}
.table{
  margin-bottom :5px;
}
.table-line {
  width:100%;
  height: 20px;
  position: relative;
  margin-bottom: 10px;
 }
.table-cell{
  width:4%;
  display:inline-block;
  padding: 0px 11px;
  text-align: center;
}
.table-cell-10{
  width:12%;
  display:inline-block;
  padding: 0px 11px;
  text-align: center;
}
.table-cell-20{
  width:29%;
  display:inline-block;
  padding: 0px 11px;
  text-align: center;

}
.text-right{
  width:80%;
  display:inline-block;
  padding: 0px 11px;
  text-align: right;
  color:#3c8dbc;

}
.th{
  color: #3c8dbc;
  font-weight: bold;
 }
body
{
  font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
}
h1,h2,h3,h4
{
  font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
}
.section
{
  padding: 0px 20px;
}

.pull-right
{
  position: absolute;
  right: 0px;
  top:13px;
}
.label
{
  width: 10%;
  height: 10px;
  border-radius: 5px;
  display: inline-block;
}
.label-success
{
  background-color: rgb(98, 186, 87);
}
.label-danger
{
  background-color: rgb(186, 54, 54);
}
  </style>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="text-center">Rapport Merchandising</h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <section class="col-xs-12">
            <div class="box">

                 <!-- /.box-header -->
                <div class="box-body">
                    <!-- START ACCORDION -->
                    <div class="box-group" id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        {{-- general--}}
                        <div class="panel box box-primary">
                                  <hr class="bluelabel thickline">
                                 <h4 class="bluelabel box-title">
                                         Information Rapport Merchandising
                                 </h4>

                                 <div class="section">
                                  <div class="float-left "><div class="table-line">
                                              <hr class="line"><b>Point de vente</b> <span class="bluelabel pull-right">{{ $rpm['pdv_nom'] }}</span>
                                            </div><div class="table-line">
                                                <hr class="line">
                                              <b>Assortiment recommandé</b>
                                              @if($rpm['assortiment_recommande'] == 1) <span class="label label-success pull-right"><i class="fa fa-check"></i> Oui</span>
                                              @else  <span class="label label-danger pull-right"><i class="fa fa-close"></i> Non</span>
                                              @endif</div>
                                              <div class="table-line">
                                                <hr class="line">
                                              <b>Bloc produit</b>
                                              @if($rpm['bloc_produit'] == 1) <span class="label label-success pull-right"><i class="fa fa-check"></i> Oui</span>
                                              @else  <span class="label label-danger pull-right"><i class="fa fa-close"></i> Non</span>
                                              @endif</div>
                                              <div class="table-line">
                                                <hr class="line">
                                              <b>Planogramme</b>
                                              @if($rpm['planogramme'] == 1) <span class="label label-success pull-right"><i class="fa fa-check"></i> Oui</span>
                                              @else  <span class="label label-danger pull-right"><i class="fa fa-close"></i> Non</span>
                                              @endif</div>
                                              <div class="table-line">
                                                <hr class="line">
                                              <b>Planogramme</b>
                                              @if($rpm['planogramme'] == 1) <span class="label label-success pull-right"><i class="fa fa-check"></i> Oui</span>
                                              @else  <span class="label label-danger pull-right"><i class="fa fa-close"></i> Non</span>
                                              @endif</div>
                                              <div class="table-line">
                                                <hr class="line">
                                              <b>Mise avant tg</b>
                                              @if($rpm['mise_avant_tg'] == 1) <span class="label label-success pull-right"><i class="fa fa-check"></i> Oui</span>
                                              @else  <span class="label label-danger pull-right"><i class="fa fa-close"></i> Non</span>
                                              @endif
</div>
                                              <div class="table-line">
                                                <hr class="line">
                                              <b>Freins d'éxecution</b> <span class="bluelabel  ">{{ $rpm['execution_frein'] }}</span>
                                            </div>
                                  </div>
                                  <!--<div class="col-xs-3">
                                        {!! $map !!}
                                    </div>--->

                                    <div class="float-left text-center">
                                         <img class="img-circle"
                                             src="{{URL::to('/')}}/images/marques/{{ $rpm['marque_id'] }}_square.png"
                                             >
                                        <h3 class="profile-username text-center">{{ $rpm['marque_nom'] }}</h3>
                                    </div>

                                    <div class="float-left text-right text-center">
                                       <img class="profile-user-img img-responsive img-circle"
                                             src="/{{ $rpm['user_avatar'] }}"  />
                                        <h3 class="profile-username text-center">{{ $rpm['user_nom'] }}</h3>
                                        <p class="text-muted text-center">Nom d'utilisateur
                                            : {{ $rpm['user_username'] }}</p>
                                        <p class="text-muted text-center">Téléphone
                                            : {{ $rpm['user_telephone'] }}</p>
                                    </div>
                                </div>


                                @if(count($data_collection)>0)
                                        <div class="table"><hr class="thickline">
                                            <h4 class="bluelabel box-title">
                                                     Details Rapport Merchandising
                                             </h4>
                                        </div>
                                        <div class=" ">
                                             @foreach($data_collection as $key=>$collection)
                                                <div class="table">
                                                  <hr class="line">
                                                            <div class="table-line">
                                                                    <span class="table-cell th"> {{ $key  }}</span>
                                                                    <span class="table-cell th"> Up</span>
                                                                    <span class="table-cell th"> Pc</span>
                                                                    <span class="table-cell th"> Pvc</span>
                                                                    <span class="table-cell th"> Facing</span>
                                                                    <span class="table-cell-10 th"> Facing ma</span>
                                                                    <span class="table-cell th"> Dispo</span>
                                                                    <span class="table-cell th"> Stock</span>
                                                                    <span class="table-cell-20 th"> Raison rupture</span>
                                                             </div>
                                                    <hr class="line">

                                                    @foreach($collection as $produit)
                                                      <div class="table-line">
                                                        <span class="table-cell">{{ $produit['produit']['produit_nom'] }}</span>
                                                         @foreach($produit['produit'] as $key=>$row)
                                                            @if($key != 'produit_nom')

                                                                @if($key == 'rupture_reason_nom' )
                                                                    <span class="table-cell-20">
                                                                @elseif($key == 'facing_ma' )
                                                                    <span class="table-cell-10" >
                                                                @else
                                                                    <span class="table-cell" >
                                                                @endif

                                                                @if($key == 'rupture_reason_nom' )
                                                                         {{ ($row != '') ? $row : '-' }}
                                                                 @elseif($key == 'up' or $key == 'pc' or $key == 'pvc' or $key == 'facing' or $key == 'facing_ma' or $key == 'dispo' or $key='stock_reserve')
                                                                         {{$row }}
                                                                 @endif
                                                               </span>

                                                            @endif
                                                        @endforeach</div><hr class="line">
                                                    @endforeach
                                                </div><div class="text-center">###</div>



                                            @endforeach
                                          </div>

                                        </div>
                                 @endif

                                {{--observation--}}
                                @if($rpm['observation']!= '')
                                        <div class="table"><hr class="thickline">
                                            <h4 class="bluelabel box-title">
                                                     Observation
                                             </h4>
                                        </div>
                                        <div class="section">

                                                <div class="col-lg-12">
                                                    <p class="text-justify">{{ $rpm['observation'] }}</p>
                                                </div>
                                        </div>
                                @endif

                                @if(isset($photos))
                                  <hr class="thickline">
                                      <h4 class="bluelabel box-title">
                                               Photos
                                       </h4>
                                <div class="section">
                                    @foreach($photos as $photo)
                                             <img src="{{ $photo['url'] }}"  >
                                     @endforeach
                                </div>
                                @endif


                         </div>
                        {{-- details --}}


                    </div>
                    <!-- /.box-body -->
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
    </div>
</section>
</body>
</html>
