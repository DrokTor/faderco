@extends('layouts.master')
@section('title', 'Rapports Merchandising')
@section('header')
    @parent
    <link rel="stylesheet" href="/plugins/gallery/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="/plugins/gallery/css/bootstrap-image-gallery.css">
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rapport Merchandising
            <small>Afficher</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active"><a href="/rpms">Rapport Merchandising</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <section class="col-lg-12">
                <div class="box">
                    <div class="box-header">
                        <div class="col-lg-6 no-padding">
                            <h3 class="box-title">Rapport Merchandising</h3>
                        </div>
                        <div class="col-lg-6 no-padding">
                            <a type="button" id="edit-pdv" class="btn btn-primary pull-right" href="{{$rpm['id']}}/pdf"><i
                                        class="fa fa-download"></i> Exporter en PDF
                            </a>
                            <button type="button" id="destroy-pdv" class="btn btn-danger pull-right"
                                    style="margin-right: 5px;"><i class="fa fa-close"></i> Supprimer
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- START ACCORDION -->
                        <div class="box-group" id="accordion">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            {{-- general--}}
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Information Rapport Merchandising
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-4">
                                            <ul class="list-group list-group-unbordered">
                                                <li class="list-group-item">
                                                    <b>Point de vente</b> <a
                                                            class="pull-right">{{ $rpm['pdv_nom'] }}</a>
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Assortiment recommandé</b>
                                                    @if($rpm['assortiment_recommande'] == 1) <a class="pull-right"><span
                                                                class="label label-success"><i class="fa fa-check"></i> Oui</span></a>
                                                    @else <a class="pull-right"><span class="label label-danger"><i
                                                                    class="fa fa-close"></i> Non</span></a>
                                                    @endif
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Bloc produit</b>
                                                    @if($rpm['bloc_produit'] == 1) <a class="pull-right"><span
                                                                class="label label-success"><i class="fa fa-check"></i> Oui</span></a>
                                                    @else <a class="pull-right"><span class="label label-danger"><i
                                                                    class="fa fa-close"></i> Non</span></a>
                                                    @endif
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Planogramme</b>
                                                    @if($rpm['planogramme'] == 1) <a class="pull-right"><span
                                                                class="label label-success"><i class="fa fa-check"></i> Oui</span></a>
                                                    @else <a class="pull-right"><span class="label label-danger"><i
                                                                    class="fa fa-close"></i> Non</span></a>
                                                    @endif
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Mise en avant TG</b>
                                                    @if($rpm['mise_avant_tg'] == 1) <a class="pull-right"><span
                                                                class="label label-success"><i class="fa fa-check"></i> Oui</span></a>
                                                    @else <a class="pull-right"><span class="label label-danger"><i
                                                                    class="fa fa-close"></i> Non</span></a>
                                                    @endif
                                                </li>
                                                <li class="list-group-item">
                                                    <b>Freins d'éxecution</b> <a
                                                            class="pull-right">{{ $rpm['execution_frein'] }}</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-3">
                                            {!! $map !!}
                                        </div>
                                        <div class="col-lg-2">
                                            <img class="profile-user-img img-responsive img-circle"
                                                 src="/images/marques/{{ $rpm['marque_id'] }}_square.png"
                                                 alt="User profile picture">
                                            <h3 class="profile-username text-center">{{ $rpm['marque_nom'] }}</h3>
                                        </div>
                                        <div class="col-lg-3">
                                            <img class="profile-user-img img-responsive img-circle"
                                                 src="/{{ $rpm['user_avatar'] }}" alt="User profile picture"/>
                                            <h3 class="profile-username text-center">{{ $rpm['user_nom'] }}</h3>
                                            <p class="text-muted text-center">Nom d'utilisateur
                                                : {{ $rpm['user_username'] }}</p>
                                            <p class="text-muted text-center">Téléphone
                                                : {{ $rpm['user_telephone'] }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- details --}}
                            @if(count($data_collection)>0)
                                <div class="panel box box-primary">
                                    <div class="box-header">
                                        <h4 class="box-title"><a data-toggle="collapse" data-parent="#accordion"
                                                                 href="#collapseTwo">Details Rapport Merchandising</a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse in">
                                        @foreach($data_collection as $key=>$collection)
                                            <div class="box-header">
                                                <ul class="list-group list-group-unbordered">
                                                    <li class="list-group-item col-md-2"><h4
                                                                class="text-success text-capitalize"><b>{{ $key  }}</b>
                                                        </h4></li>
                                                    <li class="list-group-item col-md-1"><h4
                                                                class="text-center text-info text-capitalize">up</h4>
                                                    </li>
                                                    <li class="list-group-item col-md-1"><h4
                                                                class="text-center text-info text-capitalize">pc</h4>
                                                    </li>
                                                    <li class="list-group-item col-md-1"><h4
                                                                class="text-center text-info text-capitalize">pvc</h4>
                                                    </li>
                                                    <li class="list-group-item col-md-1"><h4
                                                                class="text-center text-info text-capitalize">
                                                            facing</h4></li>
                                                    <li class="list-group-item col-md-1"><h4
                                                                class="text-center text-info text-capitalize">facing
                                                            Ma</h4></li>
                                                    <li class="list-group-item col-md-1"><h4
                                                                class="text-center text-info text-capitalize">dispo</h4>
                                                    </li>
                                                    <li class="list-group-item col-md-1"><h4
                                                                class="text-center text-info text-capitalize">stock</h4>
                                                    </li>
                                                    <li class="list-group-item col-md-3"><h4
                                                                class="text-center text-info text-capitalize">raison
                                                            rupture</h4></li>
                                                </ul>
                                            </div>
                                            <div class="box-body">
                                                <div class="col-lg-12 no-padding">
                                                    <ul class="list-group list-group-unbordered">
                                                        @foreach($collection as $produit)
                                                            <li class="list-group-item col-md-2">
                                                                <b class="text-muted">{{ $produit['produit']['produit_nom'] }}</b>
                                                            </li>
                                                            @foreach($produit['produit'] as $key=>$row)
                                                                @if($key != 'produit_nom')
                                                                    @if($key == 'rupture_reason_nom' )
                                                                        <li class="list-group-item col-md-3 text-center">
                                                                            {{ ($row != '') ? $row : '-' }}
                                                                        </li>
                                                                    @elseif($key == 'up' or $key == 'pc' or $key == 'pvc' or $key == 'facing' or $key == 'facing_ma' or $key == 'dispo' or $key='stock_reserve')
                                                                        <li class="list-group-item col-md-1 text-center">
                                                                            {{$row }}
                                                                        </li>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            {{--observation--}}
                            @if($rpm['observation']!= '')
                                <div class="panel box box-primary">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                Observation
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="box-body">
                                            <div class="col-lg-12">
                                                <p>{{ $rpm['observation'] }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            {{--photos--}}
                            @if(isset($photos))
                                <div class="panel box box-primary">
                                    <div class="box-header">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                                Photos
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="box-body">
                                            <div class="col-lg-12">
                                                <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
                                                <div id="blueimp-gallery" class="blueimp-gallery"
                                                     data-use-bootstrap-modal="false">
                                                    <!-- The container for the modal slides -->
                                                    <div class="slides"></div>
                                                    <!-- Controls for the borderless lightbox -->
                                                    <h3 class="title"></h3>
                                                    <a class="prev">‹</a>
                                                    <a class="next">›</a>
                                                    <a class="close">×</a>
                                                    <a class="play-pause"></a>
                                                    <ol class="indicator"></ol>
                                                    <!-- The modal dialog, which will be used to wrap the lightbox content -->
                                                    <div class="modal fade">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            aria-hidden="true">&times;</button>
                                                                    <h4 class="modal-title"></h4>
                                                                </div>
                                                                <div class="modal-body next"></div>
                                                                <div class="modal-footer">
                                                                    <button type="button"
                                                                            class="btn btn-default pull-left prev">
                                                                        <i class="glyphicon glyphicon-chevron-left"></i>
                                                                        Précedent
                                                                    </button>
                                                                    <button type="button" class="btn btn-primary next">
                                                                        Suivant
                                                                        <i class="glyphicon glyphicon-chevron-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="links">
                                                    @foreach($photos as $photo)
                                                        <a href="/{{ $photo['url'] }}" title="{{ $rpm['pdv_nom'] }}"
                                                           data-gallery>
                                                            <img src="/{{ $photo['url'] }}" width="320" height="auto"
                                                                 alt="Rpm">
                                                        </a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <!-- /.box-body -->
                        <!-- /.row -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </section>
        </div>
    </section>
@stop
@section('footer')
    @parent
            <!-- Bootbox -->
    {!! HTML::script('plugins/bootbox/bootbox.min.js') !!}
            <!-- Gallery -->
    {!! HTML::script('plugins/gallery/js/jquery.blueimp-gallery.min.js') !!}
    {!! HTML::script('plugins/gallery/js/bootstrap-image-gallery.js') !!}
    <script>
        $('#destroy-pdv').click(function () {
            bootbox.dialog({
                closeButton: false,
                message: "Voulez-vous supprimer ce rapport ?",
                title: "Faderco Information",
                buttons: {
                    annuler: {
                        label: "Non !",
                        className: "btn-default",
                        callback: function () {

                        }
                    },
                    destroy: {
                        label: "Oui, je confirme la suppression.",
                        className: "btn-danger",
                        callback: function () {
                            $.ajax({
                                url: '/rpms/{{ $rpm['id'] }}',
                                type: 'DELETE',
                                success: function (result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Rapport supprimé avec succès",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-success",
                                                callback: function () {
                                                    location.href = '/rpms';
                                                }
                                            }
                                        }
                                    });
                                },
                                error: function (result) {
                                    bootbox.dialog({
                                        closeButton: false,
                                        message: "Une erreur s'est produite, veuillez reessayer",
                                        title: "Faderco Information",
                                        buttons: {
                                            success: {
                                                label: "Fermer!",
                                                className: "btn-danger",
                                                callback: function () {
                                                    location.href = '/rpms';
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            });
        });
    </script>
@stop
