@extends('layouts.master')
@section('title', 'Accueil')
@section('header')
    @parent
    <!-- Morris chart -->
    {!! HTML::style('plugins/morris/morris.css') !!}
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tableau de bord
            <small>Administation</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
            <li class="active">Tableau de bord</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ $data['pdvs'] }}</h3>

                        <p>Points de vente</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="/pdvs" class="small-box-footer">Plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ $data['rpms'] }}</h3>

                        <p>Rapports</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-clipboard"></i>
                    </div>
                    <a href="/rpms" class="small-box-footer">Plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ $data['users'] }}</h3>

                        <p>Utilisateurs</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="/users" class="small-box-footer">Plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{ $data['messages'] }}</h3>

                        <p>Messages</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-mail"></i>
                    </div>
                    <a href="/messages" class="small-box-footer">Plus <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-8 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                        <li class="pull-left header"><i class="fa fa-inbox"></i> Résumé des activités
                        </li>
                    </ul>
                    <div class="tab-content no-padding">
                        <!-- Morris chart - Sales -->
                        <div class="chart tab-pane active" id="pdvs-chart" style="position: relative; height: 300px;"></div>
                    </div>
                </div>
                <!-- /.nav-tabs-custom -->
            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-4 connectedSortable">
                <!-- Map box -->
                <div class="box box-solid bg-light-blue-gradient">
                    <div class="box-header">
                        <!-- tools box -->
                        <div class="pull-right box-tools">
                            <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
                                    data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                                <i class="fa fa-minus"></i></button>
                        </div>
                        <!-- /. tools -->
                        <i class="fa fa-map-marker"></i>

                        <h3 class="box-title">
                            Répartition des points de vente
                        </h3>
                    </div>
                    <div class="box-body">
                        <div id="world-mapppppp" style="height: 280px; width: 100%;">{!! $map !!}</div>
                    </div>
                </div>
                <!-- /.box -->
            </section>
            <!-- right col -->
        </div>
        <!-- /.row (activities row) -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <!-- Chat box -->
                <div class="box box-success">
                    <div class="box-header">
                        <i class="fa fa-comments-o"></i>

                        <h3 class="box-title">Activités des utilisateurs</h3>

                        <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                            <div class="btn-group" data-toggle="btn-toggle">
                                <button type="button" class="btn btn-default btn-sm active"><i
                                            class="fa fa-square text-green"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-sm"><i
                                            class="fa fa-square text-red"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="box-body chat" id="chat-box">
                        @foreach($activities as $activity)
                            <!-- chat item -->
                            <div class="item">
                                <img src="{{ $activity['user_avatar'] }}" alt="user image" class="online">

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{ $activity['created_at'] }}</small>
                                        {{ $activity['user_nom'] }}
                                    </a>
                                    {!!html_entity_decode($activity['message'])!!}
                                </p>
                                {{--<div class="attachment">
                                    <h4>Attachments:</h4>

                                    <p class="filename">
                                        Theme-thumbnail-image.jpg
                                    </p>

                                    <div class="pull-right">
                                        <button type="button" class="btn btn-primary btn-sm btn-flat">Open</button>
                                    </div>
                                </div>--}}
                                <!-- /.attachment -->
                            </div>
                            <!-- /.item -->
                        @endforeach
                    </div>
                    <!-- /.chat -->
                    {{--<div class="box-footer">
                        <div class="input-group">
                            <input class="form-control" placeholder="Type message...">

                            <div class="input-group-btn">
                                <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>--}}
                </div>
                <!-- /.box (chat box) -->
            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
@stop
@section('footer')
    @parent
    <!-- Bootbox -->
    {!! HTML::script('plugins/bootbox/bootbox.min.js') !!}
    <!-- Morris.js charts -->
    {!! HTML::script('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') !!}
    {!! HTML::script('plugins/morris/morris.min.js') !!}
    <!-- jvectormap -->
    {!! HTML::script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}
    {!! HTML::script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
    {!! HTML::script('plugins/jvectormap/jquery-jvectormap.algeria.js') !!}
    <!-- Slimscroll -->
    {!! HTML::script('plugins/slimScroll/jquery.slimscroll.min.js') !!}
    <!-- dashbord -->
    {!! HTML::script('dist/js/pages/dashboard.js') !!}
    <!-- AdminLTE for demo purposes -->
    {{--{!! HTML::script('dist/js/demo.js') !!}--}}
@stop