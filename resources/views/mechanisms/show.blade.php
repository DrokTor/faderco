@extends('layouts.master')

@section('content_header')
<h3>Mécanisme</h3>
@stop

@section('content')


 <div class="box">

             <!-- /.box-header -->
             <div class="box-body">
               <table class="table table-bordered">
                 <tbody>
                 <tr>
                   <td>Nom</td>
                   <td>{{$mechanism['nom']}}</td>
                 </tr>
                 <tr>
                   <td>Associée à</td>
                   <td>@foreach($actiontypes as $type)
                     {{$type}}
                     <br>
                   @endforeach</td>
                 </tr>
               </tbody>
             </table>
             </div>
             <!-- /.box-body -->

           </div>


@stop
