@extends('layouts.master')
@section('title', 'Marques')

@section('content_header')
<h3>Modifier une marque</h3>
@stop

@section('content')
  <section class="content-header">
      <h1>
          Marques
          <small>Edit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/marques">Marques</a></li>
          <li class="active">Edit</li>
      </ol>
  </section>
  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Marque</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action={{URL::to('/').'/marques/'.$marque['id']}} enctype="multipart/form-data">
                {{method_field('PATCH')}}
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input name="nom" class="form-control" id="" placeholder="nom de la marque" type="text" value="{{$marque['nom']}}">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('loclin') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Location linénaire</label><br>
                    <select name='loclin'>
                      <option value="1" @if($marque['loclin']=='1') selected='selected' @endif>Oui</option>
                      <option value="0"  @if($marque['loclin']=='0') selected='selected' @endif>Non</option>
                    </select>
                    @if ($errors->has('loclin'))
                        <span class="help-block">
                            <strong>{{ $errors->first('loclin') }}</strong>
                        </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('logo') ? ' has-error' : '' }}">
                    <label for="exampleInputFile">Logo de la marque</label>
                    <input name ="logo" id="exampleInputFile" type="file">

                    @if ($errors->has('logo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('logo') }}</strong>
                        </span>
                    @endif
                   </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
  </section>

@stop
