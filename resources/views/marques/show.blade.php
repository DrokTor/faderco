@extends('layouts.master')

@section('content_header')
<h3>Marque</h3>
@stop

@section('content')


 <div class="box">

             <!-- /.box-header -->
             <div class="box-body">
               <table class="table table-bordered">
                 <tbody>
                   <tr>
                     <td>Logo</td>
                     <td>{{$marque['logo']}}</td>
                   </tr>
                 <tr>
                   <td>Nom</td>
                   <td>{{$marque['nom']}}</td>
                 </tr>
                 <tr>
                   <td>Familles</td>
                   <td>@foreach($familles as $famille)
                     {{$famille}}
                     <br>
                   @endforeach</td>
                 </tr>
               </tbody>
             </table>
             </div>
             <!-- /.box-body -->

           </div>


@stop
