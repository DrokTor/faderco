<!DOCTYPE html>
<html>
<head>
  <link media="all" type="text/css" rel="stylesheet" href="{{URL::to('/')}}/bootstrap/css/bootstrap.min.css">

  <style>
.bluelabel
{
  color:#3c8dbc;
}
.thickline
{
  border: 1px solid #3c8dbc;
}
.line
{
  border: 1px solid #c9c9c9;
  margin:5px;
}
.float-left
{
  width:32%;
  display:inline-block;

 }
.img-circle
{
  width:100px;
  padding:3px;

}
.table{
  margin-bottom :5px;
}
.table-line {
  width:100%;
  height: 20px;
 }
.table-cell{
  width:28%;
  display:inline-block;
  padding: 0px 11px;
  text-align: center;
}
.text-right{
  width:80%;
  display:inline-block;
  padding: 0px 11px;
  text-align: right;
  color:#3c8dbc;

}
.th{
  color: #3c8dbc;
  font-weight: bold;
 }
body
{
  font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
}
h1,h2,h3,h4
{
  font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
}
.section
{
  padding: 0px 20px;
}
  </style>
</head>
<body>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="text-center">Relevé des Prix</h1>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <section class="col-xs-12">
            <div class="box">

                 <!-- /.box-header -->
                <div class="box-body">
                    <!-- START ACCORDION -->
                    <div class="box-group" id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        {{-- general--}}
                        <div class="panel box box-primary">
                                  <hr class="bluelabel thickline">
                                 <h4 class="bluelabel box-title">
                                         Information Relevé des Prix Conccurence
                                 </h4>

                                 <div class="section">
                                  <div class="float-left ">
                                              <hr class="line"><b>Point de vente</b> <span class="bluelabel pull-right">{{ $rvc['pdv_nom'] }}</span>
                                              <hr class="line"><b>Utilisateur</b> <span class="bluelabel pull-right">{{ $rvc['user_nom'] }}</span>
                                              <hr class="line"><b>Famille</b> <span class="bluelabel pull-right">{{ $rvc['famille_nom'] }}</span>
                                              <hr class="line"><b>Crée</b> <span class="bluelabel pull-right">{{ $rvc['created_at'] }}</span><hr class="line">
                                  </div>
                                  <!--<div class="col-xs-3">
                                        {!! $map !!}
                                    </div>--->

                                    <div class="text-right text-center">
                                       <img class="profile-user-img img-responsive img-circle"
                                             src="/{{ $rvc['user_avatar'] }}"  />
                                        <h3 class="profile-username text-center">{{ $rvc['user_nom'] }}</h3>
                                        <p class="text-muted text-center">Nom d'utilisateur
                                            : {{ $rvc['user_username'] }}</p>
                                        <p class="text-muted text-center">Téléphone
                                            : {{ $rvc['user_telephone'] }}</p>
                                    </div>
                                </div>
                                <div class="section"><hr class="line">
                                   <div class='table'>
                                     <div class="table-line"><b>Action type</b><div class=" text-right">{{$rvc['action_type_nom']}}</div></div>
                                     <hr class="line"><div class="table-line"><b>Mécanisme</b><div class=" text-right">{{$rvc['mechanism_nom']}}</div></div>
                                     <hr class="line"><div class="table-line"><b>Periode</b><div class=" text-right">{{$rvc['periode']}}</div></div>
                                   </div>
                                </div>
                                {{--observation--}}
                                @if($rvc['observation']!= '')
                                        <div class="table"><hr class="thickline">
                                            <h4 class="bluelabel box-title">
                                                     Observation
                                             </h4>
                                        </div>
                                        <div class="section">

                                                <div class="col-lg-12">
                                                    <p class="text-justify">{{ $rvc['observation'] }}</p>
                                                </div>
                                        </div>
                                @endif

                                @if(isset($photos))
                                  <hr class="thickline">
                                      <h4 class="bluelabel box-title">
                                               Photos
                                       </h4>
                                <div class="section">
                                    @foreach($photos as $photo)
                                             <img src="{{ $photo['thumbs'] }}"  >
                                     @endforeach
                                </div>
                                @endif


                         </div>
                        {{-- details --}}


                    </div>
                    <!-- /.box-body -->
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
    </div>
</section>
</body>
</html>
