@extends('layouts.master')
@section('title', 'Rapports Veille Concurrentielle')
@section('header')
@parent
<link rel="stylesheet" href="/plugins/gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" href="/plugins/gallery/css/bootstrap-image-gallery.css">
@stop
@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Rapports Veille Concurrentielle
        <small>Afficher</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
        <li class="active"><a href="/rvcs">Rapports Veille Concurrentielle</a></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <section class="col-lg-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-lg-6 no-padding">
                        <h3 class="box-title">Relevé des Prix</h3>
                    </div> 
                    <div class="col-lg-6 no-padding">
                        <a type="button" id="edit-pdv" class="btn btn-primary pull-right" href="{{$rvc['id']}}/pdf"><i
                                    class="fa fa-download"></i> Exporter en PDF
                        </a>
                        <button type="button" id="destroy-pdv" class="btn btn-danger pull-right"
                                style="margin-right: 5px;"><i class="fa fa-close"></i> Supprimer
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- START ACCORDION -->
                    <div class="box-group" id="accordion">
                        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                        {{-- general--}}
                        <div class="panel box box-primary">
                            <div class="box-header">
                                <h4 class="box-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        Information Rapport Veille Concurrentielle
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="box-body">
                                    <div class="col-lg-5">
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b>Point de vente</b> <a
                                                        class="pull-right">{{ $rvc['pdv_nom'] }}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Utilisateur</b> <a
                                                        class="pull-right">{{ $rvc['user_nom'] }}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Famille</b> <a
                                                        class="pull-right">{{ $rvc['famille_nom'] }}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Référence</b> <a
                                                        class="pull-right">{{ $rvc['reference'] }}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Crée</b> <a
                                                        class="pull-right">{{ $rvc['created_at'] }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-4">
                                        {!! $map !!}
                                    </div>
                                    <div class="col-lg-3">
                                        <img class="profile-user-img img-responsive img-circle"
                                             src="/{{ $rvc['user_avatar'] }}" alt="User profile picture"/>
                                        <h3 class="profile-username text-center">{{ $rvc['user_nom'] }}</h3>
                                        <p class="text-muted text-center">Nom d'utilisateur
                                            : {{ $rvc['user_username'] }}</p>
                                        <p class="text-muted text-center">Téléphone
                                            : {{ $rvc['user_telephone'] }}</p>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="col-lg-12">
                                        <ul class="list-group list-group-unbordered">
                                            <li class="list-group-item">
                                                <b>Action type</b> <a
                                                        class="pull-right">{{ $rvc['action_type_nom'] }}</a>
                                            </li>
                                            <li class="list-group-item">
                                                <b>Méchanisme</b> <a
                                                        class="pull-right">{{ $rvc['mechanism_nom'] }}</a>
                                            </li>
                                            @if( $rvc['autre'] != "")
                                                <li class="list-group-item">
                                                    <b>Précision</b> <a class="pull-right">{{ $rvc['autre'] }}</a>
                                                </li>
                                            @endif
                                            <li class="list-group-item">
                                                <b>Période</b> <a
                                                        class="pull-right">{{ $rvc['periode'] }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--observation--}}
                        @if($rvc['observation']!= '')
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            Observation
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-12">
                                            <p class="text-justify">{{ $rvc['observation'] }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        {{--photos--}}
                        @if(isset($photos))
                            <div class="panel box box-primary">
                                <div class="box-header">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            Photos
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="box-body">
                                        <div class="col-lg-12">
                                            <!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
                                            <div id="blueimp-gallery" class="blueimp-gallery"
                                                 data-use-bootstrap-modal="false">
                                                <!-- The container for the modal slides -->
                                                <div class="slides"></div>
                                                <!-- Controls for the borderless lightbox -->
                                                <h3 class="title"></h3>
                                                <a class="prev">‹</a>
                                                <a class="next">›</a>
                                                <a class="close">×</a>
                                                <a class="play-pause"></a>
                                                <ol class="indicator"></ol>
                                                <!-- The modal dialog, which will be used to wrap the lightbox content -->
                                                <div class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title"></h4>
                                                            </div>
                                                            <div class="modal-body next"></div>
                                                            <div class="modal-footer">
                                                                <button type="button"
                                                                        class="btn btn-default pull-left prev">
                                                                    <i class="glyphicon glyphicon-chevron-left"></i>
                                                                    Précedent
                                                                </button>
                                                                <button type="button" class="btn btn-primary next">
                                                                    Suivant
                                                                    <i class="glyphicon glyphicon-chevron-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="links">
                                                @foreach($photos as $photo)
                                                    <a href="/{{ $photo['url'] }}" title="{{ $rvc['pdv_nom'] }}" data-gallery>
                                                        <img src="/{{ $photo['thumbs'] }}" alt="Rvc">
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- /.box-body -->
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
    </div>
</section>
@stop
@section('footer')
@parent
        <!-- Bootbox -->
{!! HTML::script('plugins/bootbox/bootbox.min.js') !!}
        <!-- Gallery -->
{!! HTML::script('plugins/gallery/js/jquery.blueimp-gallery.min.js') !!}
{!! HTML::script('plugins/gallery/js/bootstrap-image-gallery.js') !!}
<script>
    $('#destroy-pdv').click(function () {
        bootbox.dialog({
            closeButton: false,
            message: "Voulez-vous supprimer ce Rapport ?",
            title: "Faderco Information",
            buttons: {
                annuler: {
                    label: "Non !",
                    className: "btn-default",
                    callback: function () {

                    }
                },
                destroy: {
                    label: "Oui, je confirme la suppression.",
                    className: "btn-danger",
                    callback: function () {
                        $.ajax({
                            url: '/rvcs/{{ $rvc['id'] }}',
                            type: 'DELETE',
                            success: function (result) {
                                bootbox.dialog({
                                    closeButton: false,
                                    message: "Rapport supprimé avec succès",
                                    title: "Faderco Information",
                                    buttons: {
                                        success: {
                                            label: "Fermer!",
                                            className: "btn-success",
                                            callback: function () {
                                                location.href = '/rvcs';
                                            }
                                        }
                                    }
                                });
                            },
                            error: function (result) {
                                bootbox.dialog({
                                    closeButton: false,
                                    message: "Une erreur s'est produite, veuillez reessayer",
                                    title: "Faderco Information",
                                    buttons: {
                                        success: {
                                            label: "Fermer!",
                                            className: "btn-danger",
                                            callback: function () {
                                                location.href = '/rvcs';
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            }
        });
    });
</script>
@stop
