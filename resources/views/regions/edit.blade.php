@extends('layouts.master')
@section('title', 'Régions')

@section('content_header')
<h3>Modifier une Région</h3>
@stop
@section('content')

  <section class="content-header">
      <h1>
          Régions
          <small>Edit</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
          <li class="active"><a href="/regions">Régions</a></li>
          <li class="active">Edit</li>
      </ol>
  </section>

  <section class="content">
  <div class="box box-primary">
              <div class="box-header with-border">
                <h3 id="productID" class="box-title" data-id="{{$region['id']}}">Région</h3>
               </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="post" action={{URL::to('/').'/regions/'.$region['id']}}>
                  {{method_field('PATCH')}}
                <div class="box-body">
                  <div class="form-group {{ $errors->has('nom') ? ' has-error' : '' }}">
                    <label for="exampleInputEmail1">Nom</label>
                    <input value="{{$region['nom']}}" name="nom" class="form-control" id="" placeholder="nom de la région" type="text">
                    @if ($errors->has('nom'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nom') }}</strong>
                        </span>
                    @endif
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
              </form>
            </div>
</section>

@stop
