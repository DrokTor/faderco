<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRowRuptureReasonToProduitRpm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produit_rpm', function (Blueprint $table) {
            $table->unsignedInteger('rupture_reason_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produit_rpm', function (Blueprint $table) {
            $table->unsignedInteger('rupture_reason_id')->nullable(false)->change();
        });
    }
}
