<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarquePdvPlvPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marque_pdv_plv', function (Blueprint $table) {
            $table->integer('marque_pdv_id')->unsigned()->index();
            $table->foreign('marque_pdv_id')->references('id')->on('marque_pdv')->onDelete('cascade');
            $table->integer('plv_id')->unsigned()->index();
            $table->foreign('plv_id')->references('id')->on('plvs')->onDelete('cascade');
            $table->primary(['marque_pdv_id', 'plv_id']);
            $table->string('quantite');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('marque_pdv_plv');
    }
}
