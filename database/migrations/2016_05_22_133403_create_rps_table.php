<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pdv_id')->unsigned();
            $table->foreign('pdv_id')->references('id')->on('pdvs')->onDelete('cascade');
            $table->integer('marque_id')->unsigned();
            $table->foreign('marque_id')->references('id')->on('marques')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rps');
    }
}
