<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionTypeMechanismPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_type_mechanism', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('action_type_id')->unsigned()->index();
            $table->foreign('action_type_id')->references('id')->on('action_types')->onDelete('cascade');
            $table->integer('mechanism_id')->unsigned()->index();
            $table->foreign('mechanism_id')->references('id')->on('mechanisms')->onDelete('cascade');
            //$table->primary(['action_type_id', 'mechanism_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('action_type_mechanism');
    }
}
