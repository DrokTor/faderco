<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduitRpPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produit_rp', function (Blueprint $table) {
            $table->integer('produit_id')->unsigned()->index();
            $table->foreign('produit_id')->references('id')->on('produits')->onDelete('cascade');
            $table->integer('rp_id')->unsigned()->index();
            $table->foreign('rp_id')->references('id')->on('rps')->onDelete('cascade');
            $table->primary(['produit_id', 'rp_id']);
            $table->string("pad");
            $table->string("pvc");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produit_rp');
    }
}
