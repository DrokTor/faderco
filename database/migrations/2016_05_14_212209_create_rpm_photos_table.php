<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRpmPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpm_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->integer('rpm_id')->unsigned();
            $table->foreign('rpm_id')->references('id')->on('rpms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rpm_photos');
    }
}
