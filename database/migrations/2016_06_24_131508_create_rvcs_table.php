<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRvcsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rvcs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference');
            $table->string('autre');
            $table->integer('periode');
            $table->text('observation');
            $table->integer('action_type_mechanism_id')->unsigned();
            $table->foreign('action_type_mechanism_id')->references('id')->on('action_type_mechanism')->onDelete('cascade');
            $table->integer('pdv_id')->unsigned();
            $table->foreign('pdv_id')->references('id')->on('pdvs')->onDelete('cascade');
            $table->integer('famille_id')->unsigned();
            $table->foreign('famille_id')->references('id')->on('familles')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rvcs');
    }
}
