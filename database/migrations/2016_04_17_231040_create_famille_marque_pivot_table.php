<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamilleMarquePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('famille_marque', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('famille_id')->unsigned()->index();
            $table->foreign('famille_id')->references('id')->on('familles')->onDelete('cascade');
            $table->integer('marque_id')->unsigned()->index();
            $table->foreign('marque_id')->references('id')->on('marques')->onDelete('cascade');
            //$table->primary(['famille_id', 'marque_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('famille_marque');
    }
}
