<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProduitRpmPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produit_rpm', function (Blueprint $table) {
            $table->integer('produit_id')->unsigned()->index();
            $table->foreign('produit_id')->references('id')->on('produits')->onDelete('cascade');
            $table->integer('rpm_id')->unsigned()->index();
            $table->foreign('rpm_id')->references('id')->on('rpms')->onDelete('cascade');
            $table->primary(['produit_id', 'rpm_id']);
            $table->string('up');
            $table->string('pc');
            $table->string('pvc');
            $table->string('facing');
            $table->string('facing_ma');
            $table->boolean('dispo');
            $table->boolean('stock_reserve');
            $table->integer('rupture_reason_id')->unsigned();
            $table->foreign('rupture_reason_id')->references('id')->on('rupture_reasons')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('produit_rpm');
    }
}
