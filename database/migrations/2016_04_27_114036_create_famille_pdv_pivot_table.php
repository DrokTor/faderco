<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFamillePdvPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('famille_pdv', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('famille_id')->unsigned()->index();
            $table->foreign('famille_id')->references('id')->on('familles')->onDelete('cascade');
            $table->integer('pdv_id')->unsigned()->index();
            $table->foreign('pdv_id')->references('id')->on('pdvs')->onDelete('cascade');
            //$table->primary(['famille_id', 'pdv_id']);
            $table->string('lineaire_developpe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('famille_pdv');
    }
}
