<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePdvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pdvs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('localisation_lat');
            $table->string('localisation_long');
            $table->string('nb_sortie_caisse');
            $table->boolean('etat');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
            $table->integer('wilaya_id')->unsigned();
            $table->foreign('wilaya_id')->references('id')->on('wilayas')->onDelete('cascade');
            $table->integer('commune_id')->unsigned();
            $table->foreign('commune_id')->references('id')->on('communes')->onDelete('cascade');
            $table->integer('canal_achat_id')->unsigned();
            $table->foreign('canal_achat_id')->references('id')->on('canal_achats')->onDelete('cascade');
            $table->integer('type_pdv_id')->unsigned();
            $table->foreign('type_pdv_id')->references('id')->on('type_pdvs')->onDelete('cascade');
            $table->integer('surface_pdv_id')->unsigned();
            $table->foreign('surface_pdv_id')->references('id')->on('surface_pdvs')->onDelete('cascade');
            $table->integer('zone_chalandise_id')->unsigned();
            $table->foreign('zone_chalandise_id')->references('id')->on('zone_chalandises')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pdvs');
    }
}
