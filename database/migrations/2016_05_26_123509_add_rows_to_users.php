<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRowsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('telephone')->after('password')->default("-");
            $table->string('secteur')->after('email')->default("-");
            $table->string('fonction')->after('secteur')->default("-");
            $table->string('responsable')->after('fonction')->default("-");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('telephone');
            $table->dropColumn('secteur');
            $table->dropColumn('fonction');
            $table->dropColumn('responsable');
        });
    }
}
