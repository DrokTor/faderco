<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRowExecutionFrinId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpms', function (Blueprint $table) {
            $table->unsignedInteger('execution_frein_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpms', function (Blueprint $table) {
            $table->unsignedInteger('execution_frein_id')->nullable(false)->change();
        });
    }
}
