<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRvcPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rvc_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->integer('rvc_id')->unsigned();
            $table->foreign('rvc_id')->references('id')->on('rvcs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rvc_photos');
    }
}
