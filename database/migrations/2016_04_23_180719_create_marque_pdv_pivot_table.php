<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarquePdvPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marque_pdv', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marque_id')->unsigned()->index();
            $table->foreign('marque_id')->references('id')->on('marques')->onDelete('cascade');
            $table->integer('pdv_id')->unsigned()->index();
            $table->foreign('pdv_id')->references('id')->on('pdvs')->onDelete('cascade');
            //$table->primary(['marque_id', 'pdv_id']);
            $table->string('duree');
            $table->string('prix');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('marque_pdv');
    }
}
