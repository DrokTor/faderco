<?php

use Illuminate\Database\Seeder;

class CanalAchatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
           DB::table('canal_achats')->truncate();

          $now = date('Y-m-d H:i:s');

          $data = [ 'Moderne A', 'Moderne B', 'Détails' ];

          foreach($data as $row) {
              DB::table('canal_achats')->insert(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);
          }
      }
}
