<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::statement('SET FOREIGN_KEY_CHECKS=0;');

         $this->call(RolesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(OauthsTableSeeder::class);

         $this->call(RegionsTableSeeder::class);
         $this->call(WilayasTableSeeder::class);
         $this->call(CommunesTableSeeder::class);

         $this->call(MarquesTableSeeder::class);
         $this->call(FamillesTableSeeder::class);
         $this->call(ProduitsTableSeeder::class);

         $this->call(PdvSurfacesTableSeeder::class);
         $this->call(PdvTypesTableSeeder::class);
         $this->call(ZoneChalandisesTableSeeder::class);

         $this->call(PlvsTableSeeder::class);
         $this->call(CanalAchatsTableSeeder::class);

         $this->call(TypeActivitiesTableSeeder::class);

         $this->call(MessagesTableSeeder::class);

         $this->call(RuptureReasonsTableSeeder::class);
         $this->call(ExecutionFreinsTableSeeder::class);

         $this->call(MechanismsTableSeeder::class);
         $this->call(ActionTypesTableSeeder::class);

         $this->call(VersioningTableSeeder::class);

         DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
