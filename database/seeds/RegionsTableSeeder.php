<?php

use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regions')->truncate();

        //date
        $now = date('Y-m-d H:i:s');

        $data = ['Centre', 'Est', 'Ouest','Sud'];

        foreach($data as $row) {
            DB::table('regions')->insert(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);
        }
    }
}
