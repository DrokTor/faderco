<?php

use Illuminate\Database\Seeder;

class WilayasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wilayas')->truncate();

        //date
        $now = date('Y-m-d H:i:s');

        $data = ['Adrar', 'Chlef', 'Laghouat', 'Oum El Bouaghi', 'Batna', 'Béjaïa', 'Biskra', 'Béchar', 'Blida', 'Bouira',
            'Tamanrasset', 'Tébessa', 'Tlemcen', 'Tiaret', 'Tizi Ouzou', 'Alger', 'Djelfa', 'Jijel', 'Sétif', 'Saïda', 'Skikda',
            'Sidi Bel Abbès', 'Annaba', 'Guelma', 'Constantine', 'Médéa', 'Mostaganem', 'M\'Sila', 'Mascara', 'Ouargla', 'Oran',
            'El Bayadh', 'Illizi', 'Bordj Bou Arreridj', 'Boumerdès', 'El Tarf', 'Tindouf', 'Tissemsilt', 'El Oued', 'Khenchela',
            'Souk Ahras', 'Tipaza', 'Mila', 'Aïn Defla', 'Naâma', 'Aïn Témouchent', 'Ghardaïa', 'Relizane'];

        $regions = ['4', '3', '4', '2', '2', '2', '4', '4', '1', '1', '4', '2', '3', '3', '2', '1', '4', '2', '2', '3',
            '2', '3', '2', '2', '2', '1', '3', '3', '3', '4', '3', '4', '4', '2', '1', '2', '4', '3', '4', '2', '2',
            '1', '2', '3', '4', '3', '4', '3'];

        $region_id = 0;
        $code =1;
        foreach($data as $row) {
            DB::table('wilayas')->insert(['code'=> $code ,'nom' => $row, 'region_id'=>$regions[$region_id], 'created_at' => $now, 'updated_at' => $now ]);

            $region_id++;
            $code++;
        }
    }
}
