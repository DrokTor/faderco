<?php

use App\Models\Versioning;
use Illuminate\Database\Seeder;

class VersioningTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('versioning')->truncate();

        Versioning::create(['version' => 1]);
    }
}
