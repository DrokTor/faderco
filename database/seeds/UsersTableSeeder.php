<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        User::create([
            'nom' => 'Amine ABAOUB',
            'username' => 'messi89',
            'password' => bcrypt('123456'),
            'email' => 'a.abaoub@overgen.com',
            'avatar' => 'none',
            'role_id' => Role::find(1)->id
        ]);

        User::create([
            'nom' => 'Demo',
            'username' => 'demo',
            'password' => bcrypt('demo'),
            'email' => 'demo@overgen.com',
            'avatar' => 'none',
            'role_id' => Role::find(2)->id
        ]);

        User::create([
            'nom' => 'Faderco',
            'username' => 'faderco',
            'password' => bcrypt('faderco'),
            'email' => 'faderco@overgen.com',
            'avatar' => 'none',
            'role_id' => Role::find(2)->id
        ]);
    }
}
