<?php

use Illuminate\Database\Seeder;

class PdvTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         DB::table('type_pdvs')->truncate();

         $now = date('Y-m-d H:i:s');

         $data = ['GE', 'SUP', 'AG','COS', 'PH', 'TJ', 'GS'];

         foreach($data as $row) {
             DB::table('type_pdvs')->insert(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);
         }
     }
}
