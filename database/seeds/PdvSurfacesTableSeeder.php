<?php

use Illuminate\Database\Seeder;

class PdvSurfacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         DB::table('surface_pdvs')->truncate();

         $now = date('Y-m-d H:i:s');

         $data = ['De 0 à 25 m²', 'De 25 à 50 m²', 'De 50 à 100 m²','Plus de 100 m²'];

         foreach($data as $row) {
             DB::table('surface_pdvs')->insert(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);
         }
     }
}
