<?php

use Illuminate\Database\Seeder;
use App\Models\Marque;

class FamillesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('familles')->truncate();
        DB::table('famille_marque')->truncate();

        //date
        $now = date('Y-m-d H:i:s');

        //awane
        $awane_data = ['Confort', 'Confort Eco', 'Confort 3D','Confort 3D Duo Pack', 'Ultrafine', 'Protége Slip'];

        $awane = Marque::where('nom','=', 'Awane')->first();

        foreach($awane_data as $row) {
            $famille_id = DB::table('familles')->insertGetId(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);

            $awane->familles()->attach($famille_id);
        }

        //bimbies
        $bimbies_data = ['Small Pack', 'Pack Pratique', 'Pack Eco','Bonus Pack', 'Lingette'];

        $bimbies = Marque::where('nom','=', 'Bimbies')->first();

        foreach($bimbies_data as $row) {
            $famille_id = DB::table('familles')->insertGetId(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);

            $bimbies->familles()->attach($famille_id);
        }

        //cotex
        $cotex_data = ['Essuietout', 'Papier Hygiènique', 'Serviette de table','Mouchoir Box', 'Mouchoir Etuit'];

        $cotex = Marque::where('nom','=', 'Cotex')->first();

        foreach($cotex_data as $row) {
            $famille_id = DB::table('familles')->insertGetId(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);

            $cotex->familles()->attach($famille_id);
        }

        //uniform
        $uniform_data = ['Couche', 'Lingette'];

        $uniform = Marque::where('nom','=', 'Uni-Form')->first();

        foreach($uniform_data as $row) {
            $famille_id = DB::table('familles')->insertGetId(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);

            $uniform->familles()->attach($famille_id);
        }

        //coty'lys
        $cotylys_data = ['Coton tige', 'Disque démaquillant'];

        $cotylys = Marque::where('nom','=', 'Coty\'Lys')->first();

        foreach($cotylys_data as $row) {
            $famille_id = DB::table('familles')->insertGetId(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);

            $cotylys->familles()->attach($famille_id);
        }
    }
}
