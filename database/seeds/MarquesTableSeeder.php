<?php

use Illuminate\Database\Seeder;

class MarquesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marques')->truncate();

        //date
        $now = date('Y-m-d H:i:s');

        $data = ['Awane', 'Bimbies', 'Cotex','Uni-Form', 'Coty\'Lys'];

        foreach($data as $row) {
            DB::table('marques')->insert(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);
        }
    }
}
