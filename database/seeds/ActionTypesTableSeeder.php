<?php

use App\Models\ActionType;
use App\Models\Mechanism;
use Illuminate\Database\Seeder;

class ActionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('action_type_mechanism')->truncate();
        DB::table('action_types')->truncate();

        $now = date('Y-m-d H:i:s');

        $data = ['Promotion consommateur', 'Promotion trade', 'Animation','PLV Instore', 'PLV Outstore',
            'Sampling', 'Lancement nouveau produit', 'Lancement nouveau pack', 'Changement de prix'];

        $promo_conso_mechanisms = ['Prix barré', 'Goodies offerts', 'Echantillon gratuit','1+1', '2+1', '3+1', 'Autre'];
        $promo_trade_mechanisms = ['Prix barré', 'Gain à l\'achat', 'Autre'];
        $animation_mechanisms = ['Tombola', 'Test produit', 'Vente simple', 'Autre'];
        $plv_in_mechanisms = ['Réglettes', 'Présentoir de comptoir', 'Présentoir au sol', 'Stop Rayon', 'TG', 'Stick floor', 'Autre'];
        $plv_out_mechanisms = ['Tindas', 'Potences', 'Enseignes', 'Plaques', 'One way vision', 'Autre'];
        $sampling_mechanisms = ['Autre'];
        $nouveau_prod_mechanisms = ['Autre'];
        $nouveau_pack_mechanisms = ['Autre'];
        $change_prix_mechanisms = ['Autre'];

        //insertion
        foreach($data as $row) {
            DB::table('action_types')->insertGetId(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);
        }

        //Promotion consommateur
        $promo_conso = ActionType::where('nom','=', 'Promotion consommateur')->first();

        foreach($promo_conso_mechanisms as $row) {
            $mechanism = Mechanism::where('nom', '=', $row)->first();

            $promo_conso->mechanisms()->attach($mechanism->id);
        }

        //Promotion trade
        $promo_trade = ActionType::where('nom','=', 'Promotion trade')->first();

        foreach($promo_trade_mechanisms as $row) {
            $mechanism = Mechanism::where('nom', '=', $row)->first();

            $promo_trade->mechanisms()->attach($mechanism->id);
        }

        //Animation
        $animation = ActionType::where('nom','=', 'Animation')->first();

        foreach($animation_mechanisms as $row) {
            $mechanism = Mechanism::where('nom', '=', $row)->first();

            $animation->mechanisms()->attach($mechanism->id);
        }

        //PLV Instore
        $plv_in = ActionType::where('nom','=', 'PLV Instore')->first();

        foreach($plv_in_mechanisms as $row) {
            $mechanism = Mechanism::where('nom', '=', $row)->first();

            $plv_in->mechanisms()->attach($mechanism->id);
        }

        //PLV Outstore
        $plv_out = ActionType::where('nom','=', 'PLV Outstore')->first();

        foreach($plv_out_mechanisms as $row) {
            $mechanism = Mechanism::where('nom', '=', $row)->first();

            $plv_out->mechanisms()->attach($mechanism->id);
        }

        //Sampling
        $sampling = ActionType::where('nom','=', 'Sampling')->first();

        foreach($sampling_mechanisms as $row) {
            $mechanism = Mechanism::where('nom', '=', $row)->first();

            $sampling->mechanisms()->attach($mechanism->id);
        }

        //Lancement nouveau produit
        $nouveau_prod = ActionType::where('nom','=', 'Lancement nouveau produit')->first();

        foreach($nouveau_prod_mechanisms as $row) {
            $mechanism = Mechanism::where('nom', '=', $row)->first();

            $nouveau_prod->mechanisms()->attach($mechanism->id);
        }

        //Lancement nouveau pack
        $nouveau_pack = ActionType::where('nom','=', 'Lancement nouveau pack')->first();

        foreach($nouveau_pack_mechanisms as $row) {
            $mechanism = Mechanism::where('nom', '=', $row)->first();

            $nouveau_pack->mechanisms()->attach($mechanism->id);
        }

        //Changement de prix
        $change_prix = ActionType::where('nom','=', 'Changement de prix')->first();

        foreach($change_prix_mechanisms as $row) {
            $mechanism = Mechanism::where('nom', '=', $row)->first();

            $change_prix->mechanisms()->attach($mechanism->id);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
