<?php

use Illuminate\Database\Seeder;

class RuptureReasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('rupture_reasons')->truncate();

       $now = date('Y-m-d H:i:s');

       $data = [ 'Forte rotation du produit','Non respect du planning de visite','Manque d\'espace en rayon'
                 ,'Ne connait pas les références','Mode de paiement ','Fréquence de visite insuffisante','Autre'];

       foreach($data as $row) {
           DB::table('rupture_reasons')->insert(['nom' => $row,  'created_at' => $now, 'updated_at' => $now ]);
       }
    }
}
