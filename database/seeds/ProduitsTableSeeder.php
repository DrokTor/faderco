<?php

use App\Models\Famille;
use App\Models\Marque;
use Illuminate\Database\Seeder;

class ProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produits')->truncate();

        //date
        $now = date('Y-m-d H:i:s');

        ///////////////////////////////////Awane
        //récupération de la marque et toutes ses familles
        $awane = Marque::where('nom','=', 'Awane')->first();
        $awane_familles = $awane->familles()->get(['nom']);

        //remplir un tableau de produits de la marque par famille (pivot table id dans produit)
        foreach($awane_familles as $famille) {
            if($famille->nom == "Confort") {
                $data[] = array('nom' => 'Normal', 'largeur' => 8, "up"=>10, "pc"=>24, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Longue', 'largeur' => 10, "up"=>9, "pc"=>24, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Confort Eco") {
                $data[] = array('nom' => 'Normal', 'largeur' => 8, "up"=>20, "pc"=>18, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Longue', 'largeur' => 10, "up"=>18, "pc"=>18, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Confort 3D") {
                $data[] = array('nom' => 'Normal', 'largeur' => 8, "up"=>9, "pc"=>18, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Longue', 'largeur' => 10, "up"=>8, "pc"=>18, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Nuit', 'largeur' => 10, "up"=>7, "pc"=>18, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Confort 3D Duo Pack") {
                $data[] = array('nom' => 'Normal', 'largeur' => 8, "up"=>18, "pc"=>18, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Longue', 'largeur' => 10, "up"=>16, "pc"=>18, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Nuit', 'largeur' => 10, "up"=>14, "pc"=>18, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Ultrafine") {
                $data[] = array('nom' => 'Normal', 'largeur' => 8, "up"=>9, "pc"=>16, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Longue', 'largeur' => 10, "up"=>8, "pc"=>16, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Protége Slip") {
                $data[] = array('nom' => 'Day by Day', "up"=>20, "pc"=>18, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Slim', "up"=>20, "pc"=>18, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
        }

        ///////////////////////////////////Bimbies
        //récupération de la marque et toutes ses familles
        $bimbies = Marque::where('nom','=', 'Bimbies')->first();
        $bimbies_familles = $bimbies->familles()->get(['nom']);

        //remplir un tableau de produits de la marque par famille (pivot table id dans produit)
        foreach($bimbies_familles as $famille) {
            if($famille->nom == "Small Pack") {
                $data[] = array('nom' => 'Nouveau Née 1', "up"=>12, "pc"=>10, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Mini 2', "up"=>20, "pc"=>10, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Midi 3', "up"=>20, "pc"=>10, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Maxi 4', "up"=>20, "pc"=>10, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Junior 5', "up"=>20, "pc"=>10, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Junior +6', "up"=>20, "pc"=>10, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Pack Pratique") {
                $data[] = array('nom' => 'Nouveau Née 1', "up"=>20, "pc"=>6, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Mini 2', "up"=>22, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Midi 3', "up"=>20, "pc"=>6, "up"=>20, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Maxi 4', "up"=>18, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Junior 5', "up"=>16, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Pack Eco") {
                $data[] = array('nom' => 'Mini 2', "up"=>40, "pc"=>3, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Midi 3', "up"=>37, "pc"=>3, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Maxi 4', "up"=>34, "pc"=>3, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Junior 5', "up"=>30, "pc"=>3, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Bonus Pack") {
                $data[] = array('nom' => 'Mini 2', "up"=>76, "pc"=>2, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Midi 3', "up"=>70, "pc"=>2, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Maxi 4', "up"=>64, "pc"=>2, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Junior 5', "up"=>58, "pc"=>2, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Lingette") {
                $data[] = array('nom' => 'Fleur de Coton', "up"=>72, "pc"=>12, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Lait Nettoyant', "up"=>72, "pc"=>12, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Aloe Verra et Camomille', "up"=>72, "pc"=>12, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Thé Vert et Concombre', "up"=>72, "pc"=>12, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Huile \'Amande Douce', "up"=>72, "pc"=>12, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Fresh', "up"=>84, "pc"=>12, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
        }

        ///////////////////////////////////Cotex
        //récupération de la marque et toutes ses familles
        $cotex = Marque::where('nom','=', 'Cotex')->first();
        $cotex_familles = $cotex->familles()->get(['nom']);

        //remplir un tableau de produits de la marque par famille (pivot table id dans produit)
        foreach($cotex_familles as $famille) {
            if($famille->nom == "Essuietout") {
                $data[] = array('nom' => 'Classic 2R', "up"=>2, "pc"=>10, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Classic 4R', "up"=>4, "pc"=>6, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Le Basico 2R', "up"=>2, "pc"=>10, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Le Basico 3R', "up"=>3, "pc"=>10, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Mega', "up"=>1, "pc"=>8, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Plus 2R', "up"=>2, "pc"=>10, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Plus 4R', "up"=>4, "pc"=>6, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);

            }
            elseif($famille->nom == "Papier Hygiènique") {
                $data[] = array('nom' => 'Confort 4R', "up"=>4, "pc"=>15, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Confort 8R', "up"=>8, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'COTEX Déco 4R', "up"=>4, "pc"=>15, "up"=>20, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Déco 8R', "up"=>8, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Evasion Brise marine 4R', "up"=>4, "pc"=>15, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Evasion Mesk elil 4R', "up"=>4, "pc"=>15, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Evasion Vanille 4R', "up"=>4, "pc"=>15, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Velours 4R', "up"=>4, "pc"=>15, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Serviette de table") {
                $data[] = array('nom' => 'Sélection', "up"=>1, "pc"=>24, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Color', "up"=>1, "pc"=>27, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Elégance', "up"=>1, "pc"=>24, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Original 300', "up"=>1, "pc"=>18, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Original 200', "up"=>1, "pc"=>18, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Original 80', "up"=>1, "pc"=>40, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Pure 50', "up"=>1, "pc"=>24, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Mouchoir Box") {
                $data[] = array('nom' => 'Compactes', "up"=>1, "pc"=>24, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Classique', "up"=>1, "pc"=>24, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Cubique', "up"=>1, "pc"=>24, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Eco', "up"=>1, "pc"=>20, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Mouchoir Etuit") {
                $data[] = array('nom' => 'Classic', "up"=>10, "pc"=>24, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Kids', "up"=>10, "pc"=>24, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
        }
        ///////////////////////////////////Uni-Form
        //récupération de la marque et toutes ses familles
        $uni_form = Marque::where('nom','=', 'Uni-Form')->first();
        $uni_form_familles = $uni_form->familles()->get(['nom']);

        //remplir un tableau de produits de la marque par famille (pivot table id dans produit)
        foreach($uni_form_familles as $famille) {
            if($famille->nom == "Couche") {
                $data[] = array('nom' => 'Small', "up"=>11, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Medium', "up"=>10, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Large', "up"=>8, "pc"=>6, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Alése', "up"=>10, "pc"=>8, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Lingette") {
                $data[] = array('nom' => 'Lingette Uniform', "up"=>72, "pc"=>12, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
        }
        ///////////////////////////////////Coty'Lys
        //récupération de la marque et toutes ses familles
        $cotylys = Marque::where('nom','=', 'Coty\'Lys')->first();
        $cotylys_familles = $cotylys->familles()->get(['nom']);

        //remplir un tableau de produits de la marque par famille (pivot table id dans produit)
        foreach($cotylys_familles as $famille) {
            if($famille->nom == "Coton tige") {
                $data[] = array('nom' => 'Boite 100 Pcs', "up"=>100, "pc"=>36, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Boite Family', "up"=>200, "pc"=>36, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Boite Baby', "up"=>50, "pc"=>36, 'largeur' => 10, 'longueur' => 8, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
            elseif($famille->nom == "Disque démaquillant") {
                $data[] = array('nom' => 'Pochette 80', "up"=>80, "pc"=>18, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
                $data[] = array('nom' => 'Pochette 120', "up"=>120, "pc"=>18, 'largeur' => 8, 'longueur' => 6, 'hauteur' => 4, 'famille_marque_id' => $famille->pivot->id , 'created_at' => $now, 'updated_at' => $now);
            }
        }

        /////////////////////insertion des données
        DB::table('produits')->insert($data);
    }
}
