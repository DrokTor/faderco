<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->truncate();
        DB::table('message_user')->truncate();

        $now = date('Y-m-d H:i:s');

        $admin = \App\Models\User::where('role_id', '=', \App\Models\Role::where('designation', '=', 'Administrateur')->first()->id)->first();

        $users = \App\Models\User::where('role_id', '=', \App\Models\Role::where('designation', '=', 'Merchandiser')->first()->id)->get();

        $data = [
            'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. ',
            'Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500',
            'On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même.',
            'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire'
        ];

        foreach($data as $key=>$row)
        {
            $message_id= DB::table('messages')->insertGetId(['objet' => 'Promotion '.$key, 'contenu' => $row, 'user_id'=> $admin->id, 'created_at' => $now, 'updated_at' => $now ]);

            foreach($users as $user) {
                $user->messages()->attach($message_id);
            }
        }
    }
}
