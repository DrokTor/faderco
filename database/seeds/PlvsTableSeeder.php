<?php

use Illuminate\Database\Seeder;

class PlvsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          DB::table('plvs')->truncate();

         $now = date('Y-m-d H:i:s');

         $data = [ ['out','Tinda'],['out','Enseigne'],['out','One way'],['in','Stop rayon'],['in','Réglette'],['in','Présentoir'],['in','TG']];

         foreach($data as $row) {
             DB::table('plvs')->insert(['type' => $row[0],'nom' => $row[1], 'created_at' => $now, 'updated_at' => $now ]);
         }
     }

}
