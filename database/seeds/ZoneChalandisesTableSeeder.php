<?php

use Illuminate\Database\Seeder;

class ZoneChalandisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
         DB::table('zone_chalandises')->truncate();

         $now = date('Y-m-d H:i:s');

         $data = ['Communale', 'Départementale', 'Régionnal','National'];

         foreach($data as $row) {
             DB::table('zone_chalandises')->insert(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);
         }
     }
}
