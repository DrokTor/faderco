<?php

use Illuminate\Database\Seeder;
use App\Models\Oauth;
use App\Models\User;

class OauthsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->truncate();

        $users = User::all();

        foreach($users as $user) {
            $id = base64_encode($user->username);
            $secret = md5($user->username.'overgen'.$user->username);

            Oauth::create([
                'id' => $id,
                'secret' => $secret,
                'name' => $user->username,
            ]);
        }
    }
}
