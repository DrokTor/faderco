<?php

use Illuminate\Database\Seeder;

class TypeActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_activities')->truncate();

        $now = date('Y-m-d H:i:s');

        $data = ['Connexion', 'Validation', 'Insertion','Modification', 'Suppression'];

        foreach($data as $row) {
            DB::table('type_activities')->insert(['nom' => $row, 'created_at' => $now, 'updated_at' => $now ]);
        }
    }
}
