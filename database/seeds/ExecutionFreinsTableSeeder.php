<?php

use Illuminate\Database\Seeder;

class ExecutionFreinsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
        DB::table('execution_freins')->truncate();

        $now = date('Y-m-d H:i:s');

        $data = [ 'Aucun' ,'Refus catégorique du client','Refus client S/Condition'
                ,'Organisation PDV','Rupture Sku', 'Espace insuffisant', 'Autre'];

        foreach($data as $row) {
            DB::table('execution_freins')->insert(['nom' => $row,  'created_at' => $now, 'updated_at' => $now ]);
        }
      }
}
