<?php

use Illuminate\Database\Seeder;

class MechanismsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('mechanisms')->truncate();

        $now = date('Y-m-d H:i:s');

        $data = ['Prix barré', 'Goodies offerts', 'Echantillon gratuit','1+1', '2+1', '3+1', 'Gain à l\'achat',
            'Tombola', 'Test produit', 'Vente simple', 'Réglettes', 'Présentoir de comptoir', 'Présentoir au sol',
            'Stop Rayon', 'TG', 'Stick floor', 'Tindas', 'Potences', 'Enseignes', 'Plaques', 'One way vision',
            'Autre'
        ];

        foreach($data as $row) {
            if($row == "Autre") DB::table('mechanisms')->insert(['nom' => $row, 'prec'=> true, 'created_at' => $now, 'updated_at' => $now ]);
            else DB::table('mechanisms')->insert(['nom' => $row, 'prec'=> false, 'created_at' => $now, 'updated_at' => $now ]);
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
